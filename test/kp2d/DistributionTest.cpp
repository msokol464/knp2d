#include <iostream>

#include "../../kp2d/include/kp2d/Distribution.h"
#include "../include/catch.hpp"

TEST_CASE("BinomialDistribution", "[binomial][distrib]") {
  using namespace kp2d;
  std::unique_ptr<IntDistrib> binomial = Binomial::create(20, 0.4);
  REQUIRE(binomial);

  std::cout << "Binomial: ";
  for (int i = 0; i < 50; i++) {
    std::cout << binomial->draw() << ' ';
  }
  std::cout << '\n';
}

TEST_CASE("UniformDistribution", "[uniform][distrib]") {
  using namespace kp2d;
  std::unique_ptr<IntDistrib> iUniform = UniformInt::create(0, 20);
  REQUIRE(iUniform);

  std::cout << "Uniform ints:  ";
  for (int i = 0; i < 50; i++) {
    std::cout << iUniform->draw() << ' ';
  }
  std::cout << '\n';
}

TEST_CASE("NormalDistribution", "[normal][distrib]") {
  using namespace kp2d;
  std::unique_ptr<IntDistrib> iNormal = NormalInt::create(0, 5);
  REQUIRE(iNormal);

  std::cout << "Normal ints:  ";
  for (int i = 0; i < 50; i++) {
    std::cout << iNormal->draw() << ' ';
  }
  std::cout << '\n';

  std::unique_ptr<FloatDistrib> dNormal = NormalFloat::create(12.0, 12.0);
  REQUIRE(dNormal);

  std::cout << "Normal doubles:  ";
  for (int i = 0; i < 50; i++) {
    std::cout << dNormal->draw() << ' ';
  }
  std::cout << '\n';
}

TEST_CASE("CopyDistribution", "[cpy][distrib]") {
  using namespace kp2d;
  using Catch::Matchers::WithinAbs;
  // constexpr FloatType margin = 0.000001;

  std::unique_ptr<IntDistrib> u1 = UniformInt::create(3, 9);
  REQUIRE(u1);
  std::unique_ptr<IntDistrib> u2 = u1->clone();
  REQUIRE(u2);

  // REQUIRE(u1->draw() == u2->draw());
  // REQUIRE(u1->draw() == u2->draw());
  // REQUIRE(u1->draw() == u2->draw());

  std::unique_ptr<IntDistrib> n1 = NormalInt::create(3, 0.1);
  REQUIRE(n1);
  std::unique_ptr<IntDistrib> n2 = n1->clone();
  REQUIRE(n2);

  // REQUIRE(n1->draw() == n2->draw());
  // REQUIRE(n1->draw() == n2->draw());
  // REQUIRE(n1->draw() == n2->draw());

  std::unique_ptr<IntDistrib> b1 = Binomial::create(3, 0.1);
  REQUIRE(b1);
  std::unique_ptr<IntDistrib> b2 = b1->clone();
  REQUIRE(b2);

  // REQUIRE(b1->draw() == b2->draw());
  // REQUIRE(b1->draw() == b2->draw());
  // REQUIRE(b1->draw() == b2->draw());

  std::unique_ptr<FloatDistrib> n3 = NormalFloat::create(180, 0.2);
  REQUIRE(n3);
  std::unique_ptr<FloatDistrib> n4 = n3->clone();
  REQUIRE(n4);

  // REQUIRE_THAT(n3->draw(), WithinAbs(n4->draw(), margin));
  // REQUIRE_THAT(n3->draw(), WithinAbs(n4->draw(), margin));
  // REQUIRE_THAT(n3->draw(), WithinAbs(n4->draw(), margin));
}

TEST_CASE("DistributionEquality", "[eq][distrib]") {
  using namespace kp2d;

  std::unique_ptr<IntDistrib> n1 = NormalInt::create(3, 0.1);
  std::unique_ptr<IntDistrib> n2 = NormalInt::create(3, 0.1);

  REQUIRE(n1->operator==(n2.get()));

  std::unique_ptr<IntDistrib> n3 = n2->clone();

  REQUIRE(n1->operator==(n3.get()));

  std::unique_ptr<IntDistrib> b1 = Binomial::create(3, 0.1);

  REQUIRE_FALSE(n1->operator==(b1.get()));
}