// #include <iostream>

#include "../../kp2d/include/kp2d/State.h"
#include "../include/RangeMatchers.h"
#include "../include/catch.hpp"

TEST_CASE("StateTansition", "[state][ctor]") {
  using namespace kp2d;
  using Catch::Matchers::WithinAbs;
  constexpr FloatType margin = 0.000001;

  Point container{100, 200};

  size_t numOfSamples =
      (container.y() % param::DefaultUnitsPerSample == 0
           ? static_cast<size_t>(container.y() / param::DefaultUnitsPerSample)
           : static_cast<size_t>(container.y() / param::DefaultUnitsPerSample +
                                 1));

  Rectangle r1{{10, 30}, {0, 0}};
  // resulting cps: (0, 30), (10, 0)
  State stateOne(container);
  REQUIRE(stateOne.container().product() == 20000);
  REQUIRE(stateOne.envelopeArea() == 0);
  REQUIRE(stateOne.usedArea() == 0);
  REQUIRE(stateOne.numOfCornerPoints() == 1);
  REQUIRE(stateOne.numOfxAxisSamples() == numOfSamples);
  REQUIRE(stateOne.isValid());
  std::cout << "[TEST_CASE] Env. area increase: "
            << stateOne.envelopeAreaIncrease() << "\n";

  std::vector<Point> cps1{{0, 0}};
  REQUIRE_THAT(stateOne.cornerPoints(), EqualsRange(cps1));

  State stateTwo(stateOne, r1);
  REQUIRE(stateTwo.container().product() == 20000);
  REQUIRE(stateTwo.envelopeArea() == 300);
  REQUIRE(stateTwo.usedArea() == 300);
  REQUIRE(stateTwo.numOfCornerPoints() == 2);
  REQUIRE(stateTwo.numOfxAxisSamples() == numOfSamples);
  REQUIRE(stateTwo.isValid());
  std::cout << "[TEST_CASE] Env. area increase: "
            << stateTwo.envelopeAreaIncrease() << "\n";

  std::vector<Point> cps2{{0, 30}, {10, 0}};
  REQUIRE_THAT(stateTwo.cornerPoints(), EqualsRange(cps2));

  Rectangle r2{{50, 50}, {0, 30}};
  // resulting cps: (0, 80), (50, 0)
  State stateThree(stateTwo, r2);
  REQUIRE(stateThree.container().product() == 20000);
  REQUIRE(stateThree.envelopeArea() == 4000);
  REQUIRE(stateThree.usedArea() == 2500 + 300);
  REQUIRE(stateThree.numOfCornerPoints() == 2);
  REQUIRE(stateThree.numOfxAxisSamples() == numOfSamples);
  REQUIRE(stateThree.isValid());
  std::cout << "[TEST_CASE] Env. area increase: "
            << stateThree.envelopeAreaIncrease() << "\n";

  std::vector<Point> cps3{{0, 80}, {50, 0}};
  REQUIRE_THAT(stateThree.cornerPoints(), EqualsRange(cps3));

  Rectangle r3{{5, 40}, {50, 0}};
  // resulting cps: (0, 80), (50, 40), (55, 0)
  State stateFour(stateThree, r3);
  REQUIRE(stateFour.envelopeArea() == 4200);
  REQUIRE(stateFour.usedArea() == 2500 + 300 + 200);
  REQUIRE(stateFour.numOfCornerPoints() == 3);
  REQUIRE(stateFour.isValid());
  std::cout << "[TEST_CASE] Env. area increase: "
            << stateFour.envelopeAreaIncrease() << "\n";

  REQUIRE_THAT(stateFour.envelopeAreaPortion(), WithinAbs(0.21, margin));
  REQUIRE_THAT(stateFour.usedAreaPortion(), WithinAbs(0.15, margin));

  std::vector<Point> cps4{{0, 80}, {50, 40}, {55, 0}};
  REQUIRE_THAT(stateFour.cornerPoints(), EqualsRange(cps4));
}

TEST_CASE("StatePtr", "[state][create]") {
  using namespace kp2d;
  using Catch::Matchers::WithinAbs;

  constexpr FloatType margin = 0.000001;

  std::unique_ptr<State> primoStato = State::create(Point{400, 190});
  REQUIRE(primoStato);

  std::unique_ptr<State> statoDue =
      State::create(*primoStato, Rectangle{{10, 15}, {0, 0}});

  REQUIRE(statoDue);

  // std::cout << statoDue->cornerPointsToString() << '\n';

  REQUIRE(statoDue->envelopeArea() == 150);
  REQUIRE(statoDue->usedArea() == 150);
  REQUIRE(statoDue->numOfCornerPoints() == 2);
  REQUIRE(statoDue->isValid());

  REQUIRE_THAT(statoDue->envelopeAreaPortion(),
               WithinAbs(0.00197368421, margin));
  REQUIRE_THAT(statoDue->usedAreaPortion(), WithinAbs(0.00197368421, margin));

  std::vector<Point> cps{{0, 15}, {10, 0}};
  REQUIRE_THAT(statoDue->cornerPoints(), EqualsRange(cps));
}

TEST_CASE("MismatchTest", "[state][mismatch]") {
  using namespace kp2d;
  // using Catch::Matchers::WithinAbs;
  // constexpr FloatType margin = 0.000001;

  Point container{100, 100};
  // 10|
  //  9|..... _ _
  //  8|/_/_|    |
  //  7|    |    | _ _ _ _ _
  //  6|    |    |          |
  //  5|    |5_ _|6_ _ _ _ _|
  //  4|3_ _|/_/_/_/_|    |/:
  //  3|      |      |    |/:
  //  2|      |      |    |/:
  //  1|1_ _ _|2_ _ _|4_ _|/:_ _
  //     1 2 3  4 5 6  7 8  9 10 (x10)

  Point sizeA{30, 30}, sizeB{20, 40}, sizeC{50, 20};
  State s0(container);
  REQUIRE(s0.isValid());
  CHECK(s0.upperCPYMismatch() == 0);
  CHECK(s0.lowerCPXMismatch() == 0);

  State s1(s0, Rectangle{sizeA, {0, 0}});
  REQUIRE(s1.isValid());
  CHECK(s1.upperCPYMismatch() == 0);
  CHECK(s1.lowerCPXMismatch() == 0);

  State s2(s1, Rectangle{sizeA, {30, 0}});
  REQUIRE(s2.isValid());
  CHECK(s2.upperCPYMismatch() == 0);
  CHECK(s2.lowerCPXMismatch() == 0);

  // 10|
  //  9|..... _ _
  //  8|/_/_|    |
  //  7|    |    | _ _ _ _ _
  //  6|    |    |          |
  //  5|    |5_ _|6_ _ _ _ _|
  //  4|3_ _|/_/_/_/_|    |/:
  //  3|      |      |    |/:
  //  2|      |      |    |/:
  //  1|1_ _ _|2_ _ _|4_ _|/:_ _
  //     1 2 3  4 5 6  7 8  9 10 (x10)

  std::cout << "[TEST_CASE] State 3\n";
  State s3(s2, Rectangle{sizeB, {0, 30}});
  REQUIRE(s3.isValid());
  CHECK(s3.upperCPYMismatch() == 0);
  CHECK(s3.lowerCPXMismatch() == 40);

  std::cout << "[TEST_CASE] State 4\n";
  State s4(s3, Rectangle{sizeB, {60, 0}});
  REQUIRE(s4.isValid());
  CHECK(s4.upperCPYMismatch() == -10);
  CHECK(s4.lowerCPXMismatch() == 0);

  std::cout << "[TEST_CASE] State 5\n";
  State s5(s4, Rectangle{sizeB, {20, 40}});
  REQUIRE(s5.isValid());
  CHECK(s5.upperCPYMismatch() == -10);
  CHECK(s5.lowerCPXMismatch() == 40);

  std::cout << "[TEST_CASE] State 6\n";
  State s6(s5, Rectangle{sizeC, {40, 40}});
  REQUIRE(s6.isValid());
  CHECK(s6.upperCPYMismatch() == 20);
  CHECK(s6.lowerCPXMismatch() == -10);
}

TEST_CASE("StateManual", "[state][manual]") {
  using kp2d::Point;
  using kp2d::Rectangle;
  using kp2d::State;

  // 16, 8 (x8)
  Point C{128, 64};
  // Storage::create({{128, 8, 1}, {16, 16, 10}, {32, 32, 6}, {24, 8, 2}}, C);

  // END STATE
  // [ 128 x    8] at(   0,    0)
  // [  32 x   32] at(   0,    8)
  // [  32 x   32] at(  32,    0)
  // [  32 x   32] at(  64,    0)
  // [  32 x   32] at(  96,    0)
  // [   8 x   24] at(   0,   40)
  // [   8 x   24] at(   8,   40)
  // [  16 x   16] at(  16,   40)
  // [  32 x   32] at(  32,   32)
  // [  32 x   32] at(  64,   32)
  // [  16 x   16] at(  96,   32)
  // [  16 x   16] at( 112,   32)
  // [  16 x   16] at(  96,   48)
  // [  16 x   16] at( 112,   48)

  // (   0,   64)
  // ( 128,    0)

  State s0{C};
  State s1{s0, {{128, 8}, {0, 0}}};
  std::cout << s1.cornerPointsToString() << '\n';

  State s2{s1, {{32, 32}, {0, 8}}};
  std::cout << s2.cornerPointsToString() << '\n';

  // State s3{s2, {{32, 32}, {0, 0}}};
  // State s4{s3, {{128, 8}, {0, 0}}};
  // State s5{s4, {{128, 8}, {0, 0}}};
  // State s6{s5, {{128, 8}, {0, 0}}};
}