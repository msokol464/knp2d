#include <iostream>

#include "../../kp2d/include/kp2d/CutProblemGenerator.h"
#include "../include/catch.hpp"

TEST_CASE("CutProblemGeneration", "[relaxed][gen]") {
  using namespace kp2d;

  CutProblemGenerator rGen1{Point{1200, 800}, 34, 28, UniformInt::create(1, 6)};
  CutProblemGenerator rGen2{Point{1200, 800}, 34, 28,
                            PositiveNormalInt::create(4, 6), 10};

  Problem p11 = rGen1.generate();
  //   std::cout << p11.toJson().dump(2) << '\n';
  std::cout << p11 << '\n';

  Problem p21 = rGen2.generate();
  std::cout << p21 << '\n';
}

TEST_CASE("CutProblemSetGeneration", "[relaxed][set][micro][under]") {
  using namespace kp2d;

  CutProblemGenerator rGen1{Point{1200, 800}, 34, 28, UniformInt::create(1, 6)};

  ProblemSet<RandomPlacementSolver> set =
      rGen1.generateSet<RandomPlacementSolver>(10u);
  std::cout << set << '\n';
}

TEST_CASE("GenerateMicroExample", "[relaxed][set][micro][over]") {
  using namespace kp2d;

  CutProblemGenerator rGen1{Point{100, 64}, 10, 8, UniformInt::create(1, 6)};

  ProblemSet<RandomPlacementSolver> set =
      rGen1.generateSet<RandomPlacementSolver>(
          10u, CutProblemGenerator::AreaOption::AreaOverflow);
  std::cout << set << '\n';
}

TEST_CASE("StorageAreaProportion", "[relaxed][set][area]") {
  using namespace kp2d;

  CutProblemGenerator gen{Point{1200, 640}, 34, 25, NormalInt::create(10, 2)};

  IntType numProblems = 10;

  ProblemSet<RandomPlacementSolver> set1 =
      gen.generateSet<RandomPlacementSolver>(
          numProblems, CutProblemGenerator::AreaOption::AreaUnderflow);
  FloatType avg = 0.;
  for (const Problem& p : set1) {
    std::cout << p.unusedStorageArea() << '\t';
    avg += p.unusedStorageArea();
  }
  std::cout << "\nAreaUnderflow, unconstrained num of groups: "
            << avg / numProblems << '\n';

  gen.setNumOfGroups(-1);
  avg = 0.;

  ProblemSet<RandomPlacementSolver> set2 =
      gen.generateSet<RandomPlacementSolver>(
          numProblems, CutProblemGenerator::AreaOption::AreaOverflow);
  for (const Problem& p : set2) {
    std::cout << p.unusedStorageArea() << '\t';
    avg += p.unusedStorageArea();
  }
  std::cout << "\nAreaOverflow, unconstrained num of groups: "
            << avg / numProblems << '\n';

  gen.setNumOfGroups(8);
  avg = 0.;

  ProblemSet<RandomPlacementSolver> set3 =
      gen.generateSet<RandomPlacementSolver>(
          numProblems, CutProblemGenerator::AreaOption::AreaUnderflow);
  for (const Problem& p : set3) {
    std::cout << p.unusedStorageArea() << '\t';
    avg += p.unusedStorageArea();
  }
  std::cout << "\nAreaUnderflow, 8 groups: " << avg / numProblems << '\n';

  gen.setNumOfGroups(8);
  avg = 0.;

  ProblemSet<RandomPlacementSolver> set4 =
      gen.generateSet<RandomPlacementSolver>(
          numProblems, CutProblemGenerator::AreaOption::AreaOverflow);
  for (const Problem& p : set4) {
    std::cout << p.unusedStorageArea() << '\t';
    avg += p.unusedStorageArea();
  }

  std::cout << "\nAreaOverflow, 8 groups: " << avg / numProblems << '\n';

  avg = 0.;
}