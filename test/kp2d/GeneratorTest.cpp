#include <iostream>

#include "../../kp2d/include/kp2d/Generators.h"
#include "../include/catch.hpp"

/// TODO: Print only if VERBOSE is defined or
/// use some other method of reducing output
#define VERBOSE

TEST_CASE("DDistribPointGenerator", "[ctor][print][generate][dDistr]") {
  using namespace kp2d;

  IntType xMin = 10, yMin = 30, xMax = 20, yMax = 40;
  std::unique_ptr<PointGenerator> pointGenerator = PointGenerator::create(
      UniformInt::create(xMin, xMax), UniformInt::create(yMin, yMax));

  int iter = 100;
  for (auto i = 0; i < iter; i++) {
    Point p = pointGenerator->generate();
    REQUIRE(p.x() >= xMin);
    REQUIRE(p.x() <= xMax);
    REQUIRE(p.y() >= yMin);
    REQUIRE(p.y() <= yMax);

#ifdef VERBOSE
    std::cout << p << ' ';
    if (i % 10 == 0 || i == iter - 1) {
      std::cout << '\n';
    }
#endif
  }
}

TEST_CASE("SDistribPointGenerator", "[ctor][print][generate][sDistr]") {
  using namespace kp2d;

  IntType min = 10, max = 30;
  std::unique_ptr<PointGenerator> pointGenerator =
      PointGenerator::create(UniformInt::create(min, max));

  int iter = 37;
  for (auto i = 0; i < iter; i++) {
    Point p = pointGenerator->generate();
    REQUIRE(p.x() >= min);
    REQUIRE(p.x() <= max);
    REQUIRE(p.y() >= min);
    REQUIRE(p.y() <= max);

#ifdef VERBOSE
    std::cout << p << ' ';
    if (i % 10 == 0 || i == iter - 1) {
      std::cout << '\n';
    }
#endif
  }
}

TEST_CASE("StorageGenerator", "[ctor][print][generate][storage]") {
  using namespace kp2d;

  Point container{400, 900};

  StorageGenerator sg{PointGenerator::create(UniformInt::create(100, 350),
                                             UniformInt::create(45, 100)),
                      Binomial::create(20, 0.5)};

  std::unique_ptr<Storage> storage{sg.generate(container)};

  for (const auto& gr : storage->rectGroups()) {
    std::cout << gr << '\n';
  }

  REQUIRE(storage->sumArea() <= container.product());
  // std::cout << "Storage sum area: " << storage->sumArea() << '\n';
}

TEST_CASE("UnfeasableStorageGenerator",
          "[ctor][print][generate][storage][empty]") {
  using namespace kp2d;

  Point container{100, 900};

  StorageGenerator sg{PointGenerator::create(UniformInt::create(101, 202)),
                      UniformInt::create(1, 10)};

  std::unique_ptr<Storage> storage{sg.generate(container)};

  for (const auto& gr : storage->rectGroups()) {
    std::cout << gr << '\n';
  }

  REQUIRE(storage->sumArea() == 0);
  // std::cout << "Storage sum area: " << storage->sumArea() << '\n';
}

TEST_CASE("PointGeneratorCopy", "[point][gen][cpy]") {
  using namespace kp2d;
  Point container{10, 20};

  std::unique_ptr<PointGenerator> pg1 = PointGenerator::create(
      UniformInt::create(1, 3), UniformInt::create(4, 6));
  std::unique_ptr<PointGenerator> pg2 =
      PointGenerator::create(UniformInt::create(1, 3));
  std::unique_ptr<PointGenerator> pg3 = PointGenerator::create(
      UniformInt::create(2, 3), UniformInt::create(4, 6));

  REQUIRE_FALSE(*pg1 == pg2.get());
  REQUIRE_FALSE(*pg1 == pg3.get());

  std::unique_ptr<PointGenerator> pgCpy = pg1->clone();

  REQUIRE(*pg1 == pgCpy.get());
}

TEST_CASE("StorageGeneratorCopy", "[storage][ctor][cpy]") {
  using namespace kp2d;
  Point container{10, 20};

  StorageGenerator sg{PointGenerator::create(UniformInt::create(1, 3),
                                             UniformInt::create(4, 6)),
                      NormalInt::create(3, 0.3)};

  StorageGenerator sgCpy{sg};

  REQUIRE(sg.isValid());
  REQUIRE(sgCpy.isValid());
  REQUIRE(sg == sgCpy);

  StorageGenerator sg2{
      PointGenerator::create(UniformInt::create(1, 3), NormalInt::create(4, 6)),
      NormalInt::create(3, 0.3)};

  REQUIRE(sg2.isValid());
  REQUIRE_FALSE(sg == sg2);
}