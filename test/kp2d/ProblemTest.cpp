#include <fstream>
#include <iostream>

#include "../../kp2d/include/kp2d/Generators.h"
#include "../../kp2d/include/kp2d/Logger.h"
#include "../../kp2d/include/kp2d/Problem.h"
#include "../include/RangeMatchers.h"
#include "../include/catch.hpp"

using namespace kp2d;

TEST_CASE("ProblemCreation", "[ctor][problem][print]") {
  Point rectangle{100, 400};
  StorageGenerator g{PointGenerator::create(UniformInt::create(10, 50)),
                     UniformInt::create(1, 10)};

  std::unique_ptr<Storage> stor = g.generate(rectangle);
  stor->consolidate();
  stor->sort(RectangleGroup::byArea);

  Problem instance{rectangle, std::move(stor)};

  std::cout << instance;
  std::cout << "STORAGE\n--------------------------------\n"
            << instance.storageToString() << '\n';
}

TEST_CASE("FeatureVec", "[problem][feat]") {
  Point rectangle{100, 500};
  std::unique_ptr<Storage> stor = Storage::create(
      {{10, 20, 7}, {80, 10, 10}, {50, 50, 5}, {100, 90, 20}}, rectangle);
  Problem instance{rectangle, std::move(stor)};
}

SCENARIO("FeatureMat", "[problem][feat][mat]") {
  GIVEN("Well-formed problem instance") {
    Point rectangle{100, 500};

    std::unique_ptr<Storage> stor = Storage::create(
        {{10, 20, 7}, {80, 10, 10}, {50, 50, 5}, {100, 90, 20}}, rectangle);

    Problem instance{rectangle, std::move(stor), FeatOption::allFeatures, 5};

    WHEN("Feature matrix is created") {
      DynMatrix optFeatures = instance.prepareFeatures();

      // std::cout << "[TEST CASE] features\n" << optFeatures << '\n';

      THEN("It has expected number of rows and columns") {
        /// instance
        // 1 point x 4 groups x 2 orientations = 8 columns
        REQUIRE(optFeatures.cols() == 8);
        // 4 groups x Storage::FeaturesPerGroup (10) + State::NumOfFeatures (3)
        // + numOfSamples floor(500 / 5)
        // REQUIRE(optFeatures.rows() == 143);
        Logger log{"data/out/featureMatV03.txt"};
        log << optFeatures << '\n';
      }

      THEN("Poroposed states are created") {
        // instance
        REQUIRE(instance.numOfProposedStates() == 8);
      }

      THEN("We can select new state from proposed") {
        //
        REQUIRE_NOTHROW(instance.selectState(1));
        // solution gets rectangle from firs group unrotated
        std::vector<Rectangle> tgtSolution = {Rectangle{{20, 10}, {0, 0}}};
        REQUIRE_THAT(instance.solution(), EqualsRange(tgtSolution));
        // proposed states are cleared
        REQUIRE(instance.numOfProposedStates() == 0);
      }
    }

    WHEN("Feature matrix is not created") {
      THEN("There are no proposed states") {
        //
        REQUIRE(instance.numOfProposedStates() == 0);
      }
      THEN("We can't select new state") {
        // Info is written to console
        REQUIRE_THROWS(instance.selectState(0));
      }
    }
  }
}

TEST_CASE("ProblemCpyCtor", "[problem][cpy]") {
  Point rectangle{200, 300};
  std::unique_ptr<Storage> stor = Storage::create(
      {{10, 20, 5}, {70, 100, 2}, {93, 12, 10}, {100, 90, 20}}, rectangle);

  Problem instance{rectangle, std::move(stor)};
  DynMatrix feat1 = instance.prepareFeatures();
  REQUIRE_NOTHROW(instance.selectState(1));
  DynMatrix feat2 = instance.prepareFeatures();
  REQUIRE_NOTHROW(instance.selectState(1));

  Problem cpy{instance};

  REQUIRE(cpy.isValid());
}

// SizeType samplesPerUnit = param::SamplesPerUnit

TEST_CASE("ProblemEquivalence", "[equiv]") {
  Point rec{200, 300};
  std::unique_ptr<Storage> stor = Storage::create(
      {{10, 20, 5}, {70, 100, 2}, {93, 12, 10}, {100, 90, 20}}, rec);

  Point rec1{400, 200};
  std::unique_ptr<Storage> stor1 = Storage::create(
      {{10, 20, 5}, {70, 100, 2}, {93, 12, 10}, {100, 90, 20}}, rec1);

  std::unique_ptr<Storage> stor2 = Storage::create(
      {{10, 20, 5}, {70, 100, 2}, {93, 12, 10}, {100, 100, 1}}, rec);

  std::unique_ptr<Storage> storEquiv = Storage::create(
      {{10, 20, 5}, {93, 12, 10}, {70, 100, 2}, {100, 90, 20}}, rec);

  Problem pEquiv1{rec, stor->clone()};
  Problem pEquiv2{rec, std::move(storEquiv)};

  Problem p1{rec1, stor->clone()};
  Problem p2{rec, std::move(stor1)};
  Problem p3{rec, stor->clone(), FeatOption::allFeatures, 3};
  Problem p4{rec, std::move(stor2)};

  Problem p{rec, std::move(stor)};

  REQUIRE_FALSE(p.isEquiv(p1));
  REQUIRE_FALSE(p.isEquiv(p2));
  REQUIRE_FALSE(p.isEquiv(p3));
  REQUIRE_FALSE(p.isEquiv(p4));

  REQUIRE(p.isEquiv(pEquiv1));
  REQUIRE(p.isEquiv(pEquiv2));
}

TEST_CASE("ProblemHeurFeatures", "[heur][problem]") {
  Point container{1000, 800};
  std::unique_ptr<Storage> stor = Storage::create(
      {{10, 20, 5}, {70, 100, 2}, {80, 100, 10}, {100, 90, 20}}, container);

  Problem p1{container, stor->clone(), FeatOption::areaMismatchHeuristic, 5};
  std::cout << "Num of groups: " << p1.numOfGroups()
            << "\nNum of features: " << p1.numOfFeatures() << '\n';
  Logger log{"data/out/mat/area_mismatch_heuristic"};
  log << p1.getAreaMismatchHeuristic() << '\n' << '\n';

  Problem p2{container, stor->clone(), FeatOption::minimalFeatures, 5};
  std::cout << "Num of groups: " << p2.numOfGroups()
            << "\nNum of features: " << p2.numOfFeatures() << '\n';
  Logger log2{"data/out/mat/minimal_features"};
  log2 << p2.prepareFeatures() << '\n';
}