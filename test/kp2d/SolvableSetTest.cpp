#include "../../kp2d/include/kp2d/Distribution.h"
#include "../../kp2d/include/kp2d/ProblemGenerator.h"
#include "../../kp2d/include/kp2d/SolvableSet.h"
#include "../../nnet/include/nnet/Network.h"
#include "../include/SolvableSetFixture.h"
#include "../include/catch.hpp"

TEST_CASE("SolvableSetConstruction", "[ctor][cpy][set]") {
  using namespace kp2d;
  using namespace nnet;

  using Catch::Matchers::WithinAbs;
  constexpr FloatType margin = 0.000001;

  ProblemGenerator pg{Point{1600, 800}, NormalInt::create(10, 3),
                      NormalInt::create(300, 10), NormalInt::create(80, 5)};

  kp2d::SizeType featNum = pg.numOfFeatures();

  std::cout << "Number of features: " << featNum << '\n';

  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::ranlux48 rnd{seed};

  Network net{
      {{featNum, 25, Activation::Type::RELU}, {25, 1, Activation::Type::RELU}}};

  std::cout << "Number of parameters: " << net.numOfParams() << '\n';

  SolvableSet<Network> set1;
  SolvableSet set2{pg.generateSetFixed<Network>(5), net};
  REQUIRE(set2.isUnsolved());
  SolvableSet set3{set2};
  REQUIRE(set3.isUnsolved());

  std::vector<FloatType> x1(net.numOfParams());
  set2(x1.data(), net.numOfParams());
  REQUIRE_FALSE(set2.isUnsolved());

  std::vector<FloatType> x2(net.numOfParams());
  set3(x2.data(), net.numOfParams());
  REQUIRE_FALSE(set3.isUnsolved());

  REQUIRE_THAT(set2.aggregateEval(), WithinAbs(set3.aggregateEval(), margin));
}

TEST_CASE("SolvableSetEqivalence", "[solvable][set][equiv]") {
  using kp2d::NormalInt;
  using kp2d::Point;
  using kp2d::ProblemGenerator;
  using SolvableSet = kp2d::SolvableSet<nnet::Network>;

  using nnet::Activation;
  using nnet::DenseLayer;
  using nnet::Network;

  ProblemGenerator pg{Point{800, 1200}, NormalInt::create(10, 3),
                      NormalInt::create(300, 10), NormalInt::create(80, 5)};

  kp2d::SizeType featNum = pg.numOfFeatures();
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();

  std::ranlux48 rnd{seed};

  Network net1{
      {{featNum, 25, Activation::Type::RELU}, {25, 1, Activation::Type::RELU}}};

  Network net2{
      {{featNum, 30, Activation::Type::RELU}, {30, 1, Activation::Type::RELU}}};

  Network net3{
      {{featNum, 30, Activation::Type::SMAX}, {30, 1, Activation::Type::RELU}}};

  SolvableSet set(pg.generateSetFixed<Network>(3), net1);
  SolvableSet setEquiv{set};
  REQUIRE(set.isEquiv(setEquiv));

  SolvableSet set1(pg.generateSetFixed<Network>(3), net1);
  SolvableSet set2{pg.generateSetFixed<Network>(3), net2};
  SolvableSet set3{pg.generateSetFixed<Network>(3), net3};
  SolvableSet set4{pg.generateSetFixed<Network>(5), net1};

  REQUIRE_FALSE(set.isEquiv(set1));
  REQUIRE_FALSE(set.isEquiv(set2));
  REQUIRE_FALSE(set.isEquiv(set3));
  REQUIRE_FALSE(set.isEquiv(set4));
}

TEST_CASE_METHOD(SolvableSetFixture, "Get solver from SolvableSet",
                 "[fixture][solvable][set][getSolver]") {
  using namespace nnet;
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::ranlux48 rnd{seed};
  Network net{
      {{featNum, 30, Activation::Type::SMAX}, {30, 1, Activation::Type::RELU}}};

  Network net1{
      {{featNum, 30, Activation::Type::RELU}, {30, 1, Activation::Type::RELU}}};

  REQUIRE(testSet.getSolver().isEquiv(net));
  REQUIRE_FALSE(testSet.getSolver().isEquiv(net1));
}

TEST_CASE("SolvableSetManualCreation", "[solvable][set][manual]") {
  using namespace kp2d;

  Point C{200, 100};

  Problem p1{C, Storage::create(
                    {{10, 10, 12}, {10, 5, 2}, {40, 100, 2}, {20, 20, 5}}, C)};
  Problem p2{C, Storage::create(
                    {{20, 10, 4}, {10, 5, 1}, {50, 25, 10}, {20, 20, 1}}, C)};
  Problem p3{C, Storage::create(
                    {{10, 10, 10}, {5, 5, 5}, {10, 20, 10}, {20, 20, 1}}, C)};
  Problem p4{
      C, Storage::create({{40, 10, 4}, {5, 5, 2}, {50, 10, 4}, {5, 20, 1}}, C)};

  std::vector<Problem> pvec;

  pvec.push_back(p1);
  pvec.push_back(p2);
  pvec.push_back(p3);
  pvec.push_back(p4);

  ProblemSet<nnet::Network, EvalType::UnusedContainerPortion> pset(
      std::move(pvec));

  REQUIRE(pset.isValid());

  std::ranlux48 rnd{1000u};
  Network net{{{pset.numOfFeatures(), 16, Activation::Type::RELU},
               {16, 8, Activation::Type::RELU},
               {8, 1, Activation::Type::ID}}};

  SolvableSet<nnet::Network, EvalType::UnusedContainerPortion> solvable(
      std::move(pset), net);

  REQUIRE(solvable.isValid());
}

TEST_CASE("SolvingWithHeuristic", "[heur][solvable][set][manual]") {
  using namespace kp2d;

  Point C{200, 100};

  Problem p1{C, Storage::create(
                    {{10, 10, 12}, {10, 5, 2}, {40, 100, 2}, {20, 20, 5}}, C)};
  Problem p2{C, Storage::create(
                    {{20, 12, 4}, {10, 7, 12}, {50, 25, 5}, {4, 60, 4}}, C)};
  Problem p3{C, Storage::create(
                    {{10, 10, 10}, {5, 8, 5}, {10, 48, 10}, {30, 16, 10}}, C)};
  Problem p4{
      C, Storage::create({{40, 10, 4}, {5, 5, 2}, {50, 10, 4}, {3, 60, 5}}, C)};

  std::vector<Problem> pvec;

  pvec.push_back(p1);
  pvec.push_back(p2);
  pvec.push_back(p3);
  pvec.push_back(p4);

  ProblemSet<kp2d::HeuristicSolver> pset(std::move(pvec));
  SolvableSet sset(std::move(pset), kp2d::HeuristicSolver{});

  FloatType eval = sset.solve();
  std::cout << "Evaluation avg: " << eval << '\n';

  std::ofstream sol{"data/out/heur6/solution.json",
                    std::ios_base::out | std::ios_base::trunc};
  sset.saveToJson(sol);
  sol.close();
}

TEST_CASE("GeneratedWithHeuristic", "[heur][solvable][set][gen]") {
  using namespace kp2d;

  Point C{200, 100};

  ProblemGenerator gen{C, UniformInt::create(3, 100), NormalInt::create(20, 10),
                       FeatOption::areaMismatchHeuristic};

  gen.setNumOfGroups(5);

  ProblemSet<kp2d::HeuristicSolver> pset =
      gen.template generateSetFixed<kp2d::HeuristicSolver>(1);
  SolvableSet sset(std::move(pset), kp2d::HeuristicSolver{});

  FloatType eval = sset.solve();
  std::cout << "Evaluation avg: " << eval << '\n';

  std::ofstream sol{"data/out/heur1/gen_heur_solve_2.json",
                    std::ios_base::out | std::ios_base::trunc};
  sset.saveToJson(sol);
  sol.close();
}