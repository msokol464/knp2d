#include <iostream>

#include "../../kp2d/include/kp2d/ProblemGenerator.h"
#include "../include/catch.hpp"

TEST_CASE("ProblemGeneration", "[problem][gen]") {
  using namespace kp2d;

  ProblemGenerator kp2dGenerator{
      {500, 350}, NormalInt::create(7.5, 0.5), UniformInt::create(3, 100)};

  ProblemSet testSet = kp2dGenerator.generateSet<RandomPlacementSolver>(5);
  for (const auto& problem : testSet) {
    REQUIRE(problem.isValid());
  }

  Problem p = kp2dGenerator.generate();
  REQUIRE(p.isValid());
  std::cout << p << '\n';
}

TEST_CASE("ProblemGenerationInvalid", "[problem][gen][error]") {
  using namespace kp2d;

  REQUIRE_THROWS(ProblemGenerator{
      {0, 0}, NormalInt::create(7.5, 0.5), UniformInt::create(3, 100)});

  REQUIRE_THROWS(ProblemGenerator{
      {-1, 0}, NormalInt::create(7.5, 0.5), UniformInt::create(3, 100)});

  ProblemGenerator faultyGenerator{
      {10, 10}, NormalInt::create(7.5, 0.5), UniformInt::create(40, 100)};

  REQUIRE_THROWS(faultyGenerator.generate());

  ProblemGenerator okGenerator{
      {500, 350}, NormalInt::create(7.5, 0.5), UniformInt::create(3, 100)};

  REQUIRE_NOTHROW(okGenerator.generateSet<RandomPlacementSolver>(10));
}

TEST_CASE("ProblemGenFixedGroupNumber", "[problem][gen][fixed]") {
  using namespace kp2d;

  ProblemGenerator generator{{700, 340},
                             UniformInt::create(7, 30),
                             NormalInt::create(60, 5),
                             NormalInt::create(70, 1)};

  IntType feat = generator.numOfFeatures();

  ProblemSet fixedSet = generator.generateSetFixed<RandomPlacementSolver>(5);

  auto fIter = fixedSet.begin();
  auto sIter = fixedSet.begin();
  REQUIRE(fIter->numOfFeatures() == feat);

  for (fIter++; fIter != fixedSet.end(); fIter++, sIter++) {
    REQUIRE(fIter->isValid());
    REQUIRE(fIter->numOfGroups() == sIter->numOfGroups());
    REQUIRE(fIter->numOfFeatures() == sIter->numOfFeatures());
    REQUIRE(fIter->numOfFeatures() == feat);

    // std::cout << *fIter << '\n';
    // std::cout << fIter->numOfFeatures() << '\n';
  }
}

TEST_CASE("CopyProblemGenerator", "[problem][gen][cpy]") {
  using namespace kp2d;

  ProblemGenerator g1{{700, 340},
                      UniformInt::create(7, 30),
                      NormalInt::create(60, 5),
                      NormalInt::create(70, 1)};

  g1.setNumOfGroups(5);

  ProblemGenerator g2{{700, 340},
                      UniformInt::create(7, 30),
                      NormalInt::create(60, 5),
                      NormalInt::create(70, 1)};

  ProblemGenerator g3{g1};

  REQUIRE(g1 == g3);
  REQUIRE_FALSE(g1 == g2);
}