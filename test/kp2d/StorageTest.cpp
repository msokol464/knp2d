#include <iostream>

#include "../../kp2d/include/kp2d/Generators.h"
#include "../../kp2d/include/kp2d/Storage.h"
#include "../include/catch.hpp"

TEST_CASE("StorageCtor", "[storage][ctor][cout]") {
  using namespace kp2d;
  using StorPtr = std::unique_ptr<kp2d::Storage>;

  StorPtr storage = Storage::create(
      {{11, 172, 5}, {8, 10, 100}, {4, 12, 65}, {12, 100, 10}}, {100, 200});

  for (const auto& gr : storage->rectGroups()) {
    std::cout << gr << '\n';
  }

  REQUIRE(storage->sumArea() == 32580);
}

SCENARIO("StorageSort", "[storage][sort][cout]") {
  using namespace kp2d;
  using StorPtr = std::unique_ptr<kp2d::Storage>;

  GIVEN("unsorted storage") {
    StorPtr storage = Storage::create(
        {{8, 10, 100}, {11, 172, 5}, {4, 12, 65}, {12, 100, 10}}, {100, 200});

    WHEN("no sorting is performed") {
      THEN("rectangles in storage are not sorted") {
        REQUIRE_FALSE(storage->isSorted(RectangleGroup::byArea));
        REQUIRE_FALSE(storage->isSorted(RectangleGroup::byWidth));
        REQUIRE_FALSE(storage->isSorted(RectangleGroup::byHeight));
        REQUIRE_FALSE(storage->isSorted(RectangleGroup::bySumArea));
      }
    }
    WHEN("sorting by area is called") {
      storage->sort(RectangleGroup::CompareByArea{});
      THEN("rectangles are sorted by area") {
        // std::cout << "\nRectangle group areas"
        //           << "\n=================================================\n";
        // for (const auto& gr : storage->rectGroups()) {
        //   std::cout << gr.area() << '\n';
        // }
        REQUIRE(storage->isSorted<RectangleGroup::CompareByArea>(
            RectangleGroup::byArea));
      }
    }

    WHEN("sorting by height is called") {
      storage->sort(RectangleGroup::CompareByHeight{});
      THEN("rectangles are sorted by height") {
        // std::cout << "\nSorted by height"
        //           << "\n=================================================\n";
        // for (const auto& gr : storage->rectGroups()) {
        //   std::cout << gr << '\n';
        // }
        REQUIRE(storage->isSorted(RectangleGroup::byHeight));
      }
    }

    WHEN("sorting by width is called") {
      storage->sort(RectangleGroup::CompareByWidth{});
      THEN("rectangles are sorted by width") {
        // std::cout << "\nSorted by width"
        //           << "\n=================================================\n";
        // for (const auto& gr : storage->rectGroups()) {
        //   std::cout << gr << '\n';
        // }
        REQUIRE(storage->isSorted(RectangleGroup::byWidth));
      }
    }
  }
}

TEST_CASE("StorageConsolidationEmpty", "[storage][consolidate][empty]") {
  using namespace kp2d;
  using StorPtr = std::unique_ptr<kp2d::Storage>;
  StorPtr storageA = Storage::create({}, {10, 20});
  StorPtr storageB = Storage::create({10, 20}, 120);

  REQUIRE_NOTHROW(storageA->consolidate());
  REQUIRE_NOTHROW(storageB->consolidate());
  REQUIRE(storageA->isEquiv(*storageB));
}

TEST_CASE("StorageConsolidation", "[storage][consolidate]") {
  using namespace kp2d;
  using StorPtr = std::unique_ptr<kp2d::Storage>;
  StorPtr storage = Storage::create({{10, 10, 100},
                                     {11, 12, 5},
                                     {10, 10, 65},
                                     {2, 12, 10},
                                     {2, 12, 13},
                                     {10, 10, 100},
                                     {10, 10, 100},
                                     {12, 11, 5}},
                                    {900, 200});

  StorPtr target =
      Storage::create({{10, 10, 365}, {12, 11, 10}, {2, 12, 23}}, {900, 200});

  storage->consolidate();
  std::cout << "=================================================\n";
  for (const auto& gr : storage->rectGroups()) {
    std::cout << gr << '\n';
  }
  REQUIRE(storage->isEquiv(*target));
}

TEST_CASE("StorageMinima", "[storage][mins]") {
  using namespace kp2d;
  using StorPtr = std::unique_ptr<kp2d::Storage>;
  StorPtr storage = Storage::create(
      {{5, 10, 10}, {3, 60, 5}, {1, 13, 65}, {2, 10, 10}, {20, 4, 13}},
      {900, 200});

  Point container{100, 250};

  StorageGenerator g{PointGenerator::create(UniformInt::create(5, 50),
                                            UniformInt::create(5, 90)),
                     UniformInt::create(1, 5)};

  std::unique_ptr<Storage> stor = g.generate(container);

  for (const auto& rg : stor->rectGroups()) {
    std::cout << rg << '\n';
  }
}

TEST_CASE("StorageEquivalence", "[stor][equiv]") {
  using kp2d::Storage;
  Storage st1{{{5, 10, 10}, {3, 60, 5}, {1, 13, 65}}, {900, 200}};
  Storage st2{{{5, 10, 10}, {3, 60, 5}, {1, 13, 65}}, {100, 200}};
  Storage st3{{{5, 10, 10}, {3, 60, 5}, {1, 12, 65}}, {900, 200}};
  Storage st4{{{5, 10, 10}, {3, 60, 5}, {1, 13, 65}, {12, 12, 12}}, {900, 200}};

  REQUIRE_FALSE(st1.isEquiv(st2));
  REQUIRE_FALSE(st1.isEquiv(st3));
  REQUIRE_FALSE(st1.isEquiv(st4));

  Storage st1Equiv1{{{5, 10, 10}, {3, 60, 5}, {1, 13, 65}}, {900, 200}};
  Storage st1Equiv2{{{10, 5, 10}, {3, 60, 5}, {1, 13, 65}}, {900, 200}};
  Storage st1Equiv3{{{5, 10, 6}, {3, 60, 5}, {1, 13, 4}}, {900, 200}};
  Storage st1Equiv4{{{3, 60, 5}, {1, 13, 4}, {5, 10, 6}}, {900, 200}};

  REQUIRE(st1.isEquiv(st1Equiv1));
  REQUIRE(st1.isEquiv(st1Equiv2));
  REQUIRE(st1.isEquiv(st1Equiv3));
  REQUIRE(st1.isEquiv(st1Equiv4));
}