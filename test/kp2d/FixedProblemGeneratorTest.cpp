#include <iostream>

#include "../../kp2d/include/kp2d/FixedProblemGenerator.h"
#include "../include/catch.hpp"

//   FixedProblemGenerator(const Point& c, const std::vector<Point>& sizes,
//                         std::unique_ptr<IntDistrib>&& numdistr,
//                         std::unique_ptr<IntDistrib>&& noise =
//                             std::unique_ptr<IntDistrib>(nullptr),
//                         FeatOption opt = FeatOption::allFeatures,
//                         SizeType ups = param::DefaultUnitsPerSample,
//                         bool areaConstraint = true);

TEST_CASE("FixedProblemGeneration", "[gen][fixed]") {
  using namespace kp2d;

  FixedProblemGenerator genOver{
      Point{1200, 600},
      {{100, 84}, {50, 10}, {320, 27}, {100, 100}, {20, 2}},
      NormalInt::create(32, 2),
      std::unique_ptr<IntDistrib>(nullptr),
      false};

  FixedProblemGenerator genUnder{
      Point{1200, 600},
      {{100, 84}, {50, 10}, {320, 27}, {100, 100}, {20, 2}},
      NormalInt::create(32, 2),
      std::unique_ptr<IntDistrib>(nullptr),
      true};

  Problem p1 = genOver.generate();
  std::cout << p1 << '\n';

  Problem p2 = genUnder.generate();
  std::cout << p2 << '\n';
}

TEST_CASE("NoisyProblemGeneration", "[gen][noisy]") {
  using namespace kp2d;

  FixedProblemGenerator genOver{
      Point{1200, 600},
      {{100, 84}, {50, 10}, {320, 27}, {100, 100}, {20, 2}},
      NormalInt::create(32, 2),
      NormalInt::create(0, 3),
      false};

  FixedProblemGenerator genUnder{
      Point{1200, 600},
      {{100, 84}, {50, 10}, {320, 27}, {100, 100}, {20, 2}},
      NormalInt::create(32, 2),
      NormalInt::create(0, 3),
      true};

  Problem p1 = genOver.generateNoisy();
  std::cout << p1 << '\n';

  Problem p2 = genUnder.generateNoisy();
  std::cout << p2 << '\n';
}

TEST_CASE("FixedSetGeneration", "[gen][fixed]") {
  using namespace kp2d;

  FixedProblemGenerator genOver{
      Point{1200, 600},
      {{100, 84}, {50, 230}, {320, 27}, {100, 100}, {20, 144}},
      NormalInt::create(32, 2),
      std::unique_ptr<IntDistrib>(nullptr),
      false};

  FixedProblemGenerator genUnder{
      Point{1200, 600},
      {{100, 84}, {50, 230}, {320, 27}, {100, 100}, {20, 144}},
      NormalInt::create(32, 2),
      std::unique_ptr<IntDistrib>(nullptr),
      true};

  ProblemSet<RandomPlacementSolver, defaultEvalType> set1 =
      genOver.template generateSet<RandomPlacementSolver, defaultEvalType>(10);
  std::cout << set1.toJson().dump(2) << '\n';

  ProblemSet<RandomPlacementSolver, defaultEvalType> set2 =
      genUnder.template generateSet<RandomPlacementSolver, defaultEvalType>(10);
  std::cout << set2.toJson().dump(2) << '\n';
}

// TEST_CASE("NoisyProblemGeneration", "[gen][noisy]") {}
