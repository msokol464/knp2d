
#include "../../kp2d/include/RectangleBinPack/MaxRectsBinPack.h"
#include "../../kp2d/include/kp2d/Generators.h"
#include "../../kp2d/include/kp2d/ProblemGenerator.h"
#include "../../kp2d/include/kp2d/SolvableSet.h"
#include "../include/catch.hpp"

TEST_CASE("RectangleBinPacker", "[maxrect]") {
  using kp2d::Point;
  using kp2d::Problem;
  using kp2d::ProblemSet;
  using HSet = kp2d::SolvableSet<rbp::MaxRectsBinPack,
                                 kp2d::EvalType::UnusedStorageArea>;
  using kp2d::Storage;
  // 16, 8 (x8)
  Point C{128, 64};
  std::vector<Problem> vec;

  Problem p1{
      C, Storage::create({{40, 20, 4}, {0, 0, 0}, {0, 0, 0}, {8, 8, 1}}, C)};
  vec.push_back(p1);
  // możliwe 97,65625% zapełnienia, pozostałe np.: {24, 40, 2}
  Problem p2{
      C, Storage::create({{24, 40, 9}, {32, 40, 1}, {0, 0, 0}, {0, 0, 0}}, C)};
  vec.push_back(p2);

  // możliwe 93,75% zapełnienia, pozostałe np.: {128, 128, 2}
  Problem p3{C, Storage::create(
                    {{128, 64, 2}, {16, 16, 10}, {32, 32, 5}, {0, 0, 0}}, C)};
  vec.push_back(p3);

  // możliwe 100 % zapełnienia
  // pozostałe np.: {128, 8, 1}, {16, 16, 2}, {24, 8, 2}
  Problem p4{C, Storage::create(
                    {{128, 8, 1}, {16, 16, 10}, {32, 32, 6}, {24, 8, 2}}, C)};
  vec.push_back(p4);

  kp2d::MaxRectHeur::set(
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectContactPointRule);
  rbp::MaxRectsBinPack maxRect;
  HSet pset{vec, maxRect};
  pset.solve();

  std::cout << pset.toJson().dump(2) << '\n';
}