#include <chrono>

#include "../../kp2d/include/kp2d/Generators.h"
#include "../../kp2d/include/kp2d/ProblemGenerator.h"
#include "../../kp2d/include/kp2d/ProblemSet.h"
#include "../../nnet/include/nnet/Network.h"
#include "../include/catch.hpp"

SCENARIO("ProblemSetCreation", "[set][ten]") {
  using namespace kp2d;

  WHEN("Problem set is created") {
    StorageGenerator sGen{PointGenerator::create(NormalInt::create(20, 30.5),
                                                 NormalInt::create(50, 0.6)),
                          NormalInt::create(10, 2)};

    Point rect{500, 300};

    std::vector<Problem> problems;
    problems.reserve(20);

    for (int i = 0; i < 10; i++) {
      Problem p{rect, sGen.generate(rect)};
      problems.push_back(std::move(p));
    }
    // for each problem in problems validate initial state
    ProblemSet<RandomPlacementSolver> testSet(std::move(problems));

    THEN("it has valid problem instnaces") {
      for (auto& problem : testSet) {
        // std::cout << problem << '\n';

        /// NOTE: default action is printing to cerr - no throwing'll occur
        /// TODO: Need to improve testing
        // possibility one: expect returning bool then require true
        REQUIRE(problem.isValid());
      }
    }

    AND_WHEN("solve function is called") {
      RandomPlacementSolver randSolver;

      testSet.solveWith(randSolver);

      THEN("rectangles are placed and problems are valid") {
        for (auto& problem : testSet) {
          // std::cout << problem << '\n';

          /// NOTE: default action is printing to cerr - no throwing'll occur
          /// TODO: Need to improve testing
          // possibility one: expect returning bool then require true
          REQUIRE(problem.isValid());
          REQUIRE(problem.solution().size() > 0);
          REQUIRE(problem.validateSolution());
        }
      }

      AND_THEN("best solution can be written to file") {
        std::string solutionFile("data/out/kp2dExample.txt");
        testSet.logBestSolution(solutionFile);
        std::ifstream check(solutionFile.c_str());
        REQUIRE(check.good());
      }
    }
  }
}

TEST_CASE("ProblemSetOfOne", "[set][one][timing]") {
  using namespace kp2d;
  StorageGenerator sGen{PointGenerator::create(NormalInt::create(20, 30.5),
                                               NormalInt::create(50, 0.6)),
                        NormalInt::create(20, 10)};

  Point rect{800, 500};

  std::vector<Problem> problems;
  problems.reserve(20);

  Problem p{rect, sGen.generate(rect)};
  problems.push_back(std::move(p));

  // for each problem in problems validate initial state
  ProblemSet<RandomPlacementSolver> testSet(std::move(problems));

  RandomPlacementSolver rand;

  using namespace std::chrono;
  auto start = high_resolution_clock::now();
  testSet.solveWith(rand);
  auto stop = high_resolution_clock::now();
  auto duration = duration_cast<seconds>(stop - start);

  std::cout << std::string(32, '=') << "\nProblem instance\n"
            << "solved in " << duration.count() << " seconds\n";

  for (auto& problem : testSet) {
    std::cout << problem << "\n" << problem.solutionToString() << '\n';

    /// NOTE: default action is printing to cerr - no throwing'll occur
    /// TODO: Need to improve testing
    // possibility one: expect returning bool then require true
    REQUIRE(problem.isValid());
  }
}

TEST_CASE("ProblemSetFixed", "[set][one][fixed][timing]") {
  using namespace kp2d;
  Point rect{800, 500};

  std::vector<Problem> problems;
  problems.reserve(20);

  Problem p{rect,
            Storage::create(
                {{35, 70, 1}, {420, 10, 1}, {60, 100, 1}, {70, 70, 1}}, rect)};
  problems.push_back(std::move(p));

  // for each problem in problems validate initial state
  ProblemSet<RandomPlacementSolver> testSet(std::move(problems));

  RandomPlacementSolver rand;

  using namespace std::chrono;
  auto start = high_resolution_clock::now();
  testSet.solveWith(rand);
  auto stop = high_resolution_clock::now();
  auto duration = duration_cast<seconds>(stop - start);

  std::cout << std::string(32, '=') << "\nProblem instance\n"
            << "solved in " << duration.count() << " seconds\n";

  for (const auto& problem : testSet) {
    std::cout << problem << "\n" << problem.solutionToString() << '\n';

    /// NOTE: default action is printing to cerr - no throwing'll occur
    /// TODO: Need to improve testing
    // possibility one: expect returning bool then require true
    REQUIRE(problem.isValid());
    REQUIRE(problem.validateSolution());
  }
}

TEST_CASE("CreateNetwork", "[net]") {
  using namespace nnet;

  std::random_device rd;
  std::mt19937 mt{rd()};  // not all that random

  Network myNet{{{20, 7, nnet::Activation::Type::TANH},
                 {7, 1, nnet::Activation::Type::ID}}};
  REQUIRE(myNet.isValid());

  // std::shared_ptr<nnet::Network> netPtr = std::shared_ptr<nnet::Network>(
  //     new nnet::Network{{{20, 7, nnet::Activation::Type::TANH},
  //                        {7, 1, nnet::Activation::Type::ID}},
  //                       nnet::DenseLayer::InitType::UniformV2,
  //                       mt});

  std::shared_ptr<nnet::Network> netPtr = std::shared_ptr<nnet::Network>(
      new nnet::Network{{{20, 7, nnet::Activation::Type::TANH},
                         {7, 1, nnet::Activation::Type::ID}}});

  REQUIRE(netPtr);
  REQUIRE(netPtr->isValid());
}

TEST_CASE("ProblemSetWithNetork", "[set][one][net][timing]") {
  using namespace kp2d;
  Point rect{500, 300};

  std::vector<Problem> problems;
  problems.reserve(3);
  REQUIRE(problems.data() != nullptr);
  REQUIRE(problems.empty());

  StorageGenerator storageMaker{
      PointGenerator::create(NormalInt::create(45, 10)),
      NormalInt::create(6, 5)};

  Problem p{rect, storageMaker.generate(rect)};

  REQUIRE(p.isValid());

  IntType nFeatures = p.numOfFeatures();
  // IntType l1inDim = (nFeatures + 1) * 2 / 3;

  problems.push_back(std::move(p));

  // for each problem in problems validate initial state
  ProblemSet<nnet::Network> testSet(std::move(problems));
  REQUIRE(testSet.isUnsolved());

  std::random_device rd;
  std::mt19937 mt{rd()};  // not all that random

  // std::cout << nFeatures << '\t' << l1inDim << '\n';

  nnet::Network net{{{nFeatures, 100, nnet::Activation::Type::TANH},
                     {100, 1, nnet::Activation::Type::ID}}};

  using namespace std::chrono;
  auto start = high_resolution_clock::now();
  testSet.solveWith(net);
  auto stop = high_resolution_clock::now();
  auto duration = duration_cast<seconds>(stop - start);

  std::cout << std::string(32, '=') << "\nProblem instance\n"
            << "solved in " << duration.count() << " seconds\n";

  // for (auto& problem : testSet) {
  //   std::cout << problem << "\n" << problem.solutionToString() << '\n';

  //   /// NOTE: default action is printing to cerr - no throwing'll occur
  //   /// TODO: Need to improve testing
  //   // possibility one: expect returning bool then require true
  //   REQUIRE(problem.isValid());
  // }
}

TEST_CASE("ProblemSetEquivalence", "[equiv][set]") {
  using namespace kp2d;

  ProblemGenerator gen{
      {500, 350}, NormalInt::create(7.5, 0.5), UniformInt::create(3, 100)};

  ProblemSet<RandomPlacementSolver> s1 =
      gen.generateSet<RandomPlacementSolver>(10u);
  ProblemSet<RandomPlacementSolver> s2 =
      gen.generateSet<RandomPlacementSolver>(10u);
  ProblemSet<RandomPlacementSolver> sEquiv = s1;

  // REQUIRE_FALSE(s1.isEquiv(s2));
  // REQUIRE(s1.isEquiv(sEquiv));

  RandomPlacementSolver rndS;
  s1.solveWith(rndS);

  // REQUIRE_FALSE(s1.isEquiv(s2));
  REQUIRE(s1.isEquiv(sEquiv));
}
