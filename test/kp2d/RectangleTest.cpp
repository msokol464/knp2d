#include "../../kp2d/include/kp2d/Rectangle.h"
#include "../include/RangeMatchers.h"
#include "../include/catch.hpp"

TEST_CASE("RectangleRotation", "[basic][rect]") {
  using Rect = kp2d::Rectangle;
  Rect b{{100, 102}, {2, 0}};
  Rect c{{400, 25}, {102, 0}};
  Rect bRot{{102, 100}, {2, 0}};

  REQUIRE(b.area() != c.area());
  REQUIRE(b.area() == bRot.area());

  b.rotate();
  // REQUIRE(b.isRotated() == true);
  REQUIRE(b == bRot);

  b.rotate();
  // REQUIRE(b.isRotated() == false);
  REQUIRE(b != bRot);
  REQUIRE(b != c);
}

TEST_CASE("RectangleOrientations", "[orient][rect]") {
  using Rect = kp2d::Rectangle;

  Rect a{{10, 32}, {100, 120}};
  auto arr = Rect::getOrientations(a);
  std::array<Rect, 2> target = {Rect{{10, 32}, {100, 120}},
                                Rect{{32, 10}, {100, 120}}};

  REQUIRE_THAT(arr, EqualsRange(target));
}

/// TODO: Repair test, should throw error
TEST_CASE("RectangleError", "[error][rect]") {
  using Rect = kp2d::Rectangle;
  using Catch::Matchers::StartsWith;

  Rect a{{-10, -20}, {-30, -40}};

  std::cout << a << '\n';

  // REQUIRE_THROWS_WITH((Rect{{-10, 0}, {100, 120}}), "Created faulty
  // rectangle");
}