#include <iostream>

#include "../../kp2d/include/kp2d/ProblemGenerator.h"
#include "../../kp2d/include/kp2d/SolvableSet.h"
#include "../../nnet/include/nnet/Network.h"
#include "../../nnet/include/nnet/ObjFunction.h"
#include "../include/catch.hpp"

SCENARIO("ObjFunctionCall", "[opt]") {
  GIVEN("Problem generator") {
    using kp2d::ProblemGenerator;
    ProblemGenerator kpSource{
        kp2d::Point{200, 500}, kp2d::UniformInt::create(2, 12),
        kp2d::NormalInt::create(20, 3.0), kp2d::NormalInt::create(42, 2.1)};

    WHEN("Calling number of features mulitiple times") {
      kp2d::IntType a = kpSource.numOfFeatures();
      kp2d::IntType b = kpSource.numOfFeatures();
      kp2d::IntType c = kpSource.numOfFeatures();

      THEN("We get the same answear") {
        REQUIRE(a == b);
        REQUIRE(a == c);
      }

      AND_WHEN("Generating problems") {
        using kp2d::Problem;
        Problem p1 = kpSource.generate();
        Problem p2 = kpSource.generate();
        THEN("They have the same number of rectangle groups") {
          REQUIRE(p1.numOfGroups() == p2.numOfGroups());
          REQUIRE(p1.numOfFeatures() == a);
          REQUIRE(p2.numOfFeatures() == a);
        }
      }
    }

    AND_GIVEN("Network") {
      using nnet::Activation;
      using nnet::DenseLayer;
      using nnet::Network;
      unsigned seed = 12u;
      std::ranlux48 rnd{seed};

      std::cout << "[SCENARIO::opt->ProblemGenerator] number of features: "
                << kpSource.numOfFeatures() << '\n';

      Network net{{{kpSource.numOfFeatures(), 30, Activation::Type::RELU},
                   {30, 1, Activation::Type::RELU}}};

      REQUIRE(net.isValid());

      std::cout << "[SCENARIO::opt->ProblemGenerator] number of groups: "
                << kpSource.numOfFeatures() << '\n';

      AND_GIVEN("Solvable problem set") {
        using SolvableSet = kp2d::SolvableSet<Network>;
        SolvableSet set(kpSource.generateSetFixed<Network>(5), net);
        REQUIRE(set.isUnsolved());

        std::cout << "[ProblemSet]\nfeatures\tgroups\n"
                  << set.numOfFeatures() << '\n';

        WHEN("Creating ObjFunction callable object for that set") {
          using nnet::ObjFunction;
          ObjFunction objective{set, 6};

          THEN(
              "Calling it with vector of size equal to number of net "
              "parameters will trigger solving problems with net") {
            REQUIRE_NOTHROW(
                objective(net.getParamVec().data(), net.numOfParams()));
          }

          // THEN(
          //     "Calling it with vector of different size than number of net "
          //     "parameters") {
          //   std::vector<FloatType> v(10, 3.141592);
          //   REQUIRE_THROWS(objective(v.data(), 10));
          // }

          // THEN("Calling it with vector and number of dims mismatched") {
          //   std::vector<FloatType> v(10, 3.141592);
          //   objective(net.getParamVec().data(), net.numOfParams() + 10);
          // }
        }
      }
    }
  }
}
