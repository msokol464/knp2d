#pragma once

#include "../../kp2d/include/kp2d/ProblemGenerator.h"
#include "../../kp2d/include/kp2d/SolvableSet.h"
#include "../../nnet/include/nnet/Network.h"

using kp2d::ProblemGenerator;
using kp2d::SolvableSet;
using nnet::Activation;
using nnet::DenseLayer;
using nnet::Network;

class NetSetFixture {
 private:
  /// @brief

 protected:
  ProblemGenerator kpSource;
  Network net;
  SolvableSet<Network> set;

 public:
  NetSetFixture()
      : kpSource{kp2d::Point{200, 500}, kp2d::UniformInt::create(2, 12),
                 kp2d::NormalInt::create(20, 3.0),
                 kp2d::NormalInt::create(42, 2.1)} {
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::ranlux48 rnd{seed};
    // net = Network{{{kpSource.numOfFeatures(), 72, Activation::Type::RELU},
    //                {72, 1, Activation::Type::RELU}},
    //               DenseLayer::InitType::Zeros,
    //               rnd};

    net = Network{{{kpSource.numOfFeatures(), 72, Activation::Type::RELU},
                   {72, 1, Activation::Type::RELU}}};

    set = SolvableSet{kpSource.generateSetFixed<Network>(8), net};
  }

  ~NetSetFixture() {}
};
