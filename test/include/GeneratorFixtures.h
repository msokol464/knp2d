#pragma once

#include "../../kp2d/include/kp2d/CutProblemGenerator.h"
#include "../../kp2d/include/kp2d/FixedProblemGenerator.h"
#include "../../kp2d/include/kp2d/ProblemGenerator.h"

class PGeneratorNUU {
 public:
  PGeneratorNUU()
      : generator{kp2d::Point{1200, 800}, kp2d::NormalInt::create(10, 3),
                  kp2d::UniformInt::create(0.2 * 1200, 0.8 * 800),
                  kp2d::UniformInt::create(0.2 * 1200, 0.8 * 800),
                  kp2d::FeatOption::allFeatures} {}

  ~PGeneratorNUU() {}

 protected:
  kp2d::ProblemGenerator generator;
};

// class FGeneratorN03 {
//  public:
//   FGeneratorN03()
//       : generator{
//             kp2d::Point{1200, 800},
//             {{320, 48}, {60, 120}, {144, 30}, {64, 64}, {10, 20}, {110, 90}},
//             kp2d::NormalInt::create(10, 3),
//             kp2d::NormalInt::create(0, 3),
//             kp2d::FeatOption::allFeatures} {}

//   ~FGeneratorN03(){};

//  protected:
//   kp2d::FixedProblemGenerator generator;
// };