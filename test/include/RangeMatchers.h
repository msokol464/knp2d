#include "catch.hpp"

template <typename T>
struct EqualsRangeMatcher : public Catch::Matchers::Impl::MatcherBase<T> {
 private:
  const T& range_;

 public:
  EqualsRangeMatcher(const T& range) : range_(range) {}

  bool match(T const& other) const override {
    using std::begin;
    using std::end;
    return std::equal(begin(range_), end(range_), begin(other), end(other));
  }

  std::string describe() const override {
    std::ostringstream ss;
    ss << "Equals: " << Catch::rangeToString(range_) << '\n';
    return ss.str();
  }
};

template <typename T>
auto EqualsRange(const T& range) -> EqualsRangeMatcher<T> {
  return {range};
}
