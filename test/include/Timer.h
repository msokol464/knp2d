#pragma once

#include <chrono>
#include <vector>

using Clock = std::chrono::high_resolution_clock;
using DurationMicro = std::chrono::duration<double, std::micro>;
using Time = std::chrono::time_point<Clock, DurationMicro>;

class Timer {
 private:
  Time start_;
  Time end_;
  // DurationMicro lastMeasured_;
  std::vector<DurationMicro> times_;

 public:
  Timer() {}

  const std::vector<DurationMicro>& measurments() { return times_; }
  size_t numOfMeasurments() const { return times_.size(); }

  void start() { start_ = Clock::now(); }
  void measure() {
    end_ = std::chrono::high_resolution_clock::now();
    DurationMicro lastMeasured = end_ - start_;
    times_.push_back(lastMeasured);
    start_ = end_;
  }

  long getSec(size_t idx) const {
    if (idx >= times_.size()) return 0;
    return std::chrono::duration_cast<std::chrono::seconds>(times_[idx])
        .count();
  }

  long getMilisec(size_t idx) const {
    if (idx >= times_.size()) return 0;
    return std::chrono::duration_cast<std::chrono::milliseconds>(times_[idx])
        .count();
  }

  long getMicrosec(size_t idx) const {
    if (idx >= times_.size()) return 0;
    return std::chrono::duration_cast<std::chrono::microseconds>(times_[idx])
        .count();
  }

  // long getAvgS()
  // long getAvg
  // template<class TimeUnit>
  // long getAvg() {}

  Timer(const Timer&) = default;
  Timer(Timer&&) = default;
  Timer& operator=(const Timer&) = default;
  Timer& operator=(Timer&&) = default;
  ~Timer() = default;
};
