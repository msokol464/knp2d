#pragma once

#include "../../kp2d/include/kp2d/ProblemGenerator.h"
#include "../../kp2d/include/kp2d/SolvableSet.h"
#include "../../nnet/include/nnet/Network.h"

using kp2d::NormalInt;
using kp2d::Point;
using kp2d::ProblemGenerator;
using SolvableSetNet = kp2d::SolvableSet<nnet::Network>;

using nnet::Activation;
using nnet::DenseLayer;
using nnet::Network;

class SolvableSetFixture {
 public:
  SolvableSetFixture(/* args */);
  ~SolvableSetFixture() {}

 protected:
  SolvableSetNet testSet;
  kp2d::SizeType featNum;

 private:
  /* data */
};

SolvableSetFixture::SolvableSetFixture(/* args */) {
  unsigned seed = 120u;
  std::ranlux48 rnd{seed};
  ProblemGenerator pg{Point{800, 1200}, NormalInt::create(10, 3),
                      NormalInt::create(300, 10), NormalInt::create(80, 5)};

  featNum = pg.numOfFeatures();
  Network net{
      {{featNum, 30, Activation::Type::SMAX}, {30, 1, Activation::Type::RELU}}};
  testSet = SolvableSetNet(pg.generateSetFixed<Network>(3), net);
}
