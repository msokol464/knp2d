#include "../../nnet/include/nnet/Activations.h"
#include "../include/catch.hpp"

TEST_CASE("IdentityActivation", "[activ][nnet][id]") {
  using namespace nnet;

  DynVector v1{7};

  v1 << 1, 2, 3, 4, 5, 6, 7;

  Identity identity;

  DynVector v2 = identity.apply(v1);

  REQUIRE(v1 == v2);
}

TEST_CASE("ReLUActivation", "[activ][nnet][relu]") {
  using namespace nnet;

  Eigen::Matrix<FloatType, 7, 1> v1;

  v1 << 1, 2, -3, 4, -5, 6, 7;

  ReLU relu;

  DynVector v2 = relu.apply(v1);
  DynVector target{7};
  target << 1, 2, 0, 4, 0, 6, 7;

  REQUIRE(v2 == target);
}

TEST_CASE("ActivationFactory", "[activ][factory]") {
  using namespace nnet;

  std::unique_ptr<Activation> sigm = Activation::create(Activation::Type::SIGM);
  REQUIRE(sigm->toString() == "Sigmoid");

  std::unique_ptr<Activation> softMax =
      Activation::create(Activation::Type::SMAX);
  REQUIRE(softMax->toString() == "SoftMax");

  std::unique_ptr<Activation> unknown =
      Activation::create(Activation::Type(200));
  REQUIRE(unknown == nullptr);
}
