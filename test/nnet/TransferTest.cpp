#include "../../kp2d/include/kp2d/CutProblemGenerator.h"
#include "../../kp2d/include/kp2d/FixedProblemGenerator.h"
#include "../../kp2d/include/kp2d/ProblemGenerator.h"
#include "../../kp2d/include/kp2d/SolvableSet.h"
#include "../../nnet/include/nnet/Network.h"
#include "../../nnet/include/nnet/ObjFunction.h"
#include "../../nnet/include/nnet/Optimizer.h"
#include "../../nnet/include/nnet/ProgFunction.h"
#include "../include/GeneratorFixtures.h"
#include "../include/Timer.h"
#include "../include/catch.hpp"

// TEST_CASE("TransferStage1", "[integration][transfer][base]") {}

/// BRIEF: TransferStage2
/// - loading trained network
/// - creating or generating ProblemSet
/// - put net and set into SolvableSet
/// - create Optimizer with no task switching ProgFunction
TEST_CASE_METHOD(PGeneratorNUU, "TransferStage2gen",
                 "[integration][transfer][end][generated]") {
  // Loading network
  nnet::Network net = nnet::Network::loadFromJson(
      "data/out/num_problems_in_set/validation_128/3/b_net.json");

  // Preparing generator
  // IMPORTANT: number of groups must match
  using namespace kp2d;
  RndEngine::setSeed(99u);
  int numberOfGroups = 6;
  int numberOfProblems = 1;
  //   int W = 1200;
  //   int H = 800;

  generator.setNumOfGroups(numberOfGroups);

  // Creating set
  const EvalType eval = EvalType::UnusedStorageArea;
  using PSet = ProblemSet<nnet::Network, eval>;
  using SSet = SolvableSet<nnet::Network, eval>;
  PSet problems = generator.generateSet<nnet::Network, eval>(numberOfProblems,
                                                             numberOfGroups);

  SSet initSet{problems, net};

  // Creating objective and progress functions
  using Objective = nnet::ObjFunction<SSet>;
  using Progress = nnet::ProgFunction<PSet, Objective, cma::NoBoundStrategy>;
  Objective obj{initSet, 8};
  Progress prog{obj};

  // Setting up Optimizer
  using CMAOptimizer = nnet::Optimizer<Objective, cma::NoBoundStrategy>;
  CMAOptimizer opt{std::move(obj), 0.1, prog, -1, 500};

  // Running optimization
  opt.run();
  opt.print();

  nnet::Network rNet = opt.getResult();
  problems.solveWith(rNet);

  std::ofstream sol{
      "data/out/num_problems_in_set/validation_128/3/"
      "transfer_test_1_500.json",
      std::ios_base::out | std::ios_base::trunc};
  sol << problems.toJson().dump(2) << '\n';
  sol.close();

  std::ofstream result{
      "data/out/num_problems_in_set/validation_128/3/"
      "net_transfer_1_500.json",
      std::ios_base::out | std::ios_base::trunc};
  result << rNet.toJson().dump(2) << '\n';
  result.close();
}

// TEST_CASE("TransferStage2manual", "[integration][transfer][end][manual]") {}

TEST_CASE_METHOD(PGeneratorNUU, "NoTransferOneTask",
                 "[integration][transfer][cmp]") {
  using namespace kp2d;

  RndEngine::setSeed(99u);
  int numberOfGroups = 6;
  int numberOfProblems = 1;
  generator.setNumOfGroups(numberOfGroups);

  // Creating set
  const EvalType eval = EvalType::UnusedStorageArea;
  using PSet = ProblemSet<nnet::Network, eval>;
  using SSet = SolvableSet<nnet::Network, eval>;
  //   using HSet = SolvableSet<kp2d::MaxRectHeur, eval>;
  PSet problems = generator.generateSet<nnet::Network, eval>(numberOfProblems,
                                                             numberOfGroups);

  // Network construction
  std::mt19937 mt{1001u};
  int inputDim = problems.numOfFeatures();
  int hidden1 = 20;

  using Objective = nnet::ObjFunction<SSet>;
  using Progress = nnet::ProgFunction<PSet, Objective, cma::NoBoundStrategy>;
  using CMAOptimizer = nnet::Optimizer<Objective, cma::NoBoundStrategy>;
  std::ofstream stats;

  stats.open("data/out/specific_task/NUU1/rand/data_99_600.csv",
             std::ios_base::out | std::ios_base::trunc);
  stats << "Optim_max_eval,Evaluation,Optiization_ms,Solution_ms\n";
  stats.close();
  int mEvals = 600;
  for (int i = 0; i < 500; i++) {
    // if (i % 50 == 0) {
    //   mEvals += 50;
    // }

    PSet task = generator.generateSet<nnet::Network, eval>(numberOfProblems,
                                                           numberOfGroups);

    nnet::Network net{{{inputDim, hidden1, nnet::Activation::Type::SIGM},
                       {hidden1, 1, nnet::Activation::Type::SIGM}}};

    SSet initSet{problems, net};

    // Creating objective and progress functions
    Objective obj{initSet, 8};
    Progress prog{obj};

    // Setting up Optimizer
    CMAOptimizer opt{std::move(obj), 0.1, prog, -1, mEvals};

    // Running optimization
    opt.run();
    opt.print();

    nnet::Network rNet = opt.getResult();

    Timer timer;
    timer.start();
    double eval = task.solveWith(rNet);
    timer.measure();

    stats.open("data/out/specific_task/NUU1/rand/data_99_600.csv",
               std::ios_base::out | std::ios_base::app);

    stats << mEvals << ',' << eval << ',' << opt.optimizationTime() << ','
          << timer.getMilisec(0) << '\n';
    stats.close();
  }

  //   std::ofstream sol{
  //       "data/out/num_problems_in_set/validation_128/no_base/"
  //       "net_task_99_200.json",
  //       std::ios_base::out | std::ios_base::trunc};
  //   sol << problems.toJson().dump(2) << '\n';
  //   sol.close();

  //   std::ofstream result{
  //       "data/out/num_problems_in_set/validation_128/no_base/"
  //       "task_99_200.json",
  //       std::ios_base::out | std::ios_base::trunc};
  //   result << rNet.toJson().dump(2) << '\n';
  //   result.close();
}