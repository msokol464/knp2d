#include <cmath>
#include <iostream>
// #include <thread>

#include "../../kp2d/include/kp2d/CutProblemGenerator.h"
#include "../../kp2d/include/kp2d/FixedProblemGenerator.h"
#include "../../kp2d/include/kp2d/ProblemGenerator.h"
#include "../../kp2d/include/kp2d/SolvableSet.h"
#include "../../nnet/include/nnet/Network.h"
#include "../../nnet/include/nnet/ObjFunction.h"
#include "../../nnet/include/nnet/Optimizer.h"
#include "../../nnet/include/nnet/ValFunction.h"
#include "../include/Timer.h"
#include "../include/catch.hpp"

TEST_CASE("Validation01", "[valid][small][nobound][normal]") {
  using namespace kp2d;

  /// Problem sets generation to-dos
  //  -problem generator
  //  -initial problem set
  //  -validation problem set
  //  -problem set vector for replacement

  // Training setup
  // number of groups:
  size_t numGroups = 8;
  // problems per set:
  size_t numProblems = 10;
  // problems in validation:
  size_t numPValid = 5;
  // number of replacement sets:
  size_t numTrainigSets = 199;

  ProblemGenerator genMN{Point{1200, 800}, UniformInt::create(5, 16),
                         NormalInt::create(100, 64),
                         NormalInt::create(250, 100)};

  using PSet = ProblemSet<nnet::Network, EvalType::UnusedStorageArea>;
  using SSet = SolvableSet<nnet::Network, EvalType::UnusedStorageArea>;

  // Training sets
  std::vector<PSet> replSets;
  for (size_t i = 0; i < numTrainigSets; i++) {
    PSet set = genMN.generateSet<nnet::Network, EvalType::UnusedStorageArea>(
        numProblems, numGroups);
    set.sort(RectangleGroup::byWidthHeight);
    replSets.push_back(set);
  }

  // Network construction
  std::mt19937 mt{1001u};

  int inputDim = replSets[0].numOfFeatures();
  nnet::Network net{{{inputDim, 24, nnet::Activation::Type::TANH},
                     {24, 8, nnet::Activation::Type::TANH},
                     {8, 1, nnet::Activation::Type::ID}}};
  std::cout << "Network setup\n"
            << inputDim << " input dimensions\n"
            << net.numOfParams() << " number of parameters\n";

  // Initialization set
  SSet initSet{genMN.generateSet<nnet::Network, EvalType::UnusedStorageArea>(
                   numProblems, numGroups),
               net};
  initSet.sort(RectangleGroup::byWidthHeight);

  // Validation set
  SSet validSet{genMN.generateSet<nnet::Network, EvalType::UnusedStorageArea>(
                    numPValid, numGroups),
                net};
  validSet.sort(RectangleGroup::byWidthHeight);

  /// Optimization setup
  using Objective = nnet::ObjFunction<SSet>;
  using Optimizer = nnet::Optimizer<Objective, cma::NoBoundStrategy>;
  using Validation =
      nnet::ValFunction<SSet, PSet, Objective, cma::NoBoundStrategy>;

  Objective objectiveFun{initSet, 8};
  Validation validation{objectiveFun,
                        validSet,
                        10,
                        replSets,
                        5,
                        "data/out/test11/validation.txt",
                        "data/out/test11",
                        "data/out/test11/resultNet.json"};

  Optimizer cmaOpt(std::move(objectiveFun), 0.1, validation, -1, 50000);

  cmaOpt.run();

  ///
}

TEST_CASE("Validation02", "[valid][small][bound][normal]") {
  using namespace kp2d;

  // Training setup
  // number of groups:
  int numGroups = 5;
  // problems per set:
  size_t numProblems = 4;
  // problems in validation:
  size_t numPValid = 8;
  // number of replacement sets:
  size_t numTrainigSets = 6000;

  // Generator setup in constructor:
  //  -container
  //  - ???
  //  - ???
  //  -distribution for number of elements in group - N(avg, variance)
  //  -area option
  CutProblemGenerator gen{Point{1200, 640}, 34, 25, NormalInt::create(10, 3),
                          numGroups};

  using PSet = ProblemSet<nnet::Network, EvalType::UnusedContainerPortion>;
  using SSet = SolvableSet<nnet::Network, EvalType::UnusedContainerPortion>;

  // Training sets
  std::vector<PSet> replSets;
  for (size_t i = 0; i < numTrainigSets; i++) {
    PSet set = gen.generateSet<nnet::Network, EvalType::UnusedContainerPortion>(
        numProblems, CutProblemGenerator::AreaOption::AreaOverflow);
    set.sort(RectangleGroup::byWidthHeight);
    replSets.push_back(set);
  }

  // Network construction
  std::mt19937 mt{1001u};
  int inputDim = replSets[0].numOfFeatures();
  int hiddenDim = 2 * int(std::sqrt(inputDim));
  nnet::Network net{{{inputDim, hiddenDim, nnet::Activation::Type::TANH},
                     {hiddenDim, 1, nnet::Activation::Type::ID}}};

  std::cout << "Network setup\n"
            << inputDim << " input dimensions\n"
            << hiddenDim << " hidden dimensions\n"
            << net.numOfParams() << " number of parameters\n";

  // Initialization set
  SSet initSet{gen.generateSet<nnet::Network, EvalType::UnusedContainerPortion>(
                   numProblems, CutProblemGenerator::AreaOption::AreaOverflow),
               net};
  initSet.sort(RectangleGroup::byWidthHeight);

  // Validation set
  SSet validSet{
      gen.generateSet<nnet::Network, EvalType::UnusedContainerPortion>(
          numPValid, CutProblemGenerator::AreaOption::AreaOverflow),
      net};
  validSet.sort(RectangleGroup::byWidthHeight);

  /// Optimization setup
  using Objective = nnet::ObjFunction<SSet>;
  using Optimizer = nnet::Optimizer<Objective, cma::pwqBoundStrategy>;
  using Validation =
      nnet::ValFunction<SSet, PSet, Objective, cma::pwqBoundStrategy>;

  Objective objectiveFun{initSet, 8};
  Validation validation{objectiveFun,
                        validSet,
                        8,
                        replSets,
                        12,
                        "data/out/test21/validation.txt",
                        "data/out/test21",
                        "data/out/test21/resultNet.json"};

  Optimizer cmaOpt(std::move(objectiveFun), -1, validation, -1, 500000);

  cmaOpt.run();

  ///
}

TEST_CASE("TrainingGenerator", "[valid][rand][all]") {
  using namespace kp2d;

  // Training setup
  // number of groups:
  int numGroups = 6;
  // problems per set:
  size_t numProblems = 1;
  // problems in validation:
  size_t numPValid = 128;
  int viter = 16;
  // number of replacement sets:
  size_t numTrainigSets = 5000;
  int riter = 1;
  const EvalType eval = EvalType::UnusedStorageArea;

  // Generator setup in constructor:
  //  -container
  //  -distribution for number of elements in group - N(avg, variance)
  //  -rectangle width distribution
  //  -rectangle height distribution
  int W = 1200;
  int H = 800;
  ProblemGenerator gen{Point{W, H}, NormalInt::create(10, 3),
                       UniformInt::create(0.2 * W, 0.8 * W),
                       UniformInt::create(0.2 * H, 0.8 * H),
                       FeatOption::allFeatures};
  gen.setNumOfGroups(numGroups);

  using PSet = ProblemSet<nnet::Network, eval>;
  using SSet = SolvableSet<nnet::Network, eval>;

  // Training sets
  std::vector<PSet> replSets;
  for (size_t i = 0; i < numTrainigSets; i++) {
    PSet set = gen.generateSet<nnet::Network, eval>(numProblems);
    // set.sort(RectangleGroup::byWidthHeight);
    std::cout << "Generated task " << i << "\n";
    replSets.push_back(set);
  }

  // Network construction
  std::mt19937 mt{1001u};
  int inputDim = replSets[0].numOfFeatures();
  // int hidden1 = 2 * int(std::sqrt(inputDim)) + 2;
  int hidden1 = 20;
  //   int hidden2 = int(std::sqrt(inputDim));

  //   nnet::Network net{{{inputDim, hidden1, nnet::Activation::Type::RELU},
  //                      {hidden1, hidden2, nnet::Activation::Type::RELU},
  //                      {hidden2, 1, nnet::Activation::Type::ID}},
  //                     nnet::DenseLayer::InitType::Zeros,
  //                     mt};

  nnet::Network net{{{inputDim, hidden1, nnet::Activation::Type::SIGM},
                     {hidden1, 1, nnet::Activation::Type::SIGM}}};

  std::cout << "Network setup\n"
            << inputDim << " input dimensions\n"
            << hidden1 << " hidden dimensions\n"
            << net.numOfParams() << " number of parameters\n";

  // Initialization set
  SSet initSet{gen.generateSet<nnet::Network, eval>(numProblems), net};
  //   initSet.sort(RectangleGroup::byWidthHeight);

  // Validation set
  SSet validSet{gen.generateSet<nnet::Network, eval>(numPValid), net};
  //   validSet.sort(RectangleGroup::byWidthHeight);

  std::ofstream task{
      "data/out/num_problems_in_set/validation_128/1_2/v_task.json",
      std::ios_base::out | std::ios_base::trunc};
  validSet.saveToJson(task);

  /// Optimization setup
  using Objective = nnet::ObjFunction<SSet>;
  using Optimizer = nnet::Optimizer<Objective, cma::pwqBoundStrategy>;
  using Validation =
      nnet::ValFunction<SSet, PSet, Objective, cma::pwqBoundStrategy>;

  Objective objectiveFun{initSet, 8};
  Validation validation{objectiveFun,
                        validSet,
                        viter,
                        replSets,
                        riter,
                        "validation.txt",
                        "data/out/num_problems_in_set/validation_128/1_2",
                        "net.json"};

  Optimizer cmaOpt(std::move(objectiveFun), -1, validation, -1, 500000);

  cmaOpt.run();
  cmaOpt.print();

  // nnet::Network rNet = cmaOpt.getResult();
  // PSet set1{gen.generateSet<nnet::Network, eval>(numPValid)};
  // set1.solveWith(rNet);

  // PSet set2{gen.generateSet<nnet::Network, eval>(numPValid)};
  // set2.solveWith(rNet);

  // Logger log{"data/out/test_rand_all_4_RELU_1/best_net.json"};
  // log << rNet.toJson().dump(2);

  ///
}

TEST_CASE("TrainingNoisy", "[valid][noisy]") {
  using namespace kp2d;

  // Training setup
  // number of groups:
  //   int numGroups = 5;
  // problems per set:
  size_t numProblems = 5;
  // problems in validation:
  size_t numPValid = 10;
  int viter = 8;
  // number of replacement sets:
  size_t numTrainigSets = 4000;
  int riter = 1;
  const EvalType eval = EvalType::UnusedStorageArea;

  // RndEngine::setSeed(10001u);

  // Konstrukcja generatora zadan:
  // - wymiary arkusza W, H
  // - podstawowe wymiary grup elementów
  // - dystrybucja generujaca ilosci w grupach
  // - dystrybucja zaburzenia losowego
  FixedProblemGenerator gen{
      Point{1200, 600},
      {{100, 84}, {120, 250}, {320, 27}, {100, 100}, {20, 144}, {10, 18}},
      NormalInt::create(32, 5),
      NormalInt::create(0, 4),
      true,
      FeatOption::allFeatures};

  using PSet = ProblemSet<nnet::Network, eval>;
  using SSet = SolvableSet<nnet::Network, eval>;

  // Training sets
  std::vector<PSet> replSets;
  for (size_t i = 0; i < numTrainigSets; i++) {
    PSet set = gen.generateSet<nnet::Network, eval>(numProblems);
    // set.sort(RectangleGroup::byWidthHeight);
    replSets.push_back(set);
  }

  // Network construction
  int inputDim = replSets[0].numOfFeatures();
  int h1 = 2 * int(std::sqrt(inputDim));
  //   int hidden2 = int(std::sqrt(inputDim));

  nnet::Network net{{{inputDim, h1, nnet::Activation::Type::SIGM},
                     {h1, 1, nnet::Activation::Type::SIGM}}};

  std::cout << "Network setup\n"
            << inputDim << " input dimensions\n"
            << h1 << " hidden dimensions\n"
            << net.numOfParams() << " number of parameters\n";

  // Initialization set
  SSet initSet{gen.generateSet<nnet::Network, eval>(numProblems), net};
  //   initSet.sort(RectangleGroup::byWidthHeight);

  // Validation set
  SSet validSet{gen.generateSet<nnet::Network, eval>(numPValid), net};
  //   validSet.sort(RectangleGroup::byWidthHeight);
  std::ofstream sol{"data/out/noisy_all_unused_items/noise_0_4/v_task.json",
                    std::ios_base::out | std::ios_base::trunc};
  validSet.saveToJson(sol);
  sol.close();

  /// Optimization setup
  using Objective = nnet::ObjFunction<SSet>;
  using Optimizer = nnet::Optimizer<Objective, cma::pwqBoundStrategy>;
  using Validation =
      nnet::ValFunction<SSet, PSet, Objective, cma::pwqBoundStrategy>;

  Objective objectiveFun{initSet, 8};
  Validation validation{objectiveFun,
                        validSet,
                        viter,
                        replSets,
                        riter,
                        "validation.txt",
                        "data/out/noisy_all_unused_items/noise_0_4",
                        "net.json"};

  Optimizer cmaOpt(std::move(objectiveFun), -1, validation, -1, 500000);

  cmaOpt.run();
  cmaOpt.print();

  nnet::Network rNet = cmaOpt.getResult();
  PSet set1{gen.generateSet<nnet::Network, eval>(numPValid)};
  set1.solveWith(rNet);

  PSet set2{gen.generateSet<nnet::Network, eval>(numPValid)};
  set2.solveWith(rNet);

  Logger log{"data/out/noisy_all_unused_items/noise_0_4/best_net.json"};
  log << rNet.toJson().dump(2);

  ///
}