#include <cmath>
#include <fstream>
#include <iostream>
#include <vector>

#include "../../kp2d/include/kp2d/ProblemGenerator.h"
#include "../../kp2d/include/kp2d/SolvableSet.h"
#include "../../nnet/include/nnet/Network.h"
#include "../../nnet/include/nnet/ObjFunction.h"
#include "../../nnet/include/nnet/Optimizer.h"
#include "../../nnet/include/nnet/ValFunction.h"
#include "../include/Timer.h"
#include "../include/catch.hpp"

void time2or3LayerNet(
    const std::string& filename, const std::vector<kp2d::Problem>& pvec,
    int layer1OutDim, int layer2OutDim = -1,
    nnet::Activation::Type acFun = nnet::Activation::Type::RELU) {
  using Set =
      kp2d::ProblemSet<nnet::Network, kp2d::EvalType::UnusedStorageArea>;

  std::mt19937 mt{1001u};
  Timer timer;

  Set tset{pvec};
  int numFeatures = tset.numOfFeatures();
  nnet::Network net;

  if (layer2OutDim <= 0) {
    // net = nnet::Network{
    //     {{numFeatures, layer1OutDim, acFun}, {layer1OutDim, 1, acFun}},
    //     nnet::DenseLayer::InitType::Uniform,
    //     mt};

    net = nnet::Network{
        {{numFeatures, layer1OutDim, acFun}, {layer1OutDim, 1, acFun}}};
  } else {
    // net = nnet::Network{{{numFeatures, layer1OutDim, acFun},
    //                      {layer1OutDim, layer2OutDim, acFun},
    //                      {layer2OutDim, 1, acFun}},
    //                     nnet::DenseLayer::InitType::Uniform,
    //                     mt};

    net = nnet::Network{{{numFeatures, layer1OutDim, acFun},
                         {layer1OutDim, layer2OutDim, acFun},
                         {layer2OutDim, 1, acFun}}};
  }
  timer.start();
  tset.solveWith(net);
  timer.measure();

  double timeAvgPerProblem = static_cast<double>(timer.getMilisec(0)) /
                             static_cast<double>(pvec.size());

  std::ofstream outFile;
  outFile.open(filename.c_str(), std::ios_base::out | std::ios_base::app);
  if (outFile.good()) {
    outFile << net.numOfLayers() << ',' << nnet::Activation::typeDesc(acFun)
            << ',' << net.numOfParams() << ',' << net.inputDim() << ','
            << pvec.size() << ',' << pvec[0].numOfGroups() << ','
            << timer.getMilisec(0) << ',' << timeAvgPerProblem << ",ms\n";
  } else {
    std::cout << "Sorry: Error in file\n";
  }
  outFile.close();
}

void timeTrainedNet(const std::string& netFilename,
                    const std::string& outFilename,
                    const std::vector<kp2d::Problem>& pvec) {
  using Set =
      kp2d::ProblemSet<nnet::Network, kp2d::EvalType::UnusedStorageArea>;
  Timer timer;

  Set tset{pvec};
  int numFeatures = tset.numOfFeatures();
  nnet::Network net = nnet::Network::loadFromJson(netFilename);

  if (net.inputDim() != numFeatures) {
    std::cout << "Sorry: Error reading net from file\n";
    return;
  }

  timer.start();
  tset.solveWith(net);
  timer.measure();

  double timeAvgPerProblem = static_cast<double>(timer.getMilisec(0)) /
                             static_cast<double>(pvec.size());

  std::ofstream outFile;
  outFile.open(outFilename.c_str(), std::ios_base::out | std::ios_base::app);
  if (!outFile.good()) {
    std::cout << "Sorry: Error opening out file\n";
  }
  outFile << net.numOfLayers() << ',' << "Sigmoid" << ',' << net.numOfParams()
          << ',' << net.inputDim() << ',' << pvec.size() << ','
          << pvec[0].numOfGroups() << ',' << timer.getMilisec(0) << ','
          << timeAvgPerProblem << ",ms\n";
  outFile.close();
}

void timeMaxRects(const std::string& outFilename,
                  const std::vector<kp2d::Problem>& pvec,
                  rbp::MaxRectsBinPack::FreeRectChoiceHeuristic heur) {
  using Set =
      kp2d::ProblemSet<rbp::MaxRectsBinPack, kp2d::EvalType::UnusedStorageArea>;

  Set tset{pvec};
  Timer timer;

  kp2d::MaxRectHeur::set(heur);
  rbp::MaxRectsBinPack maxRect;

  timer.start();
  tset.solveWith(maxRect);
  timer.measure();

  double timeAvgPerProblem = static_cast<double>(timer.getMilisec(0)) /
                             static_cast<double>(pvec.size());

  std::ofstream outFile;
  outFile.open(outFilename.c_str(), std::ios_base::out | std::ios_base::app);
  if (!outFile.good()) {
    std::cout << "Sorry: Error opening out file\n";
  }

  std::string maxrType;
  switch (heur) {
    case rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBestShortSideFit:
      maxrType = "RectBestShortSideFit";
      break;
    case rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBestLongSideFit:
      maxrType = "RectBestLongSideFit";
      break;
    case rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBestAreaFit:
      maxrType = "RectBestAreaFit";
      break;
    case rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBottomLeftRule:
      maxrType = "RectBottomLeftRule";
      break;
    case rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectContactPointRule:
      maxrType = "RectContactPointRule";
      break;
    default:
      maxrType = "Unknown";
  }

  outFile << maxrType << ",,,,," << pvec.size() << ',' << pvec[0].numOfGroups()
          << ',' << timer.getMilisec(0) << ',' << timeAvgPerProblem << ",ms\n";
  outFile.close();
}

TEST_CASE("TimeComparisonParameterDemendent", "[net][time][param-dependent]") {
  using namespace kp2d;
  int numGroups = 6;
  size_t numProblems = 800;

  int W = 1200;
  int H = 800;
  kp2d::ProblemGenerator gen{Point{W, H}, NormalInt::create(8, 2),
                             UniformInt::create(0.2 * W, 0.8 * W),
                             UniformInt::create(0.2 * H, 0.8 * H),
                             FeatOption::allFeatures};

  std::vector<kp2d::Problem> problems =
      gen.generateProblems(numProblems, numGroups);

  using Set =
      kp2d::ProblemSet<nnet::Network, kp2d::EvalType::UnusedStorageArea>;

  Set set{problems};

  std::ofstream taskFile{"data/out/timing/param_dep_02_tasks.json",
                         std::ios_base::out | std::ios_base::trunc};
  taskFile << set.toJson().dump(2) << '\n';
  taskFile.close();

  std::string logFile = "data/out/timing/param_dep_02.csv";
  std::ofstream log;

  log.open(logFile.c_str(), std::ios_base::out | std::ios_base::app);
  log << "Layers,Activation,Parameters,Features,Problems,Groups,Time"
         ",Avg. Solve Time"
         ",Units\n";
  log.close();

  /// SIECI DWUWARSTWOWE
  time2or3LayerNet(logFile, problems, 50, -1, nnet::Activation::Type::RELU);
  time2or3LayerNet(logFile, problems, 46, -1, nnet::Activation::Type::RELU);
  time2or3LayerNet(logFile, problems, 40, -1, nnet::Activation::Type::RELU);
  time2or3LayerNet(logFile, problems, 38, -1, nnet::Activation::Type::RELU);
  time2or3LayerNet(logFile, problems, 28, -1, nnet::Activation::Type::RELU);
  time2or3LayerNet(logFile, problems, 24, -1, nnet::Activation::Type::RELU);
  time2or3LayerNet(logFile, problems, 22, -1, nnet::Activation::Type::RELU);
  time2or3LayerNet(logFile, problems, 14, -1, nnet::Activation::Type::RELU);
  time2or3LayerNet(logFile, problems, 12, -1, nnet::Activation::Type::RELU);
  time2or3LayerNet(logFile, problems, 10, -1, nnet::Activation::Type::RELU);
  time2or3LayerNet(logFile, problems, 8, -1, nnet::Activation::Type::RELU);
  time2or3LayerNet(logFile, problems, 6, -1, nnet::Activation::Type::RELU);

  time2or3LayerNet(logFile, problems, 50, -1, nnet::Activation::Type::SIGM);
  time2or3LayerNet(logFile, problems, 46, -1, nnet::Activation::Type::SIGM);
  time2or3LayerNet(logFile, problems, 40, -1, nnet::Activation::Type::SIGM);
  time2or3LayerNet(logFile, problems, 38, -1, nnet::Activation::Type::SIGM);
  time2or3LayerNet(logFile, problems, 28, -1, nnet::Activation::Type::SIGM);
  time2or3LayerNet(logFile, problems, 24, -1, nnet::Activation::Type::SIGM);
  time2or3LayerNet(logFile, problems, 22, -1, nnet::Activation::Type::SIGM);
  time2or3LayerNet(logFile, problems, 14, -1, nnet::Activation::Type::SIGM);
  time2or3LayerNet(logFile, problems, 12, -1, nnet::Activation::Type::SIGM);
  time2or3LayerNet(logFile, problems, 10, -1, nnet::Activation::Type::SIGM);
  time2or3LayerNet(logFile, problems, 8, -1, nnet::Activation::Type::SIGM);
  time2or3LayerNet(logFile, problems, 6, -1, nnet::Activation::Type::SIGM);

  /// SIECI TRÓJWARSTWOWE
  time2or3LayerNet(logFile, problems, 40, 12, nnet::Activation::Type::RELU);
  time2or3LayerNet(logFile, problems, 40, 10, nnet::Activation::Type::RELU);
  time2or3LayerNet(logFile, problems, 40, 7, nnet::Activation::Type::RELU);
  time2or3LayerNet(logFile, problems, 22, 9, nnet::Activation::Type::RELU);
  time2or3LayerNet(logFile, problems, 22, 7, nnet::Activation::Type::RELU);
  time2or3LayerNet(logFile, problems, 22, 5, nnet::Activation::Type::RELU);
  time2or3LayerNet(logFile, problems, 16, 9, nnet::Activation::Type::RELU);
  time2or3LayerNet(logFile, problems, 16, 7, nnet::Activation::Type::RELU);
  time2or3LayerNet(logFile, problems, 16, 5, nnet::Activation::Type::RELU);
  time2or3LayerNet(logFile, problems, 11, 7, nnet::Activation::Type::RELU);
  time2or3LayerNet(logFile, problems, 11, 5, nnet::Activation::Type::RELU);

  time2or3LayerNet(logFile, problems, 40, 12, nnet::Activation::Type::SIGM);
  time2or3LayerNet(logFile, problems, 40, 10, nnet::Activation::Type::SIGM);
  time2or3LayerNet(logFile, problems, 40, 7, nnet::Activation::Type::SIGM);
  time2or3LayerNet(logFile, problems, 22, 9, nnet::Activation::Type::SIGM);
  time2or3LayerNet(logFile, problems, 22, 7, nnet::Activation::Type::SIGM);
  time2or3LayerNet(logFile, problems, 22, 5, nnet::Activation::Type::SIGM);
  time2or3LayerNet(logFile, problems, 16, 9, nnet::Activation::Type::SIGM);
  time2or3LayerNet(logFile, problems, 16, 7, nnet::Activation::Type::SIGM);
  time2or3LayerNet(logFile, problems, 16, 5, nnet::Activation::Type::SIGM);
  time2or3LayerNet(logFile, problems, 11, 7, nnet::Activation::Type::SIGM);
  time2or3LayerNet(logFile, problems, 11, 5, nnet::Activation::Type::SIGM);
}

TEST_CASE("TimeComparisonGroupNumber", "[net][time][group-dependent]") {
  using namespace kp2d;
  size_t numProblems = 2000;

  int W = 1200;
  int H = 800;
  kp2d::ProblemGenerator gen{Point{W, H}, NormalInt::create(8, 2),
                             UniformInt::create(0.2 * W, 0.8 * W),
                             UniformInt::create(0.2 * H, 0.8 * H),
                             FeatOption::allFeatures};

  std::string logFile = "data/out/timing/group_dep_06.csv";
  std::ofstream log;
  log.open(logFile.c_str(), std::ios_base::out | std::ios_base::app);
  log << "Layers,Activation,Parameters,Features,Problems,Groups,Time"
         ",Avg. Solve Time"
         ",Units\n";
  log.close();

  std::vector<kp2d::Problem> problems = gen.generateProblems(numProblems, 3);
  timeTrainedNet("data/out/timing/group_dep_training/3_2/b_net.json", logFile,
                 problems);
  timeMaxRects(logFile, problems,
               rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBestAreaFit);
  timeMaxRects(
      logFile, problems,
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBestLongSideFit);
  timeMaxRects(
      logFile, problems,
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBestShortSideFit);
  timeMaxRects(
      logFile, problems,
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBottomLeftRule);
  timeMaxRects(
      logFile, problems,
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectContactPointRule);

  problems.clear();
  problems = gen.generateProblems(numProblems, 5);
  timeTrainedNet("data/out/timing/group_dep_training/5_2/b_net.json", logFile,
                 problems);
  timeMaxRects(logFile, problems,
               rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBestAreaFit);
  timeMaxRects(
      logFile, problems,
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBestLongSideFit);
  timeMaxRects(
      logFile, problems,
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBestShortSideFit);
  timeMaxRects(
      logFile, problems,
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBottomLeftRule);
  timeMaxRects(
      logFile, problems,
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectContactPointRule);

  problems.clear();
  problems = gen.generateProblems(numProblems, 6);
  timeTrainedNet("data/out/timing/group_dep_training/6_2/b_net.json", logFile,
                 problems);
  timeMaxRects(logFile, problems,
               rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBestAreaFit);
  timeMaxRects(
      logFile, problems,
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBestLongSideFit);
  timeMaxRects(
      logFile, problems,
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBestShortSideFit);
  timeMaxRects(
      logFile, problems,
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBottomLeftRule);
  timeMaxRects(
      logFile, problems,
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectContactPointRule);

  problems.clear();
  problems = gen.generateProblems(numProblems, 8);
  timeTrainedNet("data/out/timing/group_dep_training/8_2/b_net.json", logFile,
                 problems);
  timeMaxRects(logFile, problems,
               rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBestAreaFit);
  timeMaxRects(
      logFile, problems,
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBestLongSideFit);
  timeMaxRects(
      logFile, problems,
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBestShortSideFit);
  timeMaxRects(
      logFile, problems,
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBottomLeftRule);
  timeMaxRects(
      logFile, problems,
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectContactPointRule);

  problems.clear();
  problems = gen.generateProblems(numProblems, 10);
  timeTrainedNet("data/out/timing/group_dep_training/10_2/b_net.json", logFile,
                 problems);
  timeMaxRects(logFile, problems,
               rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBestAreaFit);
  timeMaxRects(
      logFile, problems,
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBestLongSideFit);
  timeMaxRects(
      logFile, problems,
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBestShortSideFit);
  timeMaxRects(
      logFile, problems,
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBottomLeftRule);
  timeMaxRects(
      logFile, problems,
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectContactPointRule);
}