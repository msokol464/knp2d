#include <iostream>

#include "../../kp2d/include/kp2d/ProblemGenerator.h"
#include "../../kp2d/include/kp2d/SolvableSet.h"
#include "../../nnet/include/nnet/Network.h"
#include "../../nnet/include/nnet/ObjFunction.h"
#include "../include/catch.hpp"

SCENARIO("ObjFunctionCall", "[obj]") {
  GIVEN("Network and ProblemGenerator") {
    // generator
    using kp2d::ProblemGenerator;
    ProblemGenerator kpSource{
        kp2d::Point{200, 500}, kp2d::UniformInt::create(2, 12),
        kp2d::NormalInt::create(20, 3.0), kp2d::NormalInt::create(42, 2.1)};

    // sieć
    using nnet::Activation;
    using nnet::DenseLayer;
    using nnet::Network;
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::ranlux48 rnd{seed};
    Network net{{{kpSource.numOfFeatures(), 72, Activation::Type::RELU},
                 {72, 1, Activation::Type::RELU}}};
    WHEN("Network is created") {
      THEN("it is in a valid state") { REQUIRE(net.isValid()); }
    }

    AND_GIVEN("Objective function for generated SolvableSet") {
      // zbiór
      using SolvableSet = kp2d::SolvableSet<Network>;
      SolvableSet set{kpSource.generateSetFixed<Network>(5), net};
      REQUIRE(set.isUnsolved());

      // opt
      using nnet::ObjFunction;
      using ProblemSet = kp2d::ProblemSet<Network>;
      ObjFunction objective{set, 6};
      objective(net.getParamVec().data(), net.numOfParams());

      ProblemSet newProblems = kpSource.generateSetFixed<Network>(5);
      SolvableSet newSet{newProblems, net};

      REQUIRE_FALSE(objective.getSample().isEquiv(newSet));

      WHEN("Replacing sample set") {
        objective.template replaceSampleSet<ProblemSet>(newProblems);
        THEN("Objective function has new sample") {
          REQUIRE(objective.getSample().isEquiv(newSet));
        }
        THEN("Objective function has same net") {
          REQUIRE(objective.getSampleNet().isEquiv(net));
        }
      }
    }
  }
}