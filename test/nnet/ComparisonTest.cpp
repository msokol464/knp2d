#include <cmath>
#include <iostream>
#include <vector>

#include "../../kp2d/include/kp2d/CutProblemGenerator.h"
#include "../../kp2d/include/kp2d/FixedProblemGenerator.h"
#include "../../kp2d/include/kp2d/ProblemGenerator.h"
#include "../../kp2d/include/kp2d/SolvableSet.h"
#include "../../nnet/include/nnet/Network.h"
#include "../../nnet/include/nnet/ObjFunction.h"
#include "../../nnet/include/nnet/Optimizer.h"
#include "../../nnet/include/nnet/ValFunction.h"
#include "../include/catch.hpp"

template <kp2d::EvalType eval>
void testNet(std::vector<kp2d::Problem>& pvec, const std::string& netPathname,
             const std::string& outPathname) {
  ///
  using SSet = kp2d::SolvableSet<nnet::Network, eval>;
  ///
  nnet::Network net = nnet::Network::loadFromJson(netPathname);
  for (const kp2d::Problem& p : pvec) {
    if (p.numOfFeatures() != net.inputDim()) {
      std::cout << "Net tesing failed due to mismatched number of features\n";
      return;
    }
  }
  SSet pset{pvec, net};

  //   std::ofstream task{outPathname + ".task",
  //                      std::ios_base::out | std::ios_base::trunc};
  //   pset.saveToJson(task);
  //   task.close();

  pset.solve();

  std::ofstream sol{outPathname, std::ios_base::out | std::ios_base::trunc};
  pset.saveToJson(sol);
  sol.close();
}

template <kp2d::EvalType eval>
void testHeur(std::vector<kp2d::Problem>& pvec, const std::string& outPathname,
              const rbp::MaxRectsBinPack::FreeRectChoiceHeuristic heur) {
  ///
  using HSet = kp2d::SolvableSet<rbp::MaxRectsBinPack, eval>;
  ///
  kp2d::MaxRectHeur::set(heur);
  rbp::MaxRectsBinPack maxRect;
  HSet pset{pvec, maxRect};

  //   std::ofstream task{outPathname + ".task",
  //                      std::ios_base::out | std::ios_base::trunc};
  //   pset.saveToJson(task);
  //   task.close();

  pset.solve();

  std::ofstream sol{outPathname, std::ios_base::out | std::ios_base::trunc};
  pset.saveToJson(sol);
  sol.close();
}

TEST_CASE("Comparison01", "[maxRect][all][rand]") {
  using namespace kp2d;
  // number of groups:
  int numGroups = 5;
  // problems per set:
  size_t numProblems = 300;
  const EvalType eval = EvalType::UnusedStorageArea;

  ProblemGenerator gen{Point{1200, 640}, NormalInt::create(20, 5),
                       UniformInt::create(20, 300), FeatOption::allFeatures};
  gen.setNumOfGroups(numGroups);

  std::vector<Problem> problems = gen.generateProblems(numProblems, numGroups);
  ///
  testNet<eval>(problems, "data/out/test_rand_all_0/b_net.json",
                "data/out/cmp_rand_all_01_unused_items/b_net_0.json");
  testNet<eval>(problems, "data/out/test_rand_all_0/m_net.json",
                "data/out/cmp_rand_all_01_unused_items/m_net_0.json");
  testNet<eval>(problems, "data/out/test_rand_all_1/b_net.json",
                "data/out/cmp_rand_all_01_unused_items/b_net_1.json");
  testNet<eval>(problems, "data/out/test_rand_all_1/m_net.json",
                "data/out/cmp_rand_all_01_unused_items/m_net_1.json");

  testNet<eval>(problems, "data/out/test_rand_all_2/m_net.json",
                "data/out/cmp_rand_all_01_unused_items/m_net_2.json");
  testNet<eval>(problems, "data/out/test_rand_all_2/b_net.json",
                "data/out/cmp_rand_all_01_unused_items/b_net_2.json");

  ///
  testHeur<eval>(
      problems, "data/out/cmp_rand_all_01_unused_items/maxr_baf.json",
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBestAreaFit);
  testHeur<eval>(
      problems, "data/out/cmp_rand_all_01_unused_items/maxr_blsf.json",
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBestLongSideFit);
  testHeur<eval>(
      problems, "data/out/cmp_rand_all_01_unused_items/maxr_bssf.json",
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBestShortSideFit);
  testHeur<eval>(
      problems, "data/out/cmp_rand_all_01_unused_items/maxr_blr.json",
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBottomLeftRule);
  testHeur<eval>(
      problems, "data/out/cmp_rand_all_01_unused_items/maxr_cpr.json",
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectContactPointRule);
  ///
}

TEST_CASE("Comparison02", "[maxRect][all][noisy]") {
  using namespace kp2d;

  size_t numProblems = 1000;
  const EvalType eval = EvalType::UnusedStorageArea;

  RndEngine::setSeed(909u);
  FixedProblemGenerator gen{
      Point{1200, 600},
      {{100, 84}, {120, 250}, {320, 27}, {100, 100}, {20, 144}, {10, 18}},
      NormalInt::create(32, 5),
      std::unique_ptr<IntDistrib>(nullptr),
      true,
      FeatOption::allFeatures};

  std::vector<Problem> problems = gen.generateProblems(numProblems);

  testNet<eval>(
      problems, "data/out/noisy_all_unused_items_01/no_noise/b_net.json",
      "data/out/noisy_all_unused_items_01/no_noise/test_b_net_1000.json");

  testHeur<eval>(
      problems, "data/out/noisy_all_unused_items_01/no_noise/maxr_bssf.json",
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBestShortSideFit);

  testHeur<eval>(
      problems, "data/out/noisy_all_unused_items_01/no_noise/maxr_cpr.json",
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectContactPointRule);

  testHeur<eval>(
      problems, "data/out/noisy_all_unused_items_01/no_noise/maxr_blr.json",
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBottomLeftRule);

  testHeur<eval>(
      problems, "data/out/noisy_all_unused_items_01/no_noise/maxr_blsf.json",
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBestLongSideFit);

  testHeur<eval>(
      problems, "data/out/noisy_all_unused_items_01/no_noise/maxr_baf.json",
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBestAreaFit);

  ///
  gen.addNoise(NormalInt::create(0, 2));
  RndEngine::setSeed(909u);
  std::vector<Problem> problems02 = gen.generateProblems(numProblems);

  testNet<eval>(
      problems02, "data/out/noisy_all_unused_items_01/noise_0_2/b_net.json",
      "data/out/noisy_all_unused_items_01/noise_0_2/test_b_net_1000.json");

  testHeur<eval>(
      problems02, "data/out/noisy_all_unused_items_01/noise_0_2/maxr_bssf.json",
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBestShortSideFit);

  testHeur<eval>(
      problems02, "data/out/noisy_all_unused_items_01/noise_0_2/maxr_cpr.json",
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectContactPointRule);

  testHeur<eval>(
      problems02, "data/out/noisy_all_unused_items_01/noise_0_2/maxr_blr.json",
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBottomLeftRule);

  testHeur<eval>(
      problems02, "data/out/noisy_all_unused_items_01/noise_0_2/maxr_blsf.json",
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBestLongSideFit);

  testHeur<eval>(
      problems02, "data/out/noisy_all_unused_items_01/noise_0_2/maxr_baf.json",
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBestAreaFit);

  ///
  gen.addNoise(NormalInt::create(0, 4));
  RndEngine::setSeed(909u);
  std::vector<Problem> problems04 = gen.generateProblems(numProblems);

  testNet<eval>(
      problems04, "data/out/noisy_all_unused_items_01/noise_0_4/b_net.json",
      "data/out/noisy_all_unused_items_01/noise_0_4/test_b_net_1000.json");

  testHeur<eval>(
      problems04, "data/out/noisy_all_unused_items_01/noise_0_4/maxr_bssf.json",
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBestShortSideFit);

  testHeur<eval>(
      problems04, "data/out/noisy_all_unused_items_01/noise_0_4/maxr_cpr.json",
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectContactPointRule);

  testHeur<eval>(
      problems04, "data/out/noisy_all_unused_items_01/noise_0_4/maxr_blr.json",
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBottomLeftRule);

  testHeur<eval>(
      problems04, "data/out/noisy_all_unused_items_01/noise_0_4/maxr_blsf.json",
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBestLongSideFit);

  testHeur<eval>(
      problems04, "data/out/noisy_all_unused_items_01/noise_0_4/maxr_baf.json",
      rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBestAreaFit);
}

TEST_CASE("Comparison03", "[maxRect][all][activ]") {
  using namespace kp2d;

  // problems per set:
  size_t numProblems = 1000;
  int numGroups = 6;
  const EvalType eval = EvalType::UnusedStorageArea;

  int W = 1200;
  int H = 800;
  ProblemGenerator gen{Point{W, H}, NormalInt::create(10, 3),
                       UniformInt::create(0.2 * W, 0.8 * W),
                       UniformInt::create(0.2 * H, 0.8 * H),
                       FeatOption::allFeatures};
  gen.setNumOfGroups(numGroups);

  std::vector<Problem> problems = gen.generateProblems(numProblems, numGroups);

  testNet<eval>(
      problems, "data/out/num_problems_in_set/validation_128/1/m_net.json",
      "data/out/num_problems_in_set/validation_128/1/test_m_net_1000.json");

  testNet<eval>(
      problems, "data/out/num_problems_in_set/validation_128/1_1/m_net.json",
      "data/out/num_problems_in_set/validation_128/1_1/test_m_net_1000.json");

  testNet<eval>(
      problems, "data/out/num_problems_in_set/validation_128/1_2/m_net.json",
      "data/out/num_problems_in_set/validation_128/1_2/test_m_net_1000.json");

  testNet<eval>(
      problems, "data/out/num_problems_in_set/validation_128/1/b_net.json",
      "data/out/num_problems_in_set/validation_128/1/test_b_net_1000.json");

  testNet<eval>(
      problems, "data/out/num_problems_in_set/validation_128/1_1/b_net.json",
      "data/out/num_problems_in_set/validation_128/1_1/test_b_net_1000.json");

  testNet<eval>(
      problems, "data/out/num_problems_in_set/validation_128/1_2/b_net.json",
      "data/out/num_problems_in_set/validation_128/1_2/test_b_net_1000.json");
  /*
    testNet<eval>(
        problems, "data/out/num_problems_in_set/validation_128/3/m_net.json",
        "data/out/num_problems_in_set/validation_128/3/test_m_net_1000.json");

    testNet<eval>(
        problems, "data/out/num_problems_in_set/validation_128/5/m_net.json",
        "data/out/num_problems_in_set/validation_128/5/test_m_net_1000.json");

    testNet<eval>(
        problems, "data/out/num_problems_in_set/validation_128/7/m_net.json",
        "data/out/num_problems_in_set/validation_128/7/test_m_net_1000.json");

    testNet<eval>(
        problems, "data/out/num_problems_in_set/validation_128/9/m_net.json",
        "data/out/num_problems_in_set/validation_128/9/test_m_net_1000.json");



    testNet<eval>(
        problems, "data/out/num_problems_in_set/validation_128/3/b_net.json",
        "data/out/num_problems_in_set/validation_128/3/test_b_net_1000.json");

    testNet<eval>(
        problems, "data/out/num_problems_in_set/validation_128/5/b_net.json",
        "data/out/num_problems_in_set/validation_128/5/test_b_net_1000.json");

    testNet<eval>(
        problems, "data/out/num_problems_in_set/validation_128/7/b_net.json",
        "data/out/num_problems_in_set/validation_128/7/test_b_net_1000.json");

    testNet<eval>(
        problems, "data/out/num_problems_in_set/validation_128/9/b_net.json",
        "data/out/num_problems_in_set/validation_128/9/test_b_net_1000.json");

    testHeur<eval>(
        problems,
        "data/out/num_problems_in_set/validation_128/heur/maxr_blr.json",
        rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBottomLeftRule);
    testHeur<eval>(
        problems,
        "data/out/num_problems_in_set/validation_128/heur/maxr_cpr.json",
        rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectContactPointRule);
    testHeur<eval>(
        problems,
        "data/out/num_problems_in_set/validation_128/heur/maxr_baf.json",
        rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBestAreaFit);
    testHeur<eval>(
        problems,
        "data/out/num_problems_in_set/validation_128/heur/maxr_blsf.json",
        rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBestLongSideFit);
    testHeur<eval>(
        problems,
        "data/out/num_problems_in_set/validation_128/heur/maxr_bssf.json",
        rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBestShortSideFit);
  */
}
