#include <iostream>

#include "../../kp2d/include/kp2d/ProblemGenerator.h"
#include "../../kp2d/include/kp2d/SolvableSet.h"
#include "../../nnet/include/nnet/Network.h"
#include "../../nnet/include/nnet/ObjFunction.h"
#include "../../nnet/include/nnet/Optimizer.h"
#include "../../nnet/include/nnet/ProgFunction.h"
#include "../include/NetSetFixture.h"
#include "../include/catch.hpp"

TEST_CASE("OptimizerStepNoBounds", "[bbo][micro]") {
  using namespace nnet;
  using kp2d::NormalInt;
  using kp2d::Point;
  using kp2d::ProblemSet;
  using kp2d::UniformInt;

  std::ranlux48 rnd{123};

  kp2d::ProblemGenerator kpGen{Point{220, 60},
                               UniformInt::create(2, 8),
                               NormalInt::create(40, 5),
                               NormalInt::create(10, 3),
                               kp2d::FeatOption::allFeatures,
                               2};

  SizeType featNum = kpGen.numOfFeatures();
  std::cout << featNum << '\n';

  Network net{
      {{featNum, 25, Activation::Type::RELU}, {25, 1, Activation::Type::RELU}}};
  REQUIRE(net.isValid());

  kp2d::SolvableSet set{kpGen.generateSetFixed<Network>(10), net};
  REQUIRE(set.isUnsolved());

  using namespace nnet;
  ObjFunction objective{set, 6};
  Optimizer<ObjFunction<kp2d::SolvableSet<Network>>> cmaesOpt{
      std::move(objective), 0.1};
  cmaesOpt.run();
}

TEST_CASE("OptimizerStepBoundStrategy", "[bbo][bounds][micro]") {
  using namespace nnet;
  using kp2d::NormalInt;
  using kp2d::Point;
  using kp2d::ProblemSet;
  using kp2d::UniformInt;

  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::ranlux48 rnd{seed};

  kp2d::ProblemGenerator kpGen{Point{220, 60},
                               UniformInt::create(2, 8),
                               NormalInt::create(40, 5),
                               NormalInt::create(10, 3),
                               kp2d::FeatOption::allFeatures,
                               2};

  SizeType featNum = kpGen.numOfFeatures();
  std::cout << featNum << '\n';

  Network net{
      {{featNum, 25, Activation::Type::RELU}, {25, 1, Activation::Type::RELU}}};
  REQUIRE(net.isValid());

  kp2d::SolvableSet set{kpGen.generateSetFixed<Network>(10), net};
  REQUIRE(set.isUnsolved());

  using namespace nnet;
  ObjFunction objective{set, 4};
  Optimizer<ObjFunction<kp2d::SolvableSet<Network>>, libcmaes::pwqBoundStrategy>
      cmaesOpt{std::move(objective), 0.1};
  cmaesOpt.run();
}

TEST_CASE_METHOD(NetSetFixture, "Cast ProgFunction to std::function",
                 "[fixture][bbo][cast]") {
  // opt
  using nnet::ObjFunction;
  ObjFunction objective{set, 6};

  using kp2d::ProblemSet;
  using nnet::ProgFunction;
  std::vector<ProblemSet<Network>> vec;
  size_t count = 5;
  for (size_t i = 0; i < count; i++) {
    vec.push_back(kpSource.generateSetFixed<Network>(3));
  }
  int iter = 2;
  ProgFunction progress{vec, objective, iter};

  namespace cma = libcmaes;
  std::function<int(
      const cma::CMAParameters<cma::GenoPheno<cma::NoBoundStrategy>>&,
      const cma::CMASolutions&)>
      pfun = progress;

  using nnet::Optimizer;
  Optimizer<ObjFunction<SolvableSet<Network>>, cma::NoBoundStrategy> opt(
      std::move(objective), 0.1, pfun);

  opt.run();

  Network resNet = opt.getResult();
  REQUIRE(resNet.isEquiv(net));
}

TEST_CASE("ManualProblemSet", "[bbo][manual][micro]") {
  using namespace nnet;
  using kp2d::Point;
  using kp2d::Problem;
  using kp2d::ProblemSet;
  using kp2d::SolvableSet;
  using kp2d::Storage;
  // 16, 8 (x8)
  Point C{128, 64};

  // max przynajmniej 100% zapełnienia, pozostałe np.: {24, 8, 2}, {8, 8, 1}
  Problem p1{C, Storage::create(
                    {{40, 32, 4}, {24, 64, 1}, {24, 8, 10}, {8, 8, 1}}, C)};

  // max przynajmniej 97,65625% zapełnienia, pozostałe np.: {24, 40, 2}
  Problem p2{
      C, Storage::create({{24, 40, 9}, {32, 40, 1}, {0, 0, 0}, {0, 0, 0}}, C)};

  // max przynajmniej 93,75% zapełnienia, pozostałe np.: {128, 128, 2}
  Problem p3{C, Storage::create(
                    {{128, 64, 2}, {16, 16, 10}, {32, 32, 5}, {0, 0, 0}}, C)};

  // max przynajmniej 100 % zapełnienia
  // pozostałe np.: {128, 8, 1}, {16, 16, 2}, {24, 8, 2}
  Problem p4{C, Storage::create(
                    {{128, 8, 1}, {16, 16, 10}, {32, 32, 6}, {24, 8, 2}}, C)};

  std::vector<Problem> vec;

  vec.push_back(p1);
  vec.push_back(p2);
  vec.push_back(p3);
  vec.push_back(p4);

  ProblemSet<nnet::Network, kp2d::EvalType::UnusedContainerPortion> pset{
      std::move(vec)};

  REQUIRE(pset.isValid());

  kp2d::IntType numFeat = pset.numOfFeatures();
  kp2d::IntType l1out = numFeat / 2;
  kp2d::IntType l2out = numFeat / 4;
  std::mt19937 mt{1001u};

  Network net{{{numFeat, l1out, Activation::Type::RELU},
               {l1out, l2out, Activation::Type::RELU},
               {l2out, 1, Activation::Type::ID}}};

  REQUIRE(net.isValid());

  using Objective = nnet::ObjFunction<
      SolvableSet<Network, kp2d::EvalType::UnusedContainerPortion>>;
  // using Progress = nnet::ProgFunction<ProblemSet, Objective>;
  using Optimizer = nnet::Optimizer<Objective, cma::NoBoundStrategy>;

  SolvableSet<Network, kp2d::EvalType::UnusedContainerPortion> set{pset, net};
  Objective objectiveFun(set, 4);
  // Progress progress{{ps}, objective, 10};
  // namespace cma = libcmaes;
  // std::function<int(
  //     const cma::CMAParameters<cma::GenoPheno<cma::NoBoundStrategy>>&,
  //     const cma::CMASolutions&)>
  //     pfun = progress;

  Optimizer cmaOpt(
      std::move(objectiveFun), 0.1,
      cma::CMAStrategy<cma::CovarianceUpdate,
                       cma::GenoPheno<cma::NoBoundStrategy>>::_defaultPFunc,
      -1, 5000);

  cmaOpt.run();
  Network result = cmaOpt.getResult();

  std::ofstream fout;
  fout.open("data/out/optimTestNet5000.json", std::ios::out);
  result.saveToJson(fout);
  fout.close();
  /*
   */
}
