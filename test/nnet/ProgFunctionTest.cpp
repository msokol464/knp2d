#include <iostream>

#include "../../kp2d/include/kp2d/ProblemGenerator.h"
#include "../../kp2d/include/kp2d/SolvableSet.h"
#include "../../nnet/include/nnet/Network.h"
#include "../../nnet/include/nnet/ObjFunction.h"
#include "../../nnet/include/nnet/Optimizer.h"
#include "../../nnet/include/nnet/ProgFunction.h"
#include "../include/NetSetFixture.h"
#include "../include/catch.hpp"

TEST_CASE_METHOD(NetSetFixture, "Create toy example ProgFunction with int",
                 "[fixture][prog][toy]") {
  // opt
  using nnet::ObjFunction;
  ObjFunction objective{set, 6};

  using nnet::ProgFunction;
  std::vector<int> vec = {1, 34, 52, 725, 1502};
  int iter = 10;
  ProgFunction progress{vec, objective, iter};

  REQUIRE(progress.numOfSets() == vec.size());
  REQUIRE(progress.iterPeriod() == iter);
  REQUIRE(progress.nextSet() == vec[0]);
}

TEST_CASE_METHOD(NetSetFixture, "Create ProgFunction with ProblemSet",
                 "[fixture][prog][ctor]") {
  // opt
  using nnet::ObjFunction;
  ObjFunction objective{set, 6};

  using kp2d::ProblemSet;
  using nnet::ProgFunction;
  std::vector<ProblemSet<Network>> vec;
  size_t count = 5;
  for (size_t i = 0; i < count; i++) {
    vec.push_back(kpSource.generateSetFixed<Network>(3));
  }
  int iter = 3;
  ProgFunction progress{vec, objective, iter};

  REQUIRE(progress.numOfSets() == count);
  REQUIRE(progress.iterPeriod() == iter);
  REQUIRE(progress.nextSet().isEquiv(vec[0]));

  REQUIRE_NOTHROW(progress.changeObjectiveSample());
  REQUIRE(objective.getSample().problemSet().isEquiv(vec[0]));

  progress.next();  // 1

  REQUIRE_NOTHROW(progress.changeObjectiveSample());
  REQUIRE(objective.getSample().problemSet().isEquiv(vec[1]));

  progress.next();  // 2
  progress.next();  // 3
  progress.next();  // 4
  progress.next();  // 0
  progress.next();  // 1

  REQUIRE(progress.nextSet().isEquiv(vec[1]));

  ProgFunction prog1{progress};

  REQUIRE(prog1.numOfSets() == count);
  REQUIRE(prog1.iterPeriod() == iter);
  REQUIRE(prog1.nextSet().isEquiv(vec[1]));
}
