#include <iostream>
#include <typeinfo>

#include "../../nnet/include/nnet/DenseLayer.h"
#include "../include/catch.hpp"

TEST_CASE("DenseLayerConstruction", "[layer][ctor]") {
  using namespace nnet;

  std::random_device rd;
  std::mt19937 mt{rd()};

  DenseLayer layerV1Init(5, 7, mt, Activation::Type::ID,
                         DenseLayer::InitType::UniformV1);

  // std::cout << layerV1Init.getParams() << '\n';
  REQUIRE((layerV1Init.getParams().array() <= 1 / sqrt(5)).minCoeff() == 1);
  REQUIRE((layerV1Init.getParams().array() >= -1 / sqrt(5)).minCoeff() == 1);
  std::cout << "Std initialized layer mean: " << layerV1Init.getParams().mean()
            << '\n';

  DenseLayer layerV2Init(5, 7, mt, Activation::Type::ID,
                         DenseLayer::InitType::UniformV2);

  // std::cout << '\n' << layerV2Init.getParams() << '\n';
  REQUIRE((layerV2Init.getParams().array() <= 6 / sqrt(12)).minCoeff() == 1);
  REQUIRE((layerV2Init.getParams().array() >= -6 / sqrt(12)).minCoeff() == 1);
  std::cout << "Xavier initialized layer mean: "
            << layerV2Init.getParams().mean() << '\n';
}

// TEST_CASE("DenseLayerNormalize", "[layer][normalize]") {
//   using namespace nnet;

//   DenseLayer layer(std::move((DynMatrix(7, 6) << 1, 2, 3, 4, 5, 6, 2, 2, 3,
//   4,
//                               5, 6, 3, 2, 3, 4, 5, 6, 4, 2, 3, 4, 5, 6, 5, 2,
//                               3, 4, 5, 6, 6, 2, 3, 4, 5, 6, 7, 2, 3, 4, 5, 6)
//                                  .finished()),
//                    Activation::Type::ID);
//   layer.normalize();
//   REQUIRE(ceil(layer.getParams().norm() * 10000) / 10000 == 1.0);
// }

TEST_CASE("DenseLayerForward", "[layer][forward]") {
  using namespace nnet;

  Eigen::Matrix<FloatType, 6, 1> input;
  input << 10., 9., 8., 7., 6., 5.;
  Eigen::Matrix<FloatType, 4, 1> target;
  target << 10., 9., 8., 7.;

  DenseLayer layer(std::move(DynMatrix::Identity(4, 7)), Activation::Type::ID);

  REQUIRE(layer.forward(input) == target);
}

TEST_CASE("JsonOutput", "[layer][json][save]") {
  using namespace nnet;

  DenseLayer layer(
      std::move(
          (DynMatrix(3, 3) << 1., 2., 3., 4., 5., 6., 7., 8., 9.).finished()),
      Activation::Type::SMAX);

  std::ofstream outFile;
  outFile.open("data/out/layerTest.json", std::ios::out);
  outFile << layer.toJson().dump(2);
  outFile.close();

  /// TODO: require nothrow and fileexists
}

TEST_CASE("JsonInput", "[layer][json][load]") {
  using namespace nnet;
  using namespace nlohmann::literals;
  using json = nlohmann::json;

  std::stringstream stream;
  stream << R"({
    "ObjectType": "DenseLayer",
    "Metadata": {
      "InputDim": 3,
      "OutputDim": 3,
      "Activation": "Tanh"
    },
    "Data": [
      [
        1.0,
        2.0,
        3.0,
        -0.09
      ],
      [
        4.0,
        5.0,
        6.0,
        -0.1
      ],
      [
        7.0,
        8.0,
        9.0,
        -0.08
      ]
    ]
  })"_json;
  json target;
  stream >> target;

  json layerJson;
  std::ifstream inFile;
  inFile.open("data/in/layerTest.json", std::ios::in);
  inFile >> layerJson;
  std::unique_ptr<DenseLayer> layer = DenseLayer::fromJson(layerJson);
  inFile.close();

  json returned = layer->toJson();
  REQUIRE(returned == target);

  /// TODO: require nothrow and fileexists
}

TEST_CASE("CreateLayerPtr", "[layer][create]") {
  using namespace nnet;

  std::random_device rd;
  std::mt19937 mt{rd()};

  std::unique_ptr<DenseLayer> layerPtr = DenseLayer::create(
      5, 5, mt, Activation::Type::TANH, DenseLayer::InitType::UniformV2);

  REQUIRE(layerPtr != nullptr);
}

TEST_CASE("LayerParamsResize", "[layer][resize]") {
  using namespace nnet;

  std::random_device rd;
  std::mt19937 mt{rd()};

  std::unique_ptr<DenseLayer> layerPtr = DenseLayer::create(
      5, 5, mt, Activation::Type::TANH, DenseLayer::InitType::UniformV2);

  layerPtr->resizeParamsToVec();
  REQUIRE(layerPtr->getParams().rows() == 5 * 6);
  REQUIRE(layerPtr->getParams().cols() == 1);
  layerPtr->resizeParamsToMat();
  REQUIRE(layerPtr->getParams().rows() == 5);
  REQUIRE(layerPtr->getParams().cols() == 6);
}

TEST_CASE("LayerCopyConstructor", "[layer][cpy][ctor]") {
  using namespace nnet;

  std::random_device rd;
  std::mt19937 mt{rd()};

  DenseLayer l1{5, 5, mt, Activation::Type::RELU, DenseLayer::InitType::Zeros};
  REQUIRE(l1.activation());

  DenseLayer l2{l1};
  REQUIRE(l2.activation());

  std::unique_ptr<DenseLayer> lptr1 = DenseLayer::create(
      5, 5, mt, Activation::Type::RELU, DenseLayer::InitType::UniformV1);
  REQUIRE(lptr1);
  REQUIRE(lptr1->activation());

  std::unique_ptr<DenseLayer> lptr2 =
      std::unique_ptr<DenseLayer>(new DenseLayer(*lptr1));
  REQUIRE(lptr2);
  REQUIRE(lptr2->activation());

  // std::cout << lptr2->toJson().dump(2) << '\n';
}