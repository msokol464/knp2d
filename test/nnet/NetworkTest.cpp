#include <cmath>
#include <iostream>
#include <numeric>

#include "../../kp2d/include/kp2d/CutProblemGenerator.h"
#include "../../kp2d/include/kp2d/ProblemGenerator.h"
#include "../../kp2d/include/kp2d/SolvableSet.h"
#include "../../nnet/include/nnet/DenseLayer.h"
#include "../../nnet/include/nnet/Network.h"
#include "../include/RangeMatchers.h"
#include "../include/Timer.h"
#include "../include/catch.hpp"

TEST_CASE("NetworkConstruction", "[net][ctor][forward]") {
  using namespace nnet;

  std::random_device rd;
  std::mt19937 mt{rd()};

  Network testnet{{{30, 23, Activation::Type::RELU},
                   {23, 15, Activation::Type::RELU},
                   {15, 8, Activation::Type::RELU},
                   {8, 1, Activation::Type::RELU}}};

  REQUIRE_NOTHROW(testnet.isValid());
  REQUIRE(testnet.numOfLayers() == 4);
  REQUIRE(testnet.numOfParams() == 9 + 128 + 360 + 713);

  DynMatrix input = DynMatrix::Random(30, 4);
  DynMatrix output{1, 4};

  REQUIRE_NOTHROW(output << testnet.forward(input));
}

TEST_CASE("NetworkParamSetting", "[net][set][get][params]") {
  using namespace nnet;

  std::random_device rd;
  std::mt19937 mt{rd()};

  Network testnet{{{10, 8, Activation::Type::TANH},
                   {8, 3, Activation::Type::TANH},
                   {3, 1, Activation::Type::TANH}}};
  REQUIRE_NOTHROW(testnet.isValid());

  std::vector<FloatType> paramVec{testnet.getParamVec()};
  REQUIRE(paramVec.size() == (unsigned)testnet.numOfParams());

  std::vector<FloatType> newParams(testnet.numOfParams());
  std::iota(newParams.begin(), newParams.end(), 0.5);

  DynMatrix mat = Eigen::Map<DynMatrix>(&newParams[0], newParams.size(), 1);

  REQUIRE_NOTHROW(testnet.setParams(std::move(mat)));

  std::cout << testnet.toJson().dump(2) << '\n';

  std::vector<FloatType> target(testnet.numOfParams());
  std::iota(target.begin(), target.end(), 0.5);
  REQUIRE_THAT(testnet.getParamVec(), EqualsRange(target));
}

/// TODO: add assertions
TEST_CASE("NetworkSaveToFile", "[net][json][save]") {
  using namespace nnet;

  std::random_device rd;
  std::mt19937 mt{rd()};

  Network testnet{{{10, 8, Activation::Type::TANH},
                   {8, 5, Activation::Type::TANH},
                   {5, 3, Activation::Type::TANH},
                   {3, 1, Activation::Type::ID}}};

  std::ofstream outFile;
  outFile.open("data/out/netTest.json", std::ios::out);
  testnet.saveToJson(outFile);
  outFile.close();
}

/// TODO: add assertions
TEST_CASE("NetworkReadFromFile", "[net][read]") {
  using namespace nnet;

  Network net = Network::loadFromJson("data/out/netTest.json");

  std::cout << net.toJson().dump(2) << '\n';
}

TEST_CASE("NetworkCalculations", "[net][calc]") {
  using namespace nnet;
  using Catch::Matchers::WithinAbs;
  constexpr FloatType margin = 0.000001;

  std::random_device rd;
  std::mt19937 mt{rd()};
  Network l1{{{3, 2, Activation::Type::TANH}}};
  std::vector<FloatType> param1{1, 1, 1, 1, 1, 1, 1, 1};
  l1.setParamsFromVector(std::move(param1));
  DynVector vec{3};
  DynVector out1{2};

  vec << 1.0, 2.0, 3.0;
  out1 = l1.forward(vec);

  std::cout << vec << '\n';
  std::cout << l1.toJson().dump(2) << '\n';
  std::cout << out1 << '\n';

  // std::cout << '\n' << out1 << '\n';
  CHECK_THAT(out1.coeff(0), WithinAbs(std::tanh(7), margin));
  CHECK_THAT(out1.coeff(1), WithinAbs(std::tanh(7), margin));

  Network l2{{{2, 1, Activation::Type::TANH}}};
  std::vector<FloatType> param2{1, 1, 1};
  l2.setParamsFromVector(std::move(param2));

  DynVector out2{1};
  out2 = l2.forward(out1);

  // std::cout << '\n' << out2 << '\n';
  REQUIRE_THAT(out2.coeff(0),
               WithinAbs(std::tanh(std::tanh(7) + std::tanh(7) + 1), margin));
}

TEST_CASE("NetworkCopyCtor", "[net][cpy][ctor]") {
  std::random_device rd;
  std::ranlux48 rnd{rd()};

  using namespace nnet;

  Network aNet{{{30, 23, Activation::Type::RELU},
                {23, 15, Activation::Type::RELU},
                {15, 8, Activation::Type::RELU},
                {8, 1, Activation::Type::RELU}}};

  // std::cout << aNet.toJson().dump(2) << '\n';
  REQUIRE(aNet.isValid());
  Network bNet{aNet};
  REQUIRE(bNet.isValid());
  REQUIRE(aNet.isEquiv(bNet));
}

TEST_CASE("NetworkEquivalence", "[equiv][net]") {
  std::random_device rd;
  std::ranlux48 rnd{rd()};
  using namespace nnet;

  Network net{
      {{30, 13, Activation::Type::TANH}, {13, 1, Activation::Type::RELU}}};
  Network netEq1{
      {{30, 13, Activation::Type::TANH}, {13, 1, Activation::Type::RELU}}};
  Network netEq2{
      {{30, 13, Activation::Type::TANH}, {13, 1, Activation::Type::RELU}}};
  Network netEq3{net};

  REQUIRE(net.isEquiv(netEq1));
  REQUIRE(net.isEquiv(netEq2));
  REQUIRE(net.isEquiv(netEq3));

  Network net1{
      {{30, 13, Activation::Type::TANH}, {13, 1, Activation::Type::TANH}}};
  Network net2{
      {{30, 11, Activation::Type::TANH}, {11, 1, Activation::Type::RELU}}};

  REQUIRE_FALSE(net.isEquiv(net1));
  REQUIRE_FALSE(net.isEquiv(net2));
}

TEST_CASE("NetworkBenchmark", "[timer][net]") {
  using kp2d::Point, kp2d::UniformInt, kp2d::NormalInt, kp2d::SolvableSet;
  using nnet::Activation, nnet::Network, nnet::DenseLayer;

  std::random_device rd;
  std::ranlux48 rnd{rd()};

  Timer stoper;

  kp2d::CutProblemGenerator genR{Point{1200, 800}, 34, 28,
                                 UniformInt::create(1, 6)};
  kp2d::ProblemGenerator genN{Point{1200, 800}, UniformInt::create(7, 30),
                              NormalInt::create(60, 5),
                              NormalInt::create(70, 1)};

  kp2d::ProblemSet<Network> setRunder = genR.generateSet<Network>(1u);
  kp2d::ProblemSet<Network> setRover = genR.generateSet<Network>(
      10u, kp2d::CutProblemGenerator::AreaOption::AreaOverflow);
  kp2d::ProblemSet<Network> setN =
      genN.generateSet<Network>(10u, genR.numOfGroups());

  kp2d::SizeType featNum = genR.numOfFeatures();
  std::cout << "[TEST_CASE] gen featNum: " << featNum << '\n';

  Network net{
      {{featNum, 25, Activation::Type::TANH}, {25, 1, Activation::Type::ID}}};

  SolvableSet<Network> solRU{std::move(setRunder), net};
  SolvableSet<Network> solRO{std::move(setRover), net};
  SolvableSet<Network> solN{std::move(setN), net};

  stoper.start();
  std::cout << "[TEST_CASE] solving divided underpacked" << featNum << '\n';
  solRU.solve();
  stoper.measure();

  // std::cout << "[TEST_CASE] solving divided overpacked: " << featNum << '\n';
  // solRO.solve();
  // stoper.measure();

  // std::cout << "[TEST_CASE] solving random packed: " << featNum << '\n';
  // solN.solve();
  // stoper.measure();

  std::cout << "Solving took:\n";
  for (size_t i = 0; i < stoper.numOfMeasurments(); i++) {
    std::cout << stoper.getSec(i) << "s \n";
  }

  std::ofstream outFile1, outFile2, outFile3;
  // outFile1.open("data/out/setNormal.json", std::ios::out);
  // solN.saveToJson(outFile1);
  // outFile1.close();

  // outFile2.open("data/out/setGApprox1.json", std::ios::out);
  // solRO.saveToJson(outFile2);
  // outFile2.close();

  outFile3.open("data/out/setGApprox2.json", std::ios::out);
  solRU.saveToJson(outFile3);
  outFile3.close();
}

TEST_CASE("NetworkBenchmark01", "[timer][net][micro]") {
  using kp2d::Point, kp2d::UniformInt, kp2d::NormalInt, kp2d::SolvableSet;
  using nnet::Activation, nnet::Network, nnet::DenseLayer;

  std::random_device rd;
  std::ranlux48 rnd{1001u};

  Timer stoper;

  kp2d::CutProblemGenerator genR{Point{1200, 800}, 34, 28,
                                 UniformInt::create(8, 64)};
  genR.setNumOfGroups(8);
  kp2d::ProblemGenerator genN{Point{1200, 800}, UniformInt::create(7, 30),
                              NormalInt::create(60, 5),
                              NormalInt::create(70, 1)};

  kp2d::ProblemSet<Network> setRunder = genR.generateSet<Network>(100u);
  kp2d::ProblemSet<Network> setRover = genR.generateSet<Network>(
      100u, kp2d::CutProblemGenerator::AreaOption::AreaOverflow);
  kp2d::ProblemSet<Network> setN =
      genN.generateSet<Network>(100u, genR.numOfGroups());

  // std::cout << setRover.toJson().dump(2) << '\n';
  kp2d::SizeType featNum = genR.numOfFeatures();
  std::cout << "[TEST_CASE] gen featNum: " << featNum << '\n';
  Network net{
      {{featNum, 25, Activation::Type::RELU}, {25, 1, Activation::Type::RELU}}};
  std::cout << "[TEST_CASE] net num of  params: " << net.numOfParams() << '\n';

  SolvableSet<Network> solRU{std::move(setRunder), net};
  SolvableSet<Network> solRO{std::move(setRover), net};
  SolvableSet<Network> solN{std::move(setN), net};

  stoper.start();
  solRU.solve();
  stoper.measure();
  solRO.solve();
  stoper.measure();
  solN.solve();
  stoper.measure();

  std::cout << "Solving took:\n";
  for (size_t i = 0; i < stoper.numOfMeasurments(); i++) {
    std::cout << stoper.getSec(i) << "s \n";
  }
}

TEST_CASE("NetworkParamSetting01", "[net][set][get][params]") {
  std::random_device rd;
  std::ranlux48 rnd{rd()};

  using namespace nnet;

  Network net1{
      {{5, 3, Activation::Type::RELU}, {3, 1, Activation::Type::RELU}}};

  Network net2{
      {{5, 3, Activation::Type::RELU}, {3, 1, Activation::Type::RELU}}};

  std::vector<FloatType> params = net1.getParamVec();
  DynMatrix mat = Eigen::Map<DynMatrix>(&params[0], params.size(), 1);

  std::cout << net2.toJson().dump(2) << '\n';
  net2.setParams(std::move(mat));
  std::cout << net2.toJson().dump(2) << '\n';

  std::ofstream log1{"data/out/opt/net1.json",
                     std::ios_base::out | std::ios_base::trunc};
  std::ofstream log2{"data/out/opt/net2.json",
                     std::ios_base::out | std::ios_base::trunc};

  net1.saveToJson(log1);
  net2.saveToJson(log2);

  log1.close();
  log2.close();
}