#!/bin/bash

__usage="
    Usage: $(basename $0) [OPTIONS]

    Options:
    -d --dir      Name of the parent directory for tests   (arg required)
    -i --iter     Number of iteration                      (arg required)
    -n --net-file Filename for solving network             (net by default)
    -s --sol-file Filename for solution file               (v_sol by defalult)
    -e --ext      File extension                           (json by default)
"
# DEFUALTS
dir=
iter=1
net=net
sol=v_sol
ext=json



while (($#)); do 

    case $1 in
        -d|--dir)
            if [ -n  $2 ] && [ ${2:0:1} != '-' ]; then
                dir=$2
                shift 2
            fi
            ;;
        -i|--iter)
            if [ -n  $2 ] && [ ${2:0:1} != '-' ]; then
                iter=$2
                shift 2
            fi
            ;;
        -n|--net-file)
            if [ -n  $2 ] && [ ${2:0:1} != '-' ]; then
                net=$2
                shift 2
            fi
            ;;
        -s|--sol-file)
            if [ -t  $2 ] && [ ${2:0:1} != '-' ]; then
                sol=$2
                shift 2
            fi
            ;;
        -*|--*)
            echo "$__usage"
            exit 1
            ;;
        *)
            echo "$__usage"
            exit 1
            ;;
    esac

done

mv ${dir}/${net}.${ext} ${dir}/${net}_${iter}.${ext} 
mv ${dir}/${sol}.${ext} ${dir}/${sol}_${iter}.${ext} 