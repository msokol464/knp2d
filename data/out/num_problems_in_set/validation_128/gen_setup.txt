  // Training setup
  // number of groups:
  int numGroups = 6;
  // problems per set:
  size_t numProblems = 1;
  // problems in validation:
  size_t numPValid = 128;
  int viter = 16;
  // number of replacement sets:
  size_t numTrainigSets = 5000;
  int riter = 1;
  const EvalType eval = EvalType::UnusedStorageArea;

  // Generator setup in constructor:
  //  -container
  //  -distribution for number of elements in group - N(avg, variance)
  //  -rectangle width distribution
  //  -rectangle height distribution
  int W = 1200;
  int H = 800;
  ProblemGenerator gen{Point{W, H}, NormalInt::create(10, 3),
                       UniformInt::create(0.2 * W, 0.8 * W),
                       UniformInt::create(0.2 * H, 0.8 * H),
                       FeatOption::allFeatures};
  gen.setNumOfGroups(numGroups);