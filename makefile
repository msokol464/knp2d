all: kp2d nnet test # app

kp2d:
	@cd kp2d && $(MAKE)

nnet:
	@cd nnet && $(MAKE)

app:
	@cd app && $(MAKE)

test:
	@cd test && $(MAKE)

testkp2d:
	@cd test && $(MAKE) kp2d




.PHONY: run cleankp2d kp2d nnet app test cleanobj cleantestkp2d cleantestnnet cleanproblem cleanopt

run:
	./bin/app

clean: cleantestkp2d cleantestnnet cleankp2d cleannnet

cleanproblem:
	@rm bin/kp2d/Problem.o
	@rm bin/test/kp2d/ProblemTest.o
	@rm bin/libkp2d.a
	@rm bin/testkp2d

cleankp2d:
	@cd kp2d && $(MAKE) clean

cleanobj:
	@rm -f bin/kp2d/*
	@rm -f bin/nnet/*

cleantestkp2d:
	@rm -f bin/testkp2d
	@rm -f bin/test/kp2d/*

cleantestnnet:
	@rm -f bin/testnnet
	@rm -f bin/test/nnet/*

cleanopt:
	@rm bin/test/nnet/OptimizerTest.*
	@rm bin/nnet/Optimizer.*
	@rm bin/libnnet.a

cleannnet:
	@cd nnet && $(MAKE) clean

cleanapp:
	@cd app && $(MAKE) clean