#include "include/nnet/DenseLayer.h"

#include "DenseLayer.h"

using nnet::DynMatrix;
using nnet::DynVector;

// DynVector nnet::DenseLayer::forward(const DynVector& features) const {
//   DynVector input{InDim + 1};
//   input << features, 1;
//   return activation_->apply(parametersWithBias_ * input);
// }

void nnet::DenseLayer::resizeParamsToVec() {
  parametersWithBias_.resize(numOfParams(), 1);
}

void nnet::DenseLayer::resizeParamsToMat() {
  parametersWithBias_.resize(OutDim, InDim + 1);
}

std::ofstream& nnet::DenseLayer::saveToJson(std::ofstream& oFile) const {
  if (oFile.good()) {
    oFile << "InputDim " << InDim << " OutputDim " << OutDim << '\n';
    oFile << parametersWithBias_ << '\n' << activation_->toString() << '\n';
  }
  return oFile;
}

nlohmann::ordered_json nnet::DenseLayer::toJson() const {
  using json = nlohmann::ordered_json;
  json layerJson, mData, lData;
  layerJson["ObjectType"] = "DenseLayer";
  mData["InputDim"] = InDim;
  mData["OutputDim"] = OutDim;
  mData["Activation"] = activation_->toString();
  layerJson["Metadata"] = mData;

  lData = json::array();
  for (SizeType row = 0; row < parametersWithBias_.rows(); row++) {
    json rData = json::array();
    for (SizeType col = 0; col < parametersWithBias_.cols(); col++)
      rData.push_back(parametersWithBias_(row, col));
    lData.push_back(rData);
  }
  // resizeParamsToMat();

  layerJson["Data"] = lData;
  return layerJson;
}

std::unique_ptr<nnet::DenseLayer> nnet::DenseLayer::fromJson(
    const nlohmann::json& layerJson) {
  expect(layerJson.at("ObjectType") == "DenseLayer",
         ErrorCode::UnexpectedObjType, __PRETTY_FUNCTION__);

  size_t numOfRows = layerJson.at("Metadata").at("OutputDim");
  size_t numOfCols = layerJson.at("Metadata").at("InputDim");
  numOfCols++;  // numOfCols = InputDim + 1

  expect(layerJson.at("Data").size() == numOfRows,
         ErrorCode::UnstructuredLayerJson, __PRETTY_FUNCTION__);

  DynMatrix params{numOfRows, numOfCols};

  for (size_t row = 0; row < numOfRows; row++) {
    expect(layerJson.at("Data").at(row).size() == numOfCols,
           ErrorCode::UnstructuredLayerJson, __PRETTY_FUNCTION__);

    for (size_t col = 0; col < numOfCols; col++) {
      params(row, col) = layerJson.at("Data").at(row).at(col);
    }
  }

  return DenseLayer::create(
      std::move(params),
      Activation::typeMap.at(layerJson.at("Metadata").at("Activation")));
}

void nnet::DenseLayer::appendParamBounds(
    std::vector<FloatType>& lBounds, std::vector<FloatType>& uBounds) const {
  for (int i = 0; i < numOfParams(); i++) {
    uBounds.push_back(paramUpperBound_);
    lBounds.push_back(paramLowerBound_);
  }
}

DynMatrix nnet::DenseLayer::forward(const DynMatrix& features) const {
  assert(features.rows() + 1 == parametersWithBias_.cols());
  DynMatrix input{InDim + 1, features.cols()};
  input << features, DynMatrix::Ones(1, features.cols());
  // std::cout << "[DenseLayer::forward] input\n" << input << '\n';
  // std::cout << "[DenseLayer::forward] params*input\n"
  //           << parametersWithBias_ * input << '\n';
  return activation_->apply(parametersWithBias_ * input);
}

bool nnet::DenseLayer::isEquiv(const DenseLayer& other) const {
  // check for dimensions mismatch
  if (InDim != other.InDim || OutDim != other.OutDim) return false;

  if (!activation() || !other.activation()) return false;

  // check for activation match
  if (activType() != other.activType()) return false;
  return true;
}