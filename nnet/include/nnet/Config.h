#pragma once

#include <eigen3/Eigen/Dense>
#include <random>

namespace nnet {

using SizeType = long int;
using FloatType = double;
using DynVector = Eigen::Matrix<FloatType, Eigen::Dynamic, 1>;
using DynMatrix = Eigen::Matrix<FloatType, Eigen::Dynamic, Eigen::Dynamic>;

}  // namespace nnet