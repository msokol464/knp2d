#pragma once

#include <mutex>

#include "../../../kp2d/include/kp2d/SolvableSet.h"
#include "Config.h"
#include "Network.h"

namespace nnet {

template <class Solvable>
class ObjFunction {
 public:
  const static bool IsThreadSafe;    // = true;
  const static int DefaultMaxEvals;  // = 200000;

  FloatType operator()(const double* x, const int dim);

  ObjFunction(Solvable model, size_t poolSize) : sample_{model} {
    problems_.reserve(poolSize * 2);
    for (size_t i = 0; i < poolSize; i++) {
      avaliableIndicies_.push_back(i);
      problems_.emplace_back(sample_);
    }
  }

  ObjFunction(const ObjFunction& fun);

  std::vector<FloatType> sampleParams() const { return sample_.params(); }
  void getBounds(std::vector<FloatType>& lBounds,
                 std::vector<FloatType>& uBounds) const {
    sample_.getBounds(lBounds, uBounds);
  }
  int dim() const { return sample_.dim(); }

  ~ObjFunction() = default;

  template <class TProblemSet>
  void replaceSampleSet(const TProblemSet& set) {
    std::lock_guard lock{mutex_};
    sample_.replaceSet(set);
    // std::cout << sample_.toJson().dump(2) << '\n';
  }

  Solvable getSample() {
    std::lock_guard lock{mutex_};
    return sample_;
  }

  Network getSampleNet() {
    std::lock_guard lock{mutex_};
    return sample_.getSolver();
  }

 private:
  Solvable sample_;

  std::vector<size_t> avaliableIndicies_;
  std::vector<Solvable> problems_;

  std::mutex mutex_;
};

template <class Solvable>
inline FloatType ObjFunction<Solvable>::operator()(const double* x,
                                                   const int dim) {
  assert(dim == sample_.dim());
  size_t idx = 0;
  {
    std::lock_guard lock{mutex_};
    if (avaliableIndicies_.size() == 0) {
      problems_.resize(problems_.size() * 2, sample_);
      avaliableIndicies_.push_back(problems_.size());
    }
    idx = avaliableIndicies_.back();
    avaliableIndicies_.pop_back();
  }
  FloatType eval = problems_[idx](x, dim);
  {
    std::lock_guard lock{mutex_};
    problems_[idx].replaceSet(sample_.problemSet());
    avaliableIndicies_.push_back(idx);
  }
  return eval;
}

}  // namespace nnet
