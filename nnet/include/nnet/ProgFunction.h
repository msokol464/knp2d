#pragma once

#include <libcmaes/cmaes.h>

#include "Config.h"

namespace nnet {

namespace cma = libcmaes;

template <class TProblemSet, class Objective,
          class GPStrategy = cma::NoBoundStrategy>
class ProgFunction {
 public:
  ProgFunction(const std::vector<TProblemSet>& samples, Objective& obj,
               int iter)
      : samples_{samples.cbegin(), samples.cend()},
        objective_{obj},
        iter_{iter},
        next_{0} {
    std::cout << "[ProgFunction::ctor] Sets vec size: " << samples_.size()
              << '\n';
  }

  ProgFunction(Objective& obj)
      : samples_{}, objective_{obj}, iter_{-1}, next_{0} {
    std::cout << "[ProgFunction::ctor] No replacement option\n";
  }

  // template <class ProblemGenerator, class Solver>
  // ProgFunction(ProblemGenerator& pg, Objective& obj, int iter, int numOfSets,
  //              size_t numOfProblems)
  //     : objective_{obj}, iter_{iter}, next_{0} {
  //   samples_.reserve(numOfSets * 2);
  //   for (int i = 0; i < numOfSets; i++) {
  //     samples_.push_back(pg.generateSet(numOfProblems));
  //   }
  // }

  ProgFunction(const ProgFunction&) = default;

  size_t nextIdx() const { return next_; }
  int iterPeriod() const { return iter_; }
  size_t numOfSets() const { return samples_.size(); }
  TProblemSet nextSet() const { return samples_[next_]; }

  void next() { ++next_ %= samples_.size(); }

  void changeObjectiveSample() {
    std::cout
        << "[ProgFunction::changeObjectiveSample] Problem set replacement nr: "
        << next_ << '\n';
    objective_.template replaceSampleSet<TProblemSet>(samples_[next_]);
  }

  int operator()(
      const cma::CMAParameters<cma::GenoPheno<GPStrategy>>& cmaparams,
      const cma::CMASolutions& cmasols);

 private:
  std::vector<TProblemSet> samples_;
  Objective& objective_;
  int iter_;
  size_t next_;
};

template <class TProblemSet, class Objective, class GPStrategy>
inline int ProgFunction<TProblemSet, Objective, GPStrategy>::operator()(
    const cma::CMAParameters<cma::GenoPheno<GPStrategy>>& cmaparams,
    const cma::CMASolutions& cmasols) {
  (void)cmaparams;
  if (iter_ <= 0 || samples_.size() == 0) {
    std::cout << "[ProgFunction] Iteration number: " << cmasols.niter() << '\n';
  } else if (cmasols.niter() % iter_ == 0) {
    changeObjectiveSample();
    std::cout << cmasols.niter() << '\n';
    next();
  }
  return 0;
}

}  // namespace nnet