#pragma once

#include <algorithm>
#include <fstream>
#include <initializer_list>
#include <memory>
#include <tuple>
#include <vector>

#include "../nlohmann/json.hpp"
#include "DenseLayer.h"

namespace nnet {

class Network {
 public:
  // template <class RndEngine>
  // Network(std::initializer_list<
  //             std::tuple<SizeType, SizeType, Activation::Type>>&& argsList,
  //         DenseLayer::InitType init, RndEngine& engine)
  //     : numOfParams_{0} {
  //   for (auto args : argsList) {
  //     auto&& [inDim, outDim, activType] = args;
  //     addLayer(DenseLayer::create(inDim, outDim, engine, activType, init));
  //   }
  // }

  Network(
      std::initializer_list<std::tuple<SizeType, SizeType, Activation::Type>>&&
          argsList)
      : numOfParams_{0} {
    for (auto args : argsList) {
      auto&& [inDim, outDim, activType] = args;
      addLayer(DenseLayer::create(inDim, outDim, activType,
                                  DenseLayer::InitType::Zeros));
    }
  }

  Network(const Network& other);
  Network() = default;

  Network& operator=(const Network& net);

  bool isValid() const;

  bool isEquiv(const Network& other) const;

  SizeType numOfLayers() const { return layers_.size(); }
  SizeType numOfParams() const { return numOfParams_; }

  SizeType inputDim() const {
    if (layers_.empty())
      return 0;
    else
      return layers_.front()->InDim;
  }
  SizeType outputDim() const {
    if (layers_.empty())
      return 0;
    else
      return layers_.back()->OutDim;
  }

  DynMatrix forward(const DynMatrix& features) const;

  void getParamBounds(std::vector<FloatType>& uBounds,
                      std::vector<FloatType>& lBounds) const;

  std::vector<FloatType> getParamVec() const;
  void setParamsFromVector(std::vector<FloatType>&& paramVec);

  void setParams(DynMatrix&& paramVec);

  ~Network() {}

  nlohmann::ordered_json toJson() const;
  void saveToJson(std::ofstream& oFile) const;

  static Network fromJson(const nlohmann::json& netJson);
  static Network loadFromJson(const std::string& filename);

  Network(SizeType numOfLayers) : numOfParams_{0} {
    // layers_ = std::vector<std::unique_ptr<DenseLayer>>();
    layers_.reserve(numOfLayers);
  }
  void addLayer(std::unique_ptr<DenseLayer>&& layer);

 private:
  void addLayer(DynMatrix&& params, Activation::Type funType);

  std::vector<std::unique_ptr<DenseLayer>> layers_;
  SizeType numOfParams_;
};

}  // namespace nnet
