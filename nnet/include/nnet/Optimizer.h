#pragma once

#include <libcmaes/cmaes.h>

#include <functional>
#include <iostream>
#include <random>

#include "Network.h"

namespace nnet {

namespace cma = libcmaes;

template <class GPStrategy>
struct isBoundStrategy {
  static constexpr bool value = false;
};

template <>
struct isBoundStrategy<cma::pwqBoundStrategy> {
  static constexpr bool value = true;
};

template <class Objective, class GPStrategy = cma::NoBoundStrategy>
class Optimizer {
 public:
  Optimizer(
      Objective&& funObj, double sigma,
      std::function<int(const cma::CMAParameters<cma::GenoPheno<GPStrategy>>&,
                        const cma::CMASolutions&)>
          pfunc = cma::CMAStrategy<cma::CovarianceUpdate,
                                   cma::GenoPheno<GPStrategy>>::_defaultPFunc,
      int lambda = -1, int maxEvals = Objective::DefaultMaxEvals);

  void run();

  void print() {
    std::cout << "Best solution: " << cmasols_ << std::endl;
    std::cout << "Optimization took " << cmasols_.elapsed_time() / 1000.0
              << " seconds\n";
  }

  int optimizationTime() const { return cmasols_.elapsed_time(); };

  ~Optimizer() {}

  Network getResult();
  std::vector<Network> getResults() const;

 private:
  // cma::FitFunc func_;
  Objective objFunction_;

  cma::ProgressFunc<cma::CMAParameters<cma::GenoPheno<GPStrategy>>,
                    cma::CMASolutions>
      pfunc_;
  cma::CMAParameters<cma::GenoPheno<GPStrategy>> cmaparams_;
  cma::CMASolutions cmasols_;
  std::vector<FloatType> initParamVec_;
};

template <class Objective, class GPStrategy>
inline Optimizer<Objective, GPStrategy>::Optimizer(
    Objective&& funObj, double sigma,
    std::function<int(const cma::CMAParameters<cma::GenoPheno<GPStrategy>>&,
                      const cma::CMASolutions&)>
        pfunc,
    int lambda, int maxEvals)
    : objFunction_{std::move(funObj)} {
  // func_ = funObj;
  pfunc_ = pfunc;
  initParamVec_ = objFunction_.sampleParams();

  if constexpr (isBoundStrategy<GPStrategy>::value) {
    std::vector<FloatType> lBounds, uBounds;
    objFunction_.getBounds(lBounds, uBounds);
    cma::GenoPheno<cma::pwqBoundStrategy> gp{lBounds.data(), uBounds.data(),
                                             objFunction_.dim()};
    cmaparams_ = cma::CMAParameters<cma::GenoPheno<cma::pwqBoundStrategy>>(
        initParamVec_, sigma, lambda, 0, gp);
  } else {
    for (const auto& value : initParamVec_) {
      std::cout << value << '\t';
    }
    std::cout << '\n';
    cmaparams_ = cma::CMAParameters<cma::GenoPheno<GPStrategy>>{initParamVec_,
                                                                sigma, lambda};
  }

  cmaparams_.set_mt_feval(Objective::IsThreadSafe);
  cmaparams_.set_algo(sepaCMAES);
  cmaparams_.set_max_fevals(maxEvals);
  cmaparams_.set_quiet(false);
  cmaparams_.set_maximize(false);
  cmaparams_.set_stopping_criteria(1, true);
}

template <class Objective, class GPStrategy>
inline void Optimizer<Objective, GPStrategy>::run() {
  using namespace libcmaes;

  cma::FitFunc func = objFunction_;

  cmasols_ = cmaes<GenoPheno<GPStrategy>>(func, cmaparams_, pfunc_);
}

template <class Objective, class GPStrategy>
inline Network Optimizer<Objective, GPStrategy>::getResult() {
  dVec params = cmasols_.best_candidate().get_x_dvec();
  Network net{objFunction_.getSampleNet()};

  net.setParams(std::move(params));
  return net;
}

// template <class Objective, class GPStrategy>
// inline std::vector<Network> Optimizer<Objective, GPStrategy>::getResults()
//     const {

//   return std::vector<Network>();
// }

}  // namespace nnet
