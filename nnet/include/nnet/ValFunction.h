#pragma once

#include <libcmaes/cmaes.h>

#include "Config.h"

namespace nnet {

namespace cma = libcmaes;

template <class Solvable, class PSet, class Objective,
          class GPStrategy = cma::NoBoundStrategy>
class ValFunction {
 private:
  Objective& objective_;

  Solvable validationSet_;
  int vIter_;

  std::vector<PSet> trainingProblems_;
  int rIter_;

  std::string logFileName_;
  std::string solPathName_;
  std::string netFileName_;

  size_t next_;

 public:
  ValFunction(Objective& obj, Solvable vSet, int validationIter,
              const std::vector<PSet>& tProblems, int replacementIter,
              const std::string& fName, const std::string& sPath,
              const std::string& nName)
      : objective_{obj},
        validationSet_{vSet},
        vIter_{validationIter},
        trainingProblems_{tProblems},
        rIter_{replacementIter},
        logFileName_{fName},
        solPathName_{sPath},
        netFileName_{nName},
        next_{0} {
    std::cout << "[ValFunction::ctor] Sets vec size: "
              << trainingProblems_.size() << '\n';
  }

  ValFunction(const ValFunction&) = default;

  size_t numOfSets() const { return trainingProblems_.size(); }
  PSet nextSet() const { return trainingProblems_[next_]; }

  void next() { ++next_ %= trainingProblems_.size(); }

  void changeObjectiveSample() {
    std::cout
        << "[ValFunction::changeObjectiveSample] Problem set replacement nr: "
        << next_ << '\n';
    objective_.template replaceSampleSet<PSet>(trainingProblems_[next_]);
  }

  int operator()(
      const cma::CMAParameters<cma::GenoPheno<GPStrategy>>& cmaparams,
      const cma::CMASolutions& cmasols);
};

template <class Solvable, class PSet, class Objective, class GPStrategy>
inline int ValFunction<Solvable, PSet, Objective, GPStrategy>::operator()(
    const cma::CMAParameters<cma::GenoPheno<GPStrategy>>& cmaparams,
    const cma::CMASolutions& cmasols) {
  (void)cmaparams;

  if (cmasols.niter() == 0) {
    std::ofstream log{solPathName_ + '/' + logFileName_,
                      std::ios_base::out | std::ios_base::app};
    log << "Iter.\tMean eval. (best)\tMean eval. (mean)\n";
    log.close();
  }

  if (cmasols.niter() % rIter_ == 0) {
    changeObjectiveSample();
    std::cout << cmasols.niter() << '\n';
    next();
  }

  if (cmasols.niter() % vIter_ == 0) {
    Solvable vset1 = validationSet_;
    vset1.replaceParams(cmasols.best_candidate().get_x_dvec());
    FloatType evalBest = vset1.solve();

    Solvable vset2 = validationSet_;
    vset2.replaceParams(cmasols.xmean());
    FloatType evalMean = vset2.solve();

    std::ofstream log{(solPathName_ + '/' + logFileName_),
                      std::ios_base::out | std::ios_base::app};
    log << cmasols.niter() << "\t\t" << std::fixed << std::setprecision(6)
        << evalBest << '\t' << evalMean << '\n';
    // vset2.evalsToStream(log);
    // log << '\n';
    log.close();

    std::ofstream sol{(solPathName_ + "/v_sol.json").c_str(),
                      std::ios_base::out | std::ios_base::trunc};
    vset2.saveToJson(sol);
    sol.close();

    std::ofstream net1{(solPathName_ + "/b_" + netFileName_).c_str(),
                       std::ios_base::out | std::ios_base::trunc};
    vset1.saveSolverToJson(net1);
    net1.close();

    std::ofstream net2{(solPathName_ + "/m_" + netFileName_).c_str(),
                       std::ios_base::out | std::ios_base::trunc};
    vset2.saveSolverToJson(net2);
    net2.close();
  }

  return 0;
}

}  // namespace nnet