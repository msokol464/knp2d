#pragma once

#include <fstream>
#include <memory>
#include <sstream>

#include "../nlohmann/json.hpp"
#include "Activations.h"
#include "Config.h"
#include "Error.h"

namespace nnet {

class DenseLayer {
 public:
  const SizeType InDim;
  const SizeType OutDim;

  enum InitType { Uniform, UniformV1, UniformV2, Zeros };

  template <class RndEngine>
  DenseLayer(SizeType inDim, SizeType outDim, RndEngine& gen,
             Activation::Type funType, InitType init);

  DenseLayer(SizeType inDim, SizeType outDim, Activation::Type funType,
             FloatType upperBound = 10., FloatType lowerBound = -10.);

  DenseLayer(DynMatrix&& params, Activation::Type funType)
      : InDim{params.cols() - 1},
        OutDim{params.rows()},
        parametersWithBias_{params},
        activation_{Activation::create(funType)} {}

  DenseLayer(const DenseLayer& dl);

  SizeType numOfParams() const { return OutDim * (InDim + 1); }
  FloatType lowerBound() const { return paramLowerBound_; }
  FloatType upperBound() const { return paramUpperBound_; }

  DynMatrix forward(const DynMatrix& features) const;
  const std::unique_ptr<Activation>& activation() const { return activation_; }
  const DynMatrix& getParams() const { return parametersWithBias_; }

  void resizeParamsToVec();
  void resizeParamsToMat();

  Activation::Type activType() const { return activation_->type(); }

  void setParams(DynMatrix&& newParams) {
    if (newParams.rows() == OutDim && newParams.cols() == InDim + 1)
      parametersWithBias_ = std::move(newParams);
  }

  std::ofstream& saveToJson(std::ofstream& oFile) const;
  nlohmann::ordered_json toJson() const;
  bool isEquiv(const DenseLayer& other) const;

  static std::unique_ptr<DenseLayer> fromJson(const nlohmann::json& layerJson);

  template <class RndEngine>
  static std::unique_ptr<DenseLayer> create(SizeType inDim, SizeType outDim,
                                            RndEngine& gen,
                                            Activation::Type funType,
                                            InitType init) {
    return std::unique_ptr<DenseLayer>(
        new DenseLayer(inDim, outDim, gen, funType, init));
  }

  static std::unique_ptr<DenseLayer> create(SizeType inDim, SizeType outDim,
                                            Activation::Type funType,
                                            FloatType upperBound = 10.,
                                            FloatType lowerBound = -10.) {
    return std::unique_ptr<DenseLayer>(
        new DenseLayer(inDim, outDim, funType, upperBound, lowerBound));
  }

  static std::unique_ptr<DenseLayer> create(DynMatrix&& params,
                                            Activation::Type funType) {
    return std::unique_ptr<DenseLayer>(
        new DenseLayer(std::move(params), funType));
  }

  static std::unique_ptr<DenseLayer> create(const DenseLayer& layer) {
    return std::unique_ptr<DenseLayer>(new DenseLayer(layer));
  }

  void appendParamBounds(std::vector<FloatType>& lBounds,
                         std::vector<FloatType>& uBounds) const;

  ~DenseLayer() {}

 private:
  DynMatrix parametersWithBias_;
  std::unique_ptr<Activation> activation_;
  FloatType paramUpperBound_;
  FloatType paramLowerBound_;
};

/// TODO: Error handling if unknown init type is passed
template <class RndEngine>
DenseLayer::DenseLayer(SizeType inDim, SizeType outDim, RndEngine& gen,
                       Activation::Type funType, DenseLayer::InitType init)
    : InDim{inDim}, OutDim{outDim}, activation_{Activation::create(funType)} {
  FloatType bound;

  if (init == InitType::Zeros) {
    parametersWithBias_ = DynMatrix::Zero(outDim, inDim + 1);
    paramUpperBound_ = 10;
    paramLowerBound_ = -10;

  } else if (init == InitType::UniformV2) {
    /// params ~ U( - 6/sqrt(indim + outdim), 6/sqrt(indim + outdim) )
    bound = 6 / sqrt(static_cast<FloatType>(InDim + OutDim));
    std::uniform_real_distribution<FloatType> U{-bound, bound};
    parametersWithBias_ = DynMatrix::NullaryExpr(
        outDim, inDim + 1, [&U, &gen]() { return U(gen); });
    paramUpperBound_ = bound;
    paramLowerBound_ = -bound;

  } else if (init == InitType::UniformV1) {
    /// params ~ U( - 1/sqrt(indim), 1/sqrt(indim) )
    bound = 1 / sqrt(static_cast<FloatType>(InDim));
    std::uniform_real_distribution<FloatType> U{-bound, bound};
    parametersWithBias_ = DynMatrix::NullaryExpr(
        outDim, inDim + 1, [&U, &gen]() { return U(gen); });
    paramUpperBound_ = bound;
    paramLowerBound_ = -bound;

  } else {  // init == InitType::Uniform
    bound = 10;
    std::uniform_real_distribution<FloatType> U{-bound, bound};
    parametersWithBias_ = DynMatrix::NullaryExpr(
        outDim, inDim + 1, [&U, &gen]() { return U(gen); });
    paramUpperBound_ = bound;
    paramLowerBound_ = -bound;
  }
}

inline DenseLayer::DenseLayer(SizeType inDim, SizeType outDim,
                              Activation::Type funType, FloatType upperBound,
                              FloatType lowerBound)
    : InDim{inDim},
      OutDim{outDim},
      activation_{Activation::create(funType)},
      paramUpperBound_{upperBound},
      paramLowerBound_{lowerBound} {
  parametersWithBias_ = DynMatrix::Zero(outDim, inDim + 1);
}

inline DenseLayer::DenseLayer(const DenseLayer& dl)
    : InDim{dl.InDim},
      OutDim{dl.OutDim},
      parametersWithBias_{dl.parametersWithBias_},
      paramUpperBound_{dl.paramUpperBound_},
      paramLowerBound_{dl.paramLowerBound_} {
  activation_ = Activation::create(dl.activation_->toString());
}

}  // namespace nnet
