#pragma once

#include <iostream>
#include <memory>
#include <unordered_map>

#include "Config.h"
#include "Error.h"

namespace nnet {

class Activation {
 public:
  enum Type { ID, RELU, SIGM, TANH, SMAX };
  static const std::unordered_map<std::string, Type> typeMap;

  static std::string typeDesc(Type typeCode);
  static std::unique_ptr<Activation> create(Type funType);
  static std::unique_ptr<Activation> create(const std::string& name);

  std::string toString() const { return desc_; }

  virtual DynMatrix apply(DynMatrix input) = 0;
  virtual Type type() const = 0;

  virtual ~Activation() = default;

  Activation(std::string&& desc) : desc_{desc} {}

 protected:
  std::string desc_;
};

class Identity : public Activation {
 public:
  Identity() : Activation("Identity") {}

  DynMatrix apply(DynMatrix input) override { return input; }
  Type type() const { return Type::ID; }
};

class ReLU : public Activation {
 public:
  ReLU() : Activation("ReLU"){};

  DynMatrix apply(DynMatrix input) override {
    return input.array().max(0).matrix();
  }
  Type type() const { return Type::RELU; }
};

class Tanh : public Activation {
 public:
  Tanh() : Activation("Tanh"){};
  DynMatrix apply(DynMatrix input) override {
    // DynMatrix out{input.rows(), input.cols()};
    // std::cout << "[Activation::apply] input\n" << input << '\n';
    // out = input.array().tanh().matrix();
    // std::cout << "[Activation::apply] output\n" << out << '\n';
    return input.array().tanh().matrix();
  }
  Type type() const { return Type::TANH; }
};

class Sigmoid : public Activation {
 public:
  Sigmoid() : Activation("Sigmoid"){};

  DynMatrix apply(DynMatrix input) override {
    return (1. / 1. + (-input.array()).exp()).matrix();
  }
  Type type() const { return Type::SIGM; }
};

class SoftMax : public Activation {
 public:
  SoftMax() : Activation("SoftMax"){};

  DynMatrix apply(DynMatrix input) override {
    return (input.array().exp() / input.array().exp().sum()).matrix();
  }
  Type type() const { return Type::SMAX; }
};

}  // namespace nnet
