#pragma once

#include <exception>
#include <iostream>

namespace nnet {
enum class ErrorAction { Logging, Throwing, Ignoring, Terminating };

constexpr ErrorAction defaultErrorAction = ErrorAction::Logging;

enum class ErrorCode {
  FileFailure,
  UnsupportedFileExt,
  UnstructuredNetJson,
  UnstructuredLayerJson,
  UnmatchedLayerDims,
  UnequalNumOfParams,
  NullActivationPtr,
  NullLayerPtr,
  UnknownActivation,
  UnexpectedObjType,
  FaultyNumOfParams
};

const std::vector<std::string> ErrorName{
    "File...",
    "File extension is not supported",
    "Faulty stucture of network json file",
    "Faulty stucture of layer in json file",
    "Layer has unmatched dimesions",
    "Number of params is unequal to value read from file",
    "Activation points to nullptr",
    "Unregistered activation type",
    "Unexpected object type in json file",
    "Network numOfParams_ doesn't reflect actual number of parmeters"};

template <ErrorAction action = defaultErrorAction, class Callable>
constexpr void expect(Callable expectedCondition, ErrorCode code,
                      const std::string& functionName) {
  if constexpr (action == ErrorAction::Logging) {
    if (!expectedCondition())
      std::cerr << "Error in " << functionName << ": "
                << ErrorName[static_cast<size_t>(code)] << '\n';
  } else if constexpr (action == ErrorAction::Throwing) {
    if (!expectedCondition()) throw code;
  } else if constexpr (action == ErrorAction::Terminating) {
    if (!expectedCondition()) std::terminate();
  }
  // else ErrorAction::Ignoring
}

template <ErrorAction action = defaultErrorAction>
constexpr void expect(bool condition, ErrorCode code,
                      const std::string& functionName) {
  if constexpr (action == ErrorAction::Logging) {
    if (!condition)
      std::cerr << "Error in " << functionName << ": "
                << ErrorName[static_cast<size_t>(code)] << '\n';
  } else if constexpr (action == ErrorAction::Throwing) {
    if (!condition) throw code;
  } else if constexpr (action == ErrorAction::Terminating) {
    if (!condition) std::terminate();
  }
  // else ErrorAction::Ignoring
}

}  // namespace nnet
