
#include "include/nnet/Network.h"

#include <iostream>

#include "Network.h"
#include "include/nnet/Error.h"

using nnet::DynMatrix;
using nnet::DynVector;
using nnet::FloatType;

// std::vector<std::unique_ptr<DenseLayer>> layers_;
// SizeType numOfParams_;
nnet::Network::Network(const Network& net)
    : layers_{}, numOfParams_{net.numOfParams_} {
  for (const auto& layer : net.layers_) {
    if (layer) {
      // std::unique_ptr<DenseLayer> lptr2 =
      //     std::unique_ptr<DenseLayer>(new DenseLayer(*layer));
      // std::cout << lptr2.get() << "->" << lptr2->InDim << ", " <<
      // lptr2->OutDim
      //           << '\n';
      // this->layers_.push_back(std::move(lptr2));
      this->layers_.push_back(DenseLayer::create(*layer));
    }
  }
}

nnet::Network& nnet::Network::operator=(const Network& net) {
  // TODO: insert return statement here
  layers_.reserve(net.numOfLayers());
  numOfParams_ = net.numOfParams();

  for (const auto& layer : net.layers_) {
    if (layer) {
      this->layers_.push_back(DenseLayer::create(*layer));

      // std::unique_ptr<DenseLayer> lptr2 =
      //     std::unique_ptr<DenseLayer>(new
      //     DenseLayer(*layer));DenseLayer::create(*layer));
    }
  }
  return *this;
}

bool nnet::Network::isValid() const {
  expect(layers_.size() > 0, ErrorCode::NullLayerPtr, __PRETTY_FUNCTION__);
  if (layers_.size() == 0) return false;
  SizeType lastOutDim = layers_.front()->InDim;
  SizeType paramCheck = 0;
  for (const auto& layer : layers_) {
    expect(layer != nullptr, ErrorCode::NullLayerPtr, __PRETTY_FUNCTION__);
    expect(layer->InDim == lastOutDim, ErrorCode::UnmatchedLayerDims,
           __PRETTY_FUNCTION__);
    expect(layer->activation() != nullptr, ErrorCode::NullActivationPtr,
           __PRETTY_FUNCTION__);

    if (layer == nullptr)
      return false;
    else if (layer->InDim != lastOutDim)
      return false;
    else if (layer->activation() == nullptr)
      return false;

    lastOutDim = layer->OutDim;
    paramCheck += layer->numOfParams();
  }
  expect<ErrorAction::Throwing>(numOfParams_ == paramCheck,
                                ErrorCode::FaultyNumOfParams,
                                __PRETTY_FUNCTION__);
  return true;
}

DynMatrix nnet::Network::forward(const DynMatrix& features) const {
  DynMatrix output;

  // std::cout << "[Network::forward] features dim: " << features.rows() << "x"
  //           << features.cols() << '\n';
  // std::cout << "[Network::forward] input dim: " << inputDim() << '\n';

  assert(features.rows() == inputDim());
  DynMatrix input = features;

  for (const auto& layer : layers_) {
    output.resize(layer->OutDim, features.cols());
    // std::cout << "[Network::forward] input\n" << input << '\n';
    output = layer->forward(input);
    // std::cout << "[Network::forward] output\n" << output << '\n';

    input.resize(layer->OutDim, features.cols());
    input = output;
  }
  return output;
}

void nnet::Network::getParamBounds(std::vector<FloatType>& lBounds,
                                   std::vector<FloatType>& uBounds) const {
  uBounds.clear();
  uBounds.reserve(numOfParams());
  lBounds.clear();
  lBounds.reserve(numOfParams());
  for (const auto& layer : layers_) {
    layer->appendParamBounds(lBounds, uBounds);
  }
}

std::vector<FloatType> nnet::Network::getParamVec() const {
  DynVector paramVec{numOfParams_};

  SizeType idx = 0;
  for (const auto& layer : layers_) {
    layer->resizeParamsToVec();
    paramVec.block(idx, 0, layer->numOfParams(), 1) = layer->getParams();
    layer->resizeParamsToMat();
    idx += layer->numOfParams();
  }

  return std::vector<FloatType>(&paramVec[0],
                                paramVec.data() + paramVec.size());
}

void nnet::Network::setParamsFromVector(std::vector<FloatType>&& paramVec) {
  SizeType idx = 0;
  for (const auto& layer : layers_) {
    Eigen::Map<DynMatrix> temp(&paramVec[idx], layer->OutDim, layer->InDim + 1);
    layer->setParams(temp);
    idx += layer->numOfParams();
  }
}

void nnet::Network::setParams(DynMatrix&& paramVec) {
  SizeType beginIdx = 0;
  SizeType size = 0;
  for (const auto& layer : layers_) {
    size = layer->OutDim * (layer->InDim + 1);

    DynMatrix x = paramVec.block(beginIdx, 0, size, 1);
    // std::cout << x.rows() << 'x' << x.cols() << '\n';
    // std::cout << x << '\n' << '\n';
    x.resize(layer->OutDim, layer->InDim + 1);
    // std::cout << x.rows() << 'x' << x.cols() << '\n';
    // std::cout << x << '\n';
    // std::cout << "---------------------------------<<\n";

    layer->setParams(std::move(x));
    beginIdx += size;
  }
  // TODO
}

nlohmann::ordered_json nnet::Network::toJson() const {
  using json = nlohmann::ordered_json;
  json netJson, mData, lData;

  netJson["Libname"] = "nnet";
  netJson["ObjectType"] = "Network";

  mData["NumOfParams"] = numOfParams();
  mData["NumOfLayers"] = numOfLayers();
  netJson["Metadata"] = mData;

  lData = json::array();
  for (const auto& layer : layers_) {
    lData.push_back(layer->toJson());
  }
  netJson["Data"] = lData;
  return netJson;
}

void nnet::Network::saveToJson(std::ofstream& oFile) const {
  nlohmann::ordered_json netJson = toJson();
  oFile << netJson.dump(2) << '\n';
}

void nnet::Network::addLayer(DynMatrix&& params, Activation::Type funType) {
  expect(layers_.size() == 0 || outputDim() == params.cols() - 1,
         ErrorCode::UnmatchedLayerDims, __PRETTY_FUNCTION__);
  numOfParams_ += params.size();
  layers_.push_back(DenseLayer::create(std::move(params), funType));
}

void nnet::Network::addLayer(std::unique_ptr<DenseLayer>&& layer) {
  expect(layer != nullptr, ErrorCode::NullLayerPtr, __PRETTY_FUNCTION__);
  expect(layers_.size() == 0 || outputDim() == layer->InDim,
         ErrorCode::UnmatchedLayerDims, __PRETTY_FUNCTION__);
  numOfParams_ += layer->numOfParams();
  layers_.push_back(std::move(layer));
}

nnet::Network nnet::Network::fromJson(const nlohmann::json& netJson) {
  // using json = nlohmann::json;

  SizeType num = netJson.at("Metadata").at("NumOfLayers");
  Network net{num};

  for (const auto& layerJson : netJson.at("Data")) {
    SizeType numOfRows = layerJson.at("Metadata").at("OutputDim");
    SizeType numOfCols = layerJson.at("Metadata").at("InputDim");
    numOfCols++;  // numOfCols = InputDim + 1

    DynMatrix params{numOfRows, numOfCols};

    for (SizeType row = 0; row < numOfRows; row++) {
      for (SizeType col = 0; col < numOfCols; col++) {
        params(row, col) = layerJson.at("Data").at(row).at(col);
      }
    }

    net.addLayer(
        std::move(params),
        Activation::typeMap.at(layerJson.at("Metadata").at("Activation")));
    // net->addLayer(DenseLayer::fromJson(layerJson));
  }

  expect(net.numOfParams() == netJson.at("Metadata").at("NumOfParams"),
         ErrorCode::UnequalNumOfParams, __PRETTY_FUNCTION__);

  return net;
}

nnet::Network nnet::Network::loadFromJson(const std::string& filename) {
  nlohmann::json netJson;

  std::ifstream inFile{filename};
  if (inFile.good()) {
    inFile >> netJson;
    inFile.close();

    return fromJson(netJson);
  } else {
    return Network{};
  }
}

bool nnet::Network::isEquiv(const nnet::Network& other) const {
  if (numOfLayers() != other.numOfLayers()) return false;
  if (numOfParams_ != other.numOfParams_) return false;

  for (SizeType i = 0; i < numOfLayers(); i++) {
    // check for null pointers (..eh)
    if (!layers_.at(i) || !other.layers_.at(i)) return false;

    if (!layers_.at(i)->isEquiv(*other.layers_.at(i))) return false;
  }
  return true;
}
