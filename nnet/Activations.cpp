#include "include/nnet/Activations.h"

#include "Activations.h"

using nnet::Activation;

const std::unordered_map<std::string, Activation::Type> Activation::typeMap{
    {"Identity", Type::ID},
    {"ReLU", Type::RELU},
    {"Sigmoid", Type::SIGM},
    {"Tanh", Type::TANH},
    {"SoftMax", Type::SMAX}};

std::unique_ptr<Activation> Activation::create(Activation::Type funType) {
  switch (funType) {
    case Type::ID:
      return std::make_unique<Identity>();
    case Type::RELU:
      return std::make_unique<ReLU>();
    case Type::SIGM:
      return std::make_unique<Sigmoid>();
    case Type::TANH:
      return std::make_unique<Tanh>();
    case Type::SMAX:
      return std::make_unique<SoftMax>();
    default:
      expect(false, ErrorCode::UnknownActivation, __PRETTY_FUNCTION__);
      return nullptr;
  }
}

std::unique_ptr<Activation> nnet::Activation::create(const std::string& name) {
  Activation::Type funType = Activation::typeMap.at(name);
  return Activation::create(funType);
}

std::string Activation::typeDesc(Type typeCode) {
  switch (typeCode) {
    case Type::ID:
      return "Identity";
    case Type::RELU:
      return "ReLU";
    case Type::SIGM:
      return "Sigmoid";
    case Type::TANH:
      return "Tanh";
    case Type::SMAX:
      return "SoftMax";
    default:
      return "Unknown Activation Type";
  }
}