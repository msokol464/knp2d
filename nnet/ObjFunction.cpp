#include "include/nnet/ObjFunction.h"

template <class Solvable>
nnet::ObjFunction<Solvable>::ObjFunction(const nnet::ObjFunction<Solvable>& fun)
    : sample_{fun.sample_},
      avaliableIndicies_{fun.avaliableIndicies_.cbegin(),
                         fun.avaliableIndicies_.cend()},
      problems_{fun.problems_.cbegin(), fun.problems_.cend()} {}

template <class Solvable>
const bool nnet::ObjFunction<Solvable>::IsThreadSafe = true;

template <class Solvable>
const int nnet::ObjFunction<Solvable>::DefaultMaxEvals = 200;

template class nnet::ObjFunction<
    kp2d::SolvableSet<nnet::Network, kp2d::EvalType::UnusedContainerPortion>>;
template class nnet::ObjFunction<
    kp2d::SolvableSet<nnet::Network, kp2d::EvalType::UnusedStorageArea>>;
template class nnet::ObjFunction<
    kp2d::SolvableSet<nnet::Network, kp2d::EvalType::BlockedAndUnusedArea>>;
