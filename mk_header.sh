#!/bin/bash

__usage="
    Usage: $(basename $0) [OPTIONS]

    Options:
    -n --name           Name for the header file                     (required argument)
    -d --directory      Name of the parent directory for header file (opt by default)
    -e --extension      File extension                               (.h by default)
    -r --relative       Relative path from sourve to header          (include by defalult)
    -t --test-path      
"



# Default values
ext="h"
cpp="cpp"
dir="kp2d"



while (($#)); do 

    case $1 in
        -n|--name)
            if [ -n  $2 ] && [ ${2:0:1} != '-' ]; then
                name=$2
                shift 2
            fi
            ;;
        -d|--directory)
            if [ -n  $2 ] && [ ${2:0:1} != '-' ]; then
                dir=$2
                shift 2
            fi
            ;;
        -e|--extension)
            if [ -n  $2 ] && [ ${2:0:1} != '-' ]; then
                ext=$2
                shift 2
            fi
            ;;
        -r|--relative-path)
            if [ -n  $2 ] && [ ${2:0:1} != '-' ]; then
                relative=$2
                shift 2
            fi
            ;;
        -t|--test-path)
            if [ -t  $2 ] && [ ${2:0:1} != '-' ]; then
                test=$2
                shift 2
            fi
            ;;
        -*|--*)
            echo "$__usage"
            exit 1
            ;;
        *)
            echo "$__usage"
            exit 1
            ;;
    esac

done


relative="include/${dir}"
test="test/${dir}"
testinclude="../include"
test



if [ ${name:=x} != 'x' ]; then

    if [ ${dir:=x} != 'x' ]; then
        fullname="${dir}/"
        srcname="${dir}/"
    fi
    if [ ${relative:=x} != 'x' ]; then
        fullname+="${relative}/"
    fi
    fullname+="${name}.${ext:=h}"
    touch ${fullname}

cat <<EOT > ${fullname}
#ifndef ${name^^}_${ext^^}
#define ${name^^}_${ext^^}

namespace ${dir}
{

} // ${dir}

#endif // ${name^^}_${ext^^}
EOT

echo "Created header:  ${fullname}"
else
    echo "Filename not specified"
    echo "$__usage"
    exit 1
fi





srcname+=${name}.${cpp:=cpp}
touch ${srcname}

cat <<EOT > ${srcname}
#include "${relative:=some_path}/${name}.${ext}"



EOT

echo "Created source:  ${srcname}"







if [ ${test:=x} != 'x' ]; then

testname=test/${name}Test.${cpp:=cpp}
touch ${testname}

cat <<EOT > ${testname}
#include "${testinclude:=some_path}/catch.hpp"
#include "../../${dir}/include/${dir}/${name}.cpp"

TEST_CASE("", "[]")
{
    REQUIRE( false );
}

EOT

echo "Created test:    ${testname}" 
fi

