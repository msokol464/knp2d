#include "include/kp2d/State.h"

#include "State.h"

using namespace kp2d;

State::State(const Point& container, SizeType unitsPerSample)
    : unitsPerSample_{unitsPerSample},
      envelopeArea_{0},
      prevEnvelopeArea_{0},
      usedArea_{0},
      upperCPYMismatch_{0},
      lowerCPXMismatch_{0},
      containerSize_{container},
      cornerPoints_{{0, 0}} {
  size_t num = (container.y() % unitsPerSample == 0
                    ? container.y() / unitsPerSample
                    : container.y() / unitsPerSample + 1);
  xAxisDistances_ = std::vector<IntType>(num, container.x());
}

State::State(const State& prevState, Rectangle newRect)
    : unitsPerSample_{prevState.unitsPerSample()},
      envelopeArea_{0},
      prevEnvelopeArea_{prevState.envelopeArea()},
      usedArea_{prevState.usedArea() + newRect.area()},
      upperCPYMismatch_{0},
      lowerCPXMismatch_{0},
      containerSize_{prevState.container()} {
  if (prevState.numOfCornerPoints() > 0) {
    cornerPoints_.reserve(prevState.numOfCornerPoints() + 2);
    xAxisDistances_.reserve(prevState.numOfxAxisSamples());
    prepareCornerPoints(prevState.cornerPoints(), newRect);
    prepareXAxisDists();

  } else {
    throw std::runtime_error("Empty previous state in State Constructor\n");
  }
}

kp2d::State::State(const State& s)
    : unitsPerSample_{s.unitsPerSample()},
      envelopeArea_{s.envelopeArea_},
      prevEnvelopeArea_{s.prevEnvelopeArea_},
      usedArea_{s.usedArea_},
      upperCPYMismatch_{0},
      lowerCPXMismatch_{0},
      containerSize_{s.containerSize_},
      cornerPoints_{s.cornerPoints_.cbegin(), s.cornerPoints_.cend()},
      xAxisDistances_{s.xAxisDistances_.cbegin(), s.xAxisDistances_.cend()} {}

std::unique_ptr<State> kp2d::State::create(const State& prevState,
                                           Rectangle newRect) {
  // return std::unique_ptr<State>(new State(prevState, newRect));
  return std::make_unique<State>(prevState, newRect);
}

std::unique_ptr<State> kp2d::State::create(const Point& container,
                                           SizeType unitsPerSample) {
  return std::unique_ptr<State>(new State(container, unitsPerSample));
}

SizeType kp2d::State::NumOfFeatures(FeatOption opt) {
  switch (opt) {
    case FeatOption::allFeatures:
    case FeatOption::noXDistances:
      return 4;
    case FeatOption::areaMismatchHeuristic:
      return 0;
    case FeatOption::minimalFeatures:
      return 3;
    default:
      assert(false);
  }
}

void State::prepareCornerPoints(const std::vector<Point>& cpVec,
                                Rectangle newRect) {
  if (cpVec.size() < 2) {
    if (newRect.uppermostY() <= containerSize_.y())
      cornerPoints_.push_back({0, newRect.uppermostY()});
    if (newRect.rightmostX() <= containerSize_.x())
      cornerPoints_.push_back({newRect.rightmostX(), 0});
    return;
  }

  /// Iteracja główna:
  /// - od lewego górnego rogu  (0, containerSize_.y())
  /// - do prawego dolnego rogu (containerSize_.x(), 0)
  /// Pierwszy stop: y <= uppermostY
  auto cornerPoint = cpVec.cbegin();
  while (cornerPoint->y() > newRect.uppermostY()) {
    cornerPoints_.push_back(*cornerPoint);
    cornerPoint++;
  }

  if (cornerPoint->y() == newRect.uppermostY()) {
    // std::cout << "[State::prepareCornerPoints] Zero Upper Mismatch: ";

    cornerPoints_.push_back(*cornerPoint);
  } else if (cornerPoint->y() == newRect.anchorPoint().y() &&
             cornerPoint->x() != 0) {
    auto prevCP = cornerPoint;
    --prevCP;

    // std::cout << "[State::prepareCornerPoints] Upper Mismatch: ";
    upperCPYMismatch_ = prevCP->y() - newRect.uppermostY();

    cornerPoints_.push_back({cornerPoint->x(), newRect.uppermostY()});

  } else if (cornerPoint->y() == newRect.anchorPoint().y() &&
             cornerPoint->x() == 0) {
    // std::cout << "[State::prepareCornerPoints] Zero Upper Mismatch: ";
    cornerPoints_.push_back({0, newRect.uppermostY()});

  } else {
    // newRect.anchorPoint().y() < cornerPoint->y() < newRect.uppermostY()
    // std::cout << cornerPoint->y() << " - " << newRect.uppermostY() << '\n';
    // std::cout << "[State::prepareCornerPoints] Negative Upper Mismatch: ";
    upperCPYMismatch_ = cornerPoint->y() - newRect.uppermostY();
    cornerPoints_.push_back({cornerPoint->x(), newRect.uppermostY()});
  }

  // std::cout << upperCPYMismatch_ << '\n';

  /// Iteracja główna:
  /// pominięcie zdominowanych punktów
  while (cornerPoint->x() < newRect.rightmostX() &&
         cornerPoint != cpVec.cend()) {
    ++cornerPoint;
  }
  if (cornerPoint == cpVec.cend()) {
    cornerPoints_.push_back({newRect.rightmostX(), 0});

    if (newRect.anchorPoint().y() > 0) {
      lowerCPXMismatch_ = cpVec.back().x() - newRect.rightmostX();

      // std::cout << cpVec.back().x() << " - " << newRect.rightmostX() << '\n';
      // std::cout << "[State::prepareCornerPoints] Negative Lower Mismatch: "
      // << lowerCPXMismatch_ << '\n';
    }

    return;
  }

  /// Iteracja główna:
  /// Drugi stop: x => rightmostX
  if (cornerPoint->x() > newRect.rightmostX()) {
    IntType nextCPX = cornerPoint->x();

    --cornerPoint;
    // anchorPointX <= x < rightmostX

    // LOWER CORNER POINT
    cornerPoints_.push_back({newRect.rightmostX(), cornerPoint->y()});

    if (cornerPoint->y() == newRect.anchorPoint().y()) {
      // std::cout << nextCPX << " - " << newRect.rightmostX() << '\n';
      // std::cout << "[State::prepareCornerPoints] Positive Lower Mismatch: ";
      lowerCPXMismatch_ = nextCPX - newRect.rightmostX();
    } else {
      // std::cout << cornerPoint->x() << " - " << newRect.rightmostX() << '\n';
      // std::cout << "[State::prepareCornerPoints] Negative Lower Mismatch: ";
      lowerCPXMismatch_ = cornerPoint->x() - newRect.rightmostX();
    }
    ++cornerPoint;

    // std::cout << lowerCPXMismatch_ << '\n';
  }
  while (cornerPoint != cpVec.cend()) {
    cornerPoints_.push_back(*cornerPoint);
    cornerPoint++;
  }
}

void State::prepareXAxisDists() {
  IntType last = containerSize_.y();
  envelopeArea_ = 0;

  IntType ySpot =
      (containerSize_.y() % unitsPerSample_ == 0
           ? containerSize_.y() - unitsPerSample_
           : containerSize_.y() - containerSize_.y() % unitsPerSample_);

  for (const auto& point : cornerPoints_) {
    while (ySpot >= point.y()) {
      xAxisDistances_.push_back(containerSize_.x() - point.x());
      ySpot -= unitsPerSample_;
    }
    envelopeArea_ += (last - point.y()) * point.x();
    last = point.y();
  }
  // std::cout << "[State::prepareXAxisDists] Envelope area: " <<
  // envelopeArea_
  //           << '\n';
  // std::cout << "[State::prepareXAxisDists] Number of XSamples: "
  //           << xAxisDistances_.size() << '\n';
}

std::string State::cornerPointsToString() const {
  std::stringstream oss;
  for (const auto& point : cornerPoints_) {
    oss << point << '\n';
  }
  return oss.str();
}

std::string State::xAxisDistToString() const {
  std::stringstream oss;
  IntType lastDist = containerSize_.x(), count = 0;
  for (const auto& dist : xAxisDistances_) {
    if (dist == lastDist) {
      count++;
    } else if (dist < lastDist) {
      if (count > 0) oss << lastDist << "[x " << count << "]\n";
      count = 1;
    }

    lastDist = dist;
  }
  oss << lastDist << "[x " << count << "]\n";
  return oss.str();
}

FloatType State::usedAreaPortion() const {
  // std::cout << "[State::usedAreaPortion] " << usedArea_ << "/"
  //           << containerSize_.product() << '\n';
  return static_cast<FloatType>(usedArea_) /
         static_cast<FloatType>(containerSize_.product());
}

FloatType State::envelopeAreaPortion() const {
  return static_cast<FloatType>(envelopeArea_) /
         static_cast<FloatType>(containerSize_.product());
}

FloatType State::envelopeAreaIncrease() const {
  return static_cast<FloatType>(envelopeArea_ - prevEnvelopeArea_) /
         static_cast<FloatType>(containerSize_.product());
}

FloatType kp2d::State::blockedAreaPortion() const {
  return static_cast<FloatType>(envelopeArea_ - usedArea_) /
         static_cast<FloatType>(containerSize_.product());
}

void State::appendStateFeatures(std::vector<FloatType>& fVec) const {
  /// NOTE: If number of features changes
  /// Value of NumOfFeatures will need adjustment
  /// Features from xAxisDistances are counted separately
  fVec.push_back(usedAreaPortion());
  fVec.push_back(envelopeAreaPortion());
  fVec.push_back(envelopeAreaIncrease());
  fVec.push_back(mismatch());
  //
}

void State::appendXDistFeatures(std::vector<FloatType>& fVec) const {
  for (const IntType& dist : xAxisDistances_) {
    fVec.push_back(static_cast<FloatType>(dist) /
                   static_cast<FloatType>(containerSize_.x()));
  }
}

void kp2d::State::appendMinimalFeatures(std::vector<FloatType>& fVec) const {
  fVec.push_back(usedAreaPortion());
  fVec.push_back(envelopeAreaIncrease());
  fVec.push_back(mismatch());
}

void State::validate() const {
  /// TODO: chceck if expect can return bool
  // print function name if error occured, as last line if possible
  // bool valid = true;

  expect(cornerPoints_.size() > 0, ErrorCode::FaultyStateNoPoints);
  expect(xAxisDistances_.size() == containerSize_.y() * unitsPerSample_,
         ErrorCode::FaultyXAxisSamplesNum);
  expect(containerSize_.product() > 0, ErrorCode::FaultyStateContainer);

  if (cornerPoints_.size() == 1) {
    // Initial state
    expect(cornerPoints_.front() == Point{0, 0}, ErrorCode::FaultyStateFirstCP);
    expect(envelopeArea_ == 0 && usedArea_ == 0,
           ErrorCode::FaultyStateInitArea);
  } else {
    // Non-initial state
    expect(envelopeArea_ <= containerSize_.product(),
           ErrorCode::FaultyStateEnvArea);
    expect(usedArea_ <= envelopeArea_, ErrorCode::FaultyStateUsedArea);
    expect(cornerPoints_.front().x() == 0, ErrorCode::FaultyStateFirstCP);
    expect(cornerPoints_.back().y() == 0, ErrorCode::FaultyStateLastCP);

    auto first = cornerPoints_.begin();
    auto second = cornerPoints_.begin();
    IntType areaCheck = 0;
    for (first++; first != cornerPoints_.end(); first++, second++) {
      expect(first->x() > second->x() && first->y() < second->y(),
             ErrorCode::FaultyXAxisSample);
      areaCheck += (first->x() - second->x()) * second->y();
    }
    expect(envelopeArea_ == areaCheck, ErrorCode::FaultyStateUsedArea);
  }
}

/// NOTE: keeping both validate and isValid
///  is probably unnecessary
bool State::isValid() const {
  std::cout << "[State::isValid] Num of samples:" << containerSize_.y() << "/"
            << unitsPerSample_ << "->" << xAxisDistances_.size() << '\n';

  size_t numOfSamplesCheck =
      (containerSize_.y() % unitsPerSample_ == 0
           ? static_cast<size_t>(containerSize_.y() / unitsPerSample_)
           : static_cast<size_t>(containerSize_.y() / unitsPerSample_) + 1);

  for (const auto& dist : xAxisDistances_) {
    std::cout << dist << '\t';
  }
  std::cout << '\n';

  if (xAxisDistances_.size() != numOfSamplesCheck) return false;
  if (containerSize_.product() <= 0) return false;
  if (cornerPoints_.size() <= 0) return false;
  if (cornerPoints_.size() == 1 && (cornerPoints_.front() != Point{0, 0} ||
                                    envelopeArea_ != 0 || usedArea_ != 0))
    return false;
  if (envelopeArea_ > containerSize_.product()) return false;
  if (usedArea_ > envelopeArea_) return false;
  if (cornerPoints_.front().x() != 0) return false;
  if (cornerPoints_.back().y() != 0) return false;

  auto first = cornerPoints_.begin();
  auto second = cornerPoints_.begin();
  IntType areaCheck = 0;
  for (first++; first != cornerPoints_.end(); first++, second++) {
    if (first->x() <= second->x() || first->y() >= second->y()) return false;
    areaCheck += (first->x() - second->x()) * second->y();
  }
  if (envelopeArea_ != areaCheck) return false;
  return true;
}