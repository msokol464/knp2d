#include "include/kp2d/Config.h"

kp2d::RndEngineDefault kp2d::RndEngine::engine_{param::defaultSeed};
rbp::MaxRectsBinPack::FreeRectChoiceHeuristic kp2d::MaxRectHeur::value_{
    rbp::MaxRectsBinPack::FreeRectChoiceHeuristic::RectBestAreaFit};
