#pragma once
#include <cassert>

#include "../../../nnet/include/nnet/Network.h"
#include "Config.h"
#include "Generators.h"
#include "Problem.h"
#include "ProblemSet.h"
#include "RectangleGroup.h"

namespace kp2d {
/// @brief Problems generated have only rectangles which side lengths divide
/// container W, H evenly. Wether that constitutes problem's relaxation remains
/// ALTERNATIVE NAME: ApproxGuilotineProblemGeneratior
class CutProblemGenerator {
 public:
  enum class AreaOption { AreaOverflow, AreaUnderflow };
  // enum class NumOption { AllowEmpty, DisallowEmpty };

  CutProblemGenerator(
      Point container, IntType binsX, IntType binsY,
      std::unique_ptr<IntDistrib>&& nDistrib, IntType numOfGroups = -1,
      FeatOption opt = FeatOption::allFeatures,
      SizeType unitsPerSample = param::DefaultUnitsPerSample);

  ~CutProblemGenerator() = default;

  Problem generate(AreaOption aOpt = AreaOption::AreaUnderflow);

  template <class Solver, EvalType Eval = defaultEvalType>
  ProblemSet<Solver, Eval> generateSet(
      SizeType numOfProblems, AreaOption aOpt = AreaOption::AreaUnderflow);

  void setNumOfGroups(const IntType& val) { numOfGroups_ = val; }
  void setFeatureOption(FeatOption val) { featureOption_ = val; }

  IntType numOfGroups() const { return numOfGroups_; }

  IntType numOfFeatures() const {
    if (numOfGroups_ <= 0) {
      return -1;
    }
    return Storage::FeaturesPerGroup(featureOption_) * numOfGroups_ +
           State::NumOfFeatures(featureOption_) +
           (container_.y() % unitsPerSample_ == 0
                ? container_.y() / unitsPerSample_
                : container_.y() / unitsPerSample_ + 1);
  }

 private:
  Point container_;
  IntType binsX_;
  IntType binsY_;
  std::unique_ptr<IntDistrib> distrib_;

  IntType numOfGroups_;
  FeatOption featureOption_;
  SizeType unitsPerSample_;

  void generateGroups(std::vector<RectangleGroup>& rectGroups, AreaOption aOpt);
  void setNumAndGenerateGroups(std::vector<RectangleGroup>& rectGroups,
                               AreaOption aOpt);

  /// @brief Function randomly assigns divisors to divX and divY
  /// @param divX container width divisor, ranging from 1 to binsX_
  /// @param divY container height divisor, ranging from 1 to binsY_
  /// @param flattenedIndicies vector of numbers ranging from `min` (0) to
  /// `max` (binsY_*(binsX_-1)) that represents combined indicies of
  /// pairs (i,j) where i in {0, binsX_-1} and j in {0, binsY_-1}
  /// @param mt random number engine
  /// @param max last currently usable value in flattenedIndicies
  template <class RndEngine>
  void setDivisors(IntType& divX, IntType& divY,
                   std::vector<IntType>& flattenedIndicies, RndEngine& mt,
                   const IntType& max, const IntType& min = 0);

  bool addOrDiscardGroup(const AreaOption& aOpt, IntType& groupsSumArea,
                         RectangleGroup& newGroup,
                         std::vector<RectangleGroup>& currentGroups);
};

template <class Solver, EvalType Eval>
inline ProblemSet<Solver, Eval> CutProblemGenerator::generateSet(
    SizeType numOfProblems, AreaOption aOpt) {
  std::vector<Problem> problems;
  problems.reserve(numOfProblems);

  while (problems.size() < numOfProblems) {
    problems.push_back(generate(aOpt));
  }

  return ProblemSet<Solver, Eval>(std::move(problems));
}

template <class RndEngine>
inline void CutProblemGenerator::setDivisors(
    IntType& divX, IntType& divY, std::vector<IntType>& flattenedIndicies,
    RndEngine& mt, const IntType& max, const IntType& min) {
  std::uniform_int_distribution<IntType> U{min, max};
  IntType rnd = U(mt);
  IntType idxXY = flattenedIndicies[rnd];
  flattenedIndicies[rnd] = flattenedIndicies.back();

  divX = idxXY % binsX_;
  divY = (idxXY - divX) / binsX_ + 1;

  ++divX;
  ++divY;
}

}  // namespace kp2d