#pragma once
#include <iostream>
#include <memory>
#include <random>

#include "Config.h"
#include "Logger.h"

namespace kp2d {

template <class T>
class Distribution {
 public:
  virtual std::unique_ptr<Distribution> clone() const = 0;
  virtual T draw() = 0;
  virtual bool operator==(const Distribution<T>* p) const = 0;
};

using IntDistrib = Distribution<IntType>;
using FloatDistrib = Distribution<FloatType>;

class UniformInt : public IntDistrib {
 public:
  UniformInt(IntType min, IntType max) : distribution_{min, max} {}

  static std::unique_ptr<UniformInt> create(IntType min, IntType max) {
    return std::unique_ptr<UniformInt>{new UniformInt(min, max)};
  }

  std::unique_ptr<IntDistrib> clone() const override {
    return std::unique_ptr<IntDistrib>{new UniformInt{*this}};
  }

  IntType draw() override { return distribution_(RndEngine::get()); }

  bool operator==(const IntDistrib* p) const override {
    if (p == nullptr) return false;

    std::cout << "*Rhs : " << typeid(*p).name() << '\n';
    std::cout << "*Lhs : " << typeid(*this).name() << '\n';

    if (typeid(*this) != typeid(*p)) return false;
    return distribution_ == dynamic_cast<const UniformInt*>(p)->distribution_;
  }

 private:
  std::uniform_int_distribution<IntType> distribution_;
};

class Binomial : public IntDistrib {
 public:
  Binomial(IntType t, double p) : distribution_{t, p} {}

  static std::unique_ptr<Binomial> create(IntType t, double p) {
    return std::unique_ptr<Binomial>{new Binomial(t, p)};
  }

  std::unique_ptr<IntDistrib> clone() const override {
    return std::unique_ptr<Binomial>{new Binomial(*this)};
  }

  IntType draw() override { return distribution_(RndEngine::get()); }

  bool operator==(const IntDistrib* p) const override {
    if (p == nullptr) return false;

    std::cout << "*Rhs : " << typeid(*p).name() << '\n';
    std::cout << "*Lhs : " << typeid(*this).name() << '\n';

    if (typeid(*this) != typeid(p)) return false;
    return distribution_ == dynamic_cast<const Binomial*>(p)->distribution_;
  }

 private:
  std::binomial_distribution<IntType> distribution_;
};

template <class T>
class Normal : public Distribution<T> {
 public:
  Normal(FloatType mu, FloatType sigm)
      : Distribution<T>{}, distribution_{mu, sigm} {}

  static std::unique_ptr<Normal> create(FloatType mu, FloatType sigm) {
    return std::unique_ptr<Normal>{new Normal(mu, sigm)};
  }

  std::unique_ptr<Distribution<T>> clone() const override {
    return std::unique_ptr<Normal>{new Normal(*this)};
  }

  T draw() override { return static_cast<T>(distribution_(RndEngine::get())); }

  bool operator==(const Distribution<T>* p) const override {
    if (p == nullptr) return false;

    std::cout << "*Rhs : " << typeid(*p).name() << '\n';
    std::cout << "*Lhs : " << typeid(*this).name() << '\n';

    if (typeid(*this) != typeid(*p)) return false;
    return distribution_ == dynamic_cast<const Normal*>(p)->distribution_;
  }

 private:
  std::normal_distribution<FloatType> distribution_;
};

class PositiveNormalInt : public IntDistrib {
 public:
  PositiveNormalInt(FloatType mu, FloatType sigm) : distribution_{mu, sigm} {}

  static std::unique_ptr<PositiveNormalInt> create(FloatType mu,
                                                   FloatType sigm) {
    return std::unique_ptr<PositiveNormalInt>{new PositiveNormalInt(mu, sigm)};
  }

  std::unique_ptr<IntDistrib> clone() const override {
    return std::unique_ptr<PositiveNormalInt>{new PositiveNormalInt(*this)};
  }

  IntType draw() override {
    IntType num = -1, trials = 0;
    while (num <= 0 && trials++ < param::MaxRandTrials) {
      num = static_cast<IntType>(distribution_(RndEngine::get()));
    }
    expect(trials <= param::MaxRandTrials, ErrorCode::ExceededMaxTrials,
           __PRETTY_FUNCTION__);
    return num;
  }

  bool operator==(const IntDistrib* p) const override {
    if (p == nullptr) return false;

    std::cout << "*Rhs : " << typeid(*p).name() << '\n';
    std::cout << "*Lhs : " << typeid(*this).name() << '\n';

    if (typeid(*this) != typeid(*p)) return false;
    return distribution_ ==
           dynamic_cast<const PositiveNormalInt*>(p)->distribution_;
  }

 private:
  std::normal_distribution<FloatType> distribution_;
};

using NormalInt = PositiveNormalInt;

using NormalFloat = Normal<FloatType>;

}  // namespace kp2d