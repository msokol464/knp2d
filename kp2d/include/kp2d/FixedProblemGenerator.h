#pragma once
#include "../../../nnet/include/nnet/Network.h"
#include "Generators.h"
#include "Problem.h"
#include "ProblemSet.h"

namespace kp2d {

class FixedProblemGenerator {
 public:
  FixedProblemGenerator(const Point& c, const std::vector<Point>& sizes,
                        std::unique_ptr<IntDistrib>&& numdistr,
                        std::unique_ptr<IntDistrib>&& noise =
                            std::unique_ptr<IntDistrib>(nullptr),
                        bool areaConstraint = true,
                        FeatOption opt = FeatOption::allFeatures,
                        SizeType ups = param::DefaultUnitsPerSample);

  Problem generate();
  Problem generateNoisy();
  std::vector<Problem> generateProblems(size_t numProblems);

  void addNoise(std::unique_ptr<IntDistrib>&& noise) {
    noiseDistr_.swap(noise);
  }
  void removeNoise() { noiseDistr_.reset(nullptr); }

  void disableAreaOverflow() { areaConstraint_ = true; }
  void enableAreaOverflow() { areaConstraint_ = false; }

  template <class Solver, EvalType Eval>
  ProblemSet<Solver, Eval> generateSet(size_t numProblems) {
    return ProblemSet<Solver, Eval>{generateProblems(numProblems)};
  }

  // template <class Solver, EvalType Eval>
  // ProblemSet<Solver, Eval> generateNoisySet(size_t numProblems) {
  //   std::vector<Problem> problems;
  //   problems.reserve(numProblems);
  //   for (size_t i = 0; i < numProblems; i++) {
  //     problems.push_back(generateNoisy());
  //   }
  //   return ProblemSet<Solver, Eval>{std::move(problems)};
  // }

 private:
  Point container_;
  std::vector<Point> elemSizes_;

  std::unique_ptr<IntDistrib> numDistr_;
  std::unique_ptr<IntDistrib> noiseDistr_;

  bool areaConstraint_;

  FeatOption featureOption_;
  SizeType unitsPerSample_;
};

}  // namespace kp2d
