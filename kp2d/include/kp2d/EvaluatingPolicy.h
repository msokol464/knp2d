#pragma once
#include <vector>

#include "Problem.h"

namespace kp2d {

enum class EvalType {
  UnusedContainerPortion,
  UnusedStorageArea,
  BlockedAndUnusedArea
};

constexpr EvalType defaultEvalType = EvalType::UnusedStorageArea;

template <EvalType et>
struct EvaluatingPolicy {
  static FloatType eval(const Problem& p);
  static std::string getName();
};

template <>
struct EvaluatingPolicy<EvalType::UnusedContainerPortion> {
  static FloatType eval(const Problem& p) { return p.unusedContainerPortion(); }
  static std::string getName() { return "UnusedContainerPortion"; }
};

template <>
struct EvaluatingPolicy<EvalType::UnusedStorageArea> {
  static FloatType eval(const Problem& p) { return p.unusedStorageArea(); }
  static std::string getName() { return "UnusedStorageArea"; }
};

template <>
struct EvaluatingPolicy<EvalType::BlockedAndUnusedArea> {
  static FloatType eval(const Problem& p) {
    return p.unusedStorageArea() + 4.0 * p.blockedAreaPortion();
  }
  static std::string getName() { return "BlockedAndUnusedArea"; }
};

}  // namespace kp2d
