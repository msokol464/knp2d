#pragma once

#include <eigen3/Eigen/Dense>
#include <random>

#include "../RectangleBinPack/MaxRectsBinPack.h"

namespace kp2d {
using RndEngineDefault = std::mt19937;
using IntType = int;
using FloatType = double;
using SizeType = size_t;
using DynMatrix = Eigen::Matrix<FloatType, Eigen::Dynamic, Eigen::Dynamic>;

// Config Console Output
namespace cco {
const SizeType ColumnWidth = 4;
const SizeType PointsPerLine = 5;
}  // namespace cco

//
namespace param {
const SizeType DefaultUnitsPerSample = 8;
const IntType MaxRandTrials = 1000000;
const SizeType MaxGroups = 100;
const long unsigned int defaultSeed = 10020234u;
}  // namespace param

enum class FeatOption {
  allFeatures = 0,
  noXDistances,
  areaMismatchHeuristic,
  minimalFeatures,
  minXDistances
};

class RndEngine {
 public:
  static RndEngineDefault& get() { return engine_; }

  static void setSeed(long unsigned int val) {
    engine_ = RndEngineDefault{val};
  }

 private:
  static RndEngineDefault engine_;

  RndEngine() = delete;
  RndEngine(const RndEngine&) = delete;
  RndEngine& operator=(const RndEngine&) = delete;
};

class MaxRectHeur {
 public:
  static rbp::MaxRectsBinPack::FreeRectChoiceHeuristic& get() { return value_; }

  static void set(rbp::MaxRectsBinPack::FreeRectChoiceHeuristic val) {
    value_ = val;
  }

 private:
  static rbp::MaxRectsBinPack::FreeRectChoiceHeuristic value_;

  MaxRectHeur() = delete;
  MaxRectHeur(const MaxRectHeur&) = delete;
  MaxRectHeur& operator=(const MaxRectHeur&) = delete;
};
}  // namespace kp2d
