#pragma once
#include <fstream>
#include <iostream>

namespace kp2d {

enum class ErrorAction { Throwing, Logging, Ignoring, Terminating, Printing };
enum class ErrorCode {
  LogFileFailure = 0,
  OutOfRange,
  UnmatchedStates,
  IllFormedRectangle,
  ExceededMaxTrials,
  FaultyStateNoPoints,
  FaultyXAxisSamplesNum,
  FaultyStateContainer,
  FaultyStateInitArea,
  FaultyStateEnvArea,
  FaultyStateUsedArea,
  FaultyStateFirstCP,
  FaultyStateLastCP,
  FaultyXAxisSample,  // <<
  FaultyStorageSumAreaErr,
  StorageExceededContainerArea,
  GroupExceededContainerArea,
  FaultyGroup,
  EmptyProblemVec,
  ContainerMismatch
};

constexpr ErrorAction defaultErrorAction = ErrorAction::Printing;

const std::vector<std::string> ErrorName{
    "Can't write to log file",
    "Idx out of range",
    "Number of states and proposed rectangles do not match",
    "Created faulty rectangle",
    "Exceeded maximum number of trials when using random number generator. "
    "Consider adjusting generator distribution parameters or problem container "
    "size.",
    "Problem state has no corner points",
    "Problem state has empty x-axis distances vector",
    "State container area is <= 0",
    "Initial state has non-zero area usage",
    "Envelope area greater than container area",
    "Used area greater then envelope area",
    "First corner point different than {0, y}",
    "Last corner point different than {x, 0}",
    "Faulty x axis sample in state",
    "Storage sumArea_ doesn't reflect summed area of rectangle groups",
    "Summed area of rectangles in storage exceeds container area",
    "Group area exceeds container area",
    "Ill-formed rectangle group",
    "Empty problem vector",
    "Mismatched problem containers"};

template <ErrorAction action = defaultErrorAction>
constexpr void expect(bool condition, ErrorCode code,
                      const std::string& functionName = "Function unknown") {
  if constexpr (action == ErrorAction::Printing) {
    if (!condition) {
      std::cerr << "Error: " << ErrorName[static_cast<size_t>(code)] << '\n'
                << "     |-> " << functionName << '\n';
      // return false;
    }
  } else if constexpr (action == ErrorAction::Throwing) {
    if (!condition) throw ErrorName[static_cast<size_t>(code)];
  } else if constexpr (action == ErrorAction::Terminating) {
    if (!condition) std::terminate();
  }
  // else if constexpr (action == ErrorAction::Logging) {
  //   if (!condition) {

  //   }
  // }
  // ErrorAction::Ignoring
}

template <ErrorAction action = defaultErrorAction>
constexpr bool expectThat(
    bool condition, ErrorCode code,
    const std::string& functionName = "Function unknown") {
  if constexpr (action == ErrorAction::Printing) {
    if (!condition) {
      std::cerr << "Error: " << ErrorName[static_cast<size_t>(code)] << '\n'
                << "     |-> " << functionName << '\n';
      return false;
    }
  } else if constexpr (action == ErrorAction::Throwing) {
    if (!condition) throw ErrorName[static_cast<size_t>(code)];
  } else if constexpr (action == ErrorAction::Terminating) {
    if (!condition) std::terminate();
  }
  // else if constexpr (action == ErrorAction::Logging) {
  //   if (!condition) {

  //   }
  // }
  if (!condition)  // ErrorAction::Ignoring
    return false;
  return true;
}

class Logger {
 public:
  Logger(const std::string& filename,
         std::ios_base::openmode mode = std::ios::out) {
    logStream.open(filename, mode);
    expect<ErrorAction::Printing>(logStream.good(), ErrorCode::LogFileFailure,
                                  __PRETTY_FUNCTION__);
  };                                 // open/create file
  ~Logger() { logStream.close(); };  // close file

  template <class Object>
  Logger& operator<<(const Object& obj) {
    logStream << obj;
    return (*this);
  }

 private:
  std::ofstream logStream;
};

}  // namespace kp2d