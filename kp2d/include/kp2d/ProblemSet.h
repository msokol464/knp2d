#pragma once
#include <functional>
#include <iostream>
#include <vector>

#include "../../../nnet/include/nnet/Network.h"
#include "EvaluatingPolicy.h"
#include "Problem.h"
#include "SolvingPolicy.h"

namespace kp2d {

enum class PrintingOpt { All, Agregate, None };

template <class Solver, EvalType Eval = defaultEvalType,
          class EvaluatingMethod = EvaluatingPolicy<Eval>,
          class SolvingMethod = SolvingPolicy<Solver, Eval>>
class ProblemSet {
 public:
  ProblemSet(std::vector<Problem>&& problems)
      : problems_{std::move(problems)}, avgEval_{0.} {
    init();
  }

  ProblemSet(const std::vector<Problem>& pvec)
      : problems_{pvec.cbegin(), pvec.cend()}, avgEval_{0.} {
    init();
  }

  void init() {
    evaluations_.reserve(problems_.size());
    for (size_t i = 0; i < problems_.size(); i++) {
      FloatType eval = EvaluatingMethod::eval(problems_[i]);
      evaluations_.push_back(eval);
      avgEval_ += eval;
    }
    avgEval_ /= problems_.size();
  }

  ProblemSet() = default;
  ProblemSet(ProblemSet&&) = default;
  ProblemSet& operator=(ProblemSet&&) = default;
  ProblemSet& operator=(const ProblemSet&) = default;

  ProblemSet(const ProblemSet& ps)
      : problems_{ps.problems_.cbegin(), ps.problems_.cend()},
        evaluations_{ps.evaluations_.cbegin(), ps.evaluations_.cend()},
        avgEval_{ps.avgEval_} {
    // std::cout << "[ProblemSet::cpy ctor] Problem vec size: " <<
    // problems_.size()
    //           << '\n'
    //           << "[ProblemSet::cpy ctor] Evaluation vec size: "
    //           << evaluations_.size() << '\n'
    //           << "[ProblemSet::cpy ctor] Avg eval: " << avgEval_ << '\n'
    //           << "[ProblemSet::cpy ctor] Avg eval: " << avgEval_ << '\n'
    //           << ".\n";
  }

  // void addSolved(const Problem& p, FloatType eval) {
  //   int num = problems_.size();

  // }

  template <class SortingRule>
  void sort(SortingRule rule) {
    for (Problem& p : problems_) {
      p.sort<SortingRule>(rule);
    }
  }

  ~ProblemSet() {}

  FloatType getAvgEval() const { return avgEval_; }
  IntType numOfFeatures() const {
    return (problems_.size() <= 0 ? 0 : problems_[0].numOfFeatures());
  }

  bool isUnsolved() const;
  bool isValid() const;

  nlohmann::ordered_json toJson() const;
  nlohmann::ordered_json solutionsToJson() const;

  /// @brief Problem sets are equivalent iff they have the same problems
  /// possibly in different stages of solution. Rotation and sorting storage
  /// shouldn't break equivalence.
  bool isEquiv(const ProblemSet& ps) const;

  FloatType solveWith(Solver& solver) {
    int i = 0;
    avgEval_ = 0.;
    for (auto& problem : this->problems_) {
      FloatType eval = SolvingMethod::solve(problem, solver);
      evaluations_[i] = eval;
      avgEval_ += eval;
      printf("[Problem %d] %f\n", ++i, eval);
    }
    expect(problems_.size() > 0, ErrorCode::EmptyProblemVec,
           __PRETTY_FUNCTION__);
    avgEval_ /= problems_.size();
    printf("Average eval %f\n", avgEval_);
    return avgEval_;
  }

  void logBestSolution(const std::string& filename) const {
    Logger log{filename};
    log << getBestCase();
    /// TODO: move logging function to Problem class
    // selectProblem().logToFile(filename);
  }

  const Problem& getBestCase() const { return getProblem(std::less<>{}); }
  const Problem& getWorstCase() const { return getProblem(std::greater<>{}); }

  std::vector<Problem>::iterator begin() { return problems_.begin(); }
  std::vector<Problem>::iterator end() { return problems_.end(); }

  friend std::ostream& operator<<(std::ostream& os, const ProblemSet& set) {
    int i = 0;
    for (const auto& problem : set.problems_) {
      os << "#" << i++ << '\n' << problem << '\n';
    }
    return os;
  }

  void evalsToStream(std::ostream& os) const {
    for (const FloatType& ev : evaluations_) {
      os << ev << '\t';
    }
  }

 private:
  std::vector<Problem> problems_;
  std::vector<FloatType> evaluations_;
  FloatType avgEval_;

  /// TODO: repair function
  template <class Criterion>
  const Problem& getProblem(Criterion compare) const {
    SizeType selectedIdx = 0;
    FloatType minEval = EvaluatingMethod::eval(problems_.at(0));
    for (SizeType i = 0; i < problems_.size(); i++) {
      FloatType currentEval = EvaluatingMethod::eval(problems_.at(i));
      if (compare(currentEval, minEval)) {
        minEval = currentEval;
        selectedIdx = i;
      }
    }
    return problems_.at(selectedIdx);
  }
};

}  // namespace kp2d