#pragma once
#include <cassert>
#include <eigen3/Eigen/Dense>

#include "ProblemSet.h"

namespace kp2d {

template <class Solver, EvalType Eval = defaultEvalType>
class SolvableSet {
 public:
  SolvableSet(ProblemSet<Solver, Eval>&& set, const Solver& sol)
      : set_{std::move(set)}, solver_{sol} {}

  SolvableSet(const ProblemSet<Solver, Eval>& set, const Solver& sol)
      : set_{set}, solver_{sol} {}

  SolvableSet(const SolvableSet& other)
      : set_{other.set_}, solver_{other.solver_} {
    // assert(false && "SolvableSet cpy ctor");
    // std::cout << "SolvableSet cpy ctor\n";
  }

  SolvableSet(SolvableSet&& other)
      : set_{std::move(other.set_)}, solver_{std::move(other.solver_)} {
    // assert(false && "SolvableSet move ctor");
    // std::cout << "SolvableSet move ctor\n";
  }

  SolvableSet() = default;
  ~SolvableSet() = default;

  SolvableSet& operator=(SolvableSet&& rhs) = default;
  SolvableSet& operator=(const SolvableSet& rhs) = default;
  FloatType operator()(const double* x, const int dim);
  FloatType solve() { return set_.solveWith(solver_); }

  template <class SortingRule>
  void sort(SortingRule rule) {
    set_.template sort<SortingRule>(rule);
  }

  nlohmann::ordered_json toJson() const;
  void saveToJson(std::ofstream& oFile) const {
    oFile << toJson().dump(2) << '\n';
  }

  void saveSolverToJson(std::ofstream& oFile) const {
    oFile << solver_.toJson().dump(2) << '\n';
  }

  std::vector<FloatType> params() const { return solver_.getParamVec(); }

  bool isEquiv(const SolvableSet& other) const;
  bool isValid() const;

  FloatType aggregateEval() const { return set_.getAvgEval(); }
  int dim() const { return solver_.numOfParams(); }

  void getBounds(std::vector<FloatType>& lBounds,
                 std::vector<FloatType>& uBounds) const {
    solver_.getParamBounds(lBounds, uBounds);
  }

  bool isUnsolved() const {
    bool res = set_.isUnsolved();
    // std::cout << "[SolvableSet::isUnsolved] result: " << res << '\n';
    return res;
  }

  const ProblemSet<Solver, Eval>& problemSet() const { return set_; }
  void replaceSet(const ProblemSet<Solver, Eval>& set) {
    set_ = set;
    // std::cout << set_ << '\n' << '\n';
  }
  void replaceParams(Eigen::Matrix<FloatType, Eigen::Dynamic, 1>&& params) {
    solver_.setParams(params);
  }

  IntType numOfFeatures() const { return set_.numOfFeatures(); }

  void evalsToStream(std::ostream& os) const { set_.evalsToStream(os); }

  Solver getSolver() const { return solver_; }

 private:
  ProblemSet<Solver, Eval> set_;
  Solver solver_;
};

template <class Solver, EvalType Eval>
inline FloatType SolvableSet<Solver, Eval>::operator()(const double* x,
                                                       const int dim) {
  solver_.setParamsFromVector(std::vector<FloatType>(x, x + dim));
  return set_.solveWith(solver_);
}

template <class Solver, EvalType Eval>
inline nlohmann::ordered_json SolvableSet<Solver, Eval>::toJson() const {
  return set_.toJson();
}

template <class Solver, EvalType Eval>
inline bool SolvableSet<Solver, Eval>::isEquiv(const SolvableSet& other) const {
  if (!set_.isEquiv(other.set_)) return false;
  if (!solver_.isEquiv(other.solver_)) return false;
  return true;
}

template <class Solver, EvalType Eval>
inline bool SolvableSet<Solver, Eval>::isValid() const {
  if (!set_.isValid()) return false;
  if (!solver_.isValid()) return false;
  return true;
}

// std::vector<Problem> problems_;
// std::vector<FloatType> evaluations_;
// FloatType avgEval_;
}  // namespace kp2d