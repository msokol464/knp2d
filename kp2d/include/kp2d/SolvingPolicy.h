#pragma once
#include <iostream>
#include <memory>
#include <random>
#include <vector>

#include "../RectangleBinPack/GuillotineBinPack.h"
#include "../RectangleBinPack/MaxRectsBinPack.h"
#include "Config.h"
#include "EvaluatingPolicy.h"
#include "Logger.h"
#include "Problem.h"

namespace kp2d {

/// @brief Template parameter class has to implement forward
struct RandomPlacementSolver {
  DynMatrix forward(const DynMatrix& mat) const {
    std::unique_ptr<NormalFloat> norm = NormalFloat::create(0.0, 1.0);

    return DynMatrix::NullaryExpr(1, mat.cols(),
                                  [&norm]() { return norm->draw(); });
  }
};

template <class Solver, EvalType Eval = defaultEvalType,
          class EvaluatingMethod = EvaluatingPolicy<Eval>>
struct SolvingPolicy {
  static FloatType solve(Problem& problem, Solver& solver) {
    DynMatrix features = problem.prepareFeatures();
    // int number = 0;
    while (features.cols() > 0) {
      // Logger log{"data/out/mat/feat_" + std::to_string(number)};
      // number++;
      DynMatrix options = solver.forward(features);
      SizeType maxRowIdx, maxColIdx;
      options.maxCoeff(&maxRowIdx, &maxColIdx);
      problem.selectState(maxColIdx);
      features = problem.prepareFeatures();
      // log << features << '\n';
      // std::cout << maxColIdx << '\n';
    }
    return EvaluatingMethod::eval(problem);
  }
  static std::string getName() { return "FeatureBased"; }
};

/// @brief Expects matrix with one row, does nothing
struct HeuristicSolver {
  // DynMatrix getHeuristic(const Problem& p) {
  //   return p.getAreaMismatchHeuristic();
  // }
};

template <EvalType Eval>
struct SolvingPolicy<HeuristicSolver, Eval, EvaluatingPolicy<Eval>> {
  static FloatType solve(Problem& problem,
                         [[maybe_unused]] HeuristicSolver& solver) {
    DynMatrix features = problem.getAreaMismatchHeuristic();
    int number = 0;
    while (features.cols() > 0) {
      Logger log{"data/out/heur6/feat_" + std::to_string(number)};
      number++;

      SizeType maxRowIdx, maxColIdx;
      features.maxCoeff(&maxRowIdx, &maxColIdx);
      problem.selectState(maxColIdx);
      features = problem.getAreaMismatchHeuristic();
      log << features << '\n';
      std::cout << maxColIdx << '\n';
    }
    return EvaluatingPolicy<defaultEvalType>::eval(problem);
  }
  static std::string getName() { return "CustomHeuristic"; }
};

template <EvalType Eval>
struct SolvingPolicy<rbp::MaxRectsBinPack, Eval, EvaluatingPolicy<Eval>> {
  static FloatType solve(Problem& problem, rbp::MaxRectsBinPack& solver) {
    // std::cout << "Something going on...\n";

    solver.Init(problem.containerWidth(), problem.containerHeight(), true);
    std::vector<rbp::RectSize> rects = problem.getRects();
    std::vector<rbp::Rect> solution;
    //
    solver.Insert(rects, solution, MaxRectHeur::get());

    problem.insert(solution);

    return EvaluatingPolicy<Eval>::eval(problem);
  }
  static std::string getName() { return "MaxRectsBinPack"; }
};

}  // namespace kp2d
