#pragma once
#include <iostream>

#include "Rectangle.h"

namespace kp2d {

class RectangleGroup {
 public:
  RectangleGroup(IntType width, IntType height, IntType number)
      : size_{width, height}, avaliableNumber_{number} {}
  RectangleGroup(Point size, IntType number)
      : size_{size}, avaliableNumber_{number} {}
  RectangleGroup() : size_{0, 0}, avaliableNumber_{0} {}

  Rectangle getRectangle(const Point& placement) {
    avaliableNumber_--;
    return Rectangle{size_, placement};
  }

  Rectangle getRectangle() {
    avaliableNumber_--;
    return Rectangle{size_};
  }

  Rectangle seeRectangle() const { return Rectangle{size_}; }

  bool empty() const { return avaliableNumber_ <= 0; }
  IntType width() const { return size_.x(); }
  IntType height() const { return size_.y(); }
  IntType area() const { return size_.product(); }
  IntType number() const { return avaliableNumber_; }
  IntType sumArea() const { return avaliableNumber_ * size_.product(); }
  bool isValidIn(const Point& container) const;

  void addRectangle() { avaliableNumber_++; }
  void removeRectangle() { avaliableNumber_ = avaliableNumber_ - 1; }
  void setNumber(IntType n) { avaliableNumber_ = n; }

  bool isEquiv(const RectangleGroup& rhs) const {
    return (this->width() == rhs.width() && this->height() == rhs.height()) ||
           (this->width() == rhs.height() && this->height() == rhs.width());
  }

  bool operator==(const RectangleGroup& rhs) const {
    return (this->width() == rhs.width() && this->height() == rhs.height() &&
            this->number() == rhs.number());
  }

  bool consolidateIfPossible(RectangleGroup& rhs) {
    if (this->isEquiv(rhs)) {
      this->setNumber(this->number() + rhs.number());
      rhs.setNumber(0);
      // std::cout << this->avaliableNumber_ << " " << rhs.number() << "\n";
      return true;
    }
    return false;
  }

  friend std::ostream& operator<<(std::ostream& os, const RectangleGroup& rb) {
    return (os << std::right << '[' << std::setw(cco::ColumnWidth)
               << rb.size_.x() << " x " << std::setw(cco::ColumnWidth)
               << rb.size_.y() << "]x" << std::setw(cco::ColumnWidth)
               << std::left << rb.avaliableNumber_ << std::right);
  }

  static struct CompareByArea {
    bool operator()(const RectangleGroup& rhs, const RectangleGroup& lhs) {
      return rhs.area() > lhs.area();
    }
  } byArea;
  static struct CompareByHeight {
    bool operator()(const RectangleGroup& rhs, const RectangleGroup& lhs) {
      return rhs.height() > lhs.height();
    }
  } byHeight;
  static struct CompareByWidth {
    bool operator()(const RectangleGroup& rhs, const RectangleGroup& lhs) {
      return rhs.width() > lhs.width();
    }
  } byWidth;
  static struct CompareBySumArea {
    bool operator()(const RectangleGroup& rhs, const RectangleGroup& lhs) {
      return rhs.sumArea() > lhs.sumArea();
    }
  } bySumArea;
  static struct CompareByWidthThenHeight {
    bool operator()(const RectangleGroup& rhs, const RectangleGroup& lhs) {
      return (rhs.width() == lhs.width() ? rhs.height() > lhs.height()
                                         : rhs.width() > lhs.width());
    }
  } byWidthHeight;

 private:
  Point size_;
  IntType avaliableNumber_;
};

}  // namespace kp2d