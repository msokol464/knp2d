#pragma once
#include "../../../nnet/include/nnet/Network.h"
#include "Generators.h"
#include "Problem.h"
#include "ProblemSet.h"

namespace kp2d {

class ProblemGenerator {
 public:
  ProblemGenerator(Point container, std::unique_ptr<IntDistrib>&& nDistr,
                   std::unique_ptr<IntDistrib>&& wDistr,
                   std::unique_ptr<IntDistrib>&& hDistr,
                   FeatOption opt = FeatOption::allFeatures,
                   SizeType unitsPerSample = param::DefaultUnitsPerSample)
      : storGen_{PointGenerator::create(std::move(wDistr), std::move(hDistr)),
                 std::move(nDistr)},
        container_{container},
        featureOption_{opt},
        unitsPerSample_{unitsPerSample},
        numOfGroups_{-1} {
    expect<ErrorAction::Throwing>(container_.x() > 0 && container_.y() > 0,
                                  ErrorCode::FaultyStateContainer,
                                  __PRETTY_FUNCTION__);
  }

  ProblemGenerator(Point container, std::unique_ptr<IntDistrib>&& nDistr,
                   std::unique_ptr<IntDistrib>&& distr,
                   FeatOption opt = FeatOption::allFeatures,
                   SizeType unitsPerSample = param::DefaultUnitsPerSample)
      : storGen_{PointGenerator::create(std::move(distr)), std::move(nDistr)},
        container_{container},
        featureOption_{opt},
        unitsPerSample_{unitsPerSample},
        numOfGroups_{-1} {
    expect<ErrorAction::Throwing>(container_.x() > 0 && container_.y() > 0,
                                  ErrorCode::FaultyStateContainer,
                                  __PRETTY_FUNCTION__);
  }

  ProblemGenerator(ProblemGenerator&& pgen)
      : storGen_{std::move(pgen.storGen_)},
        container_{pgen.container_},
        featureOption_{pgen.featureOption_},
        unitsPerSample_{pgen.unitsPerSample_},
        numOfGroups_{pgen.numOfGroups_} {}

  ProblemGenerator(const ProblemGenerator& pgen)
      : storGen_{pgen.storGen_},
        container_{pgen.container_},
        featureOption_{pgen.featureOption_},
        unitsPerSample_{pgen.unitsPerSample_},
        numOfGroups_{pgen.numOfGroups_} {}

  ~ProblemGenerator() {}

  bool operator==(const ProblemGenerator& pg) const;

  IntType numOfGroups() const { return numOfGroups_; }
  void setNumOfGroups(IntType num) { numOfGroups_ = num; }
  void setFeatureOption(FeatOption opt) { featureOption_ = opt; }

  IntType numOfFeatures();
  Problem generate();
  std::unique_ptr<Problem> generatePtr();

  template <class Solver, EvalType evalType = defaultEvalType>
  ProblemSet<Solver, evalType> generateSet(SizeType number) {
    if (numOfGroups_ > 0)
      return generateSet<Solver, evalType>(number, numOfGroups_);

    std::vector<Problem> problems;
    IntType maxNum = 0;
    for (size_t i = 0; i < 5; i++) {
      std::unique_ptr<Storage> stor = storGen_.generate(container_);
      maxNum = std::max(stor->numOfGroups(), maxNum);
    }
    numOfGroups_ = maxNum;

    while (number-- > 0) {
      problems.push_back(Problem{container_,
                                 storGen_.generate(container_, numOfGroups_),
                                 featureOption_, unitsPerSample_});
    }
    return ProblemSet<Solver, evalType>{std::move(problems)};
  }

  std::vector<Problem> generateProblems(SizeType numOfProblems,
                                        IntType numOfGroups) {
    std::vector<Problem> problems;
    while (numOfProblems-- > 0) {
      problems.push_back(Problem{container_,
                                 storGen_.generate(container_, numOfGroups),
                                 featureOption_, unitsPerSample_});
    }
    return problems;
  }

  template <class Solver, EvalType evalType = defaultEvalType>
  ProblemSet<Solver, evalType> generateSet(SizeType numOfProblems,
                                           IntType numOfGroups) {
    return ProblemSet<Solver, evalType>{
        generateProblems(numOfProblems, numOfGroups)};
  }

  template <class Solver, EvalType evalType = defaultEvalType>
  ProblemSet<Solver, evalType> generateSetFixed(SizeType numOfProblems) {
    std::vector<Problem> problems;

    if (numOfGroups_ <= 0) {
      problems.push_back(Problem{container_, storGen_.generate(container_),
                                 featureOption_, unitsPerSample_});

      numOfGroups_ = problems.front().numOfGroups();
      numOfProblems--;
    }

    for (; numOfProblems > 0; numOfProblems--) {
      problems.push_back(Problem{container_,
                                 storGen_.generate(container_, numOfGroups_),
                                 featureOption_, unitsPerSample_});
    }
    return ProblemSet<Solver, evalType>{std::move(problems)};
  }

  IntType numOfxAxisSamples() const;

 private:
  StorageGenerator storGen_;
  Point container_;
  FeatOption featureOption_;
  SizeType unitsPerSample_;
  IntType numOfGroups_;
};

}  // namespace kp2d
