#pragma once
#include <iostream>
#include <memory>
#include <vector>

#include "Config.h"
#include "Logger.h"
#include "Rectangle.h"
#include "RectangleGroup.h"

namespace kp2d {

class State {
 public:
  State(const Point& container,
        SizeType unitsPerSample = param::DefaultUnitsPerSample);
  State(const State& prevState, Rectangle newRect);

  State(const State& s);
  ~State() = default;

  State& operator=(State&& other) = default;

  static std::unique_ptr<State> create(const State& prevState,
                                       Rectangle newRect);
  static std::unique_ptr<State> create(
      const Point& container,
      SizeType unitsPerSample = param::DefaultUnitsPerSample);

  const Point& container() const { return containerSize_; }

  /// @brief Number of features in addition to sampled X Distances
  static SizeType NumOfFeatures(FeatOption opt);

  /// FIX: insecure access
  Point getPoint(IntType idx) const { return cornerPoints_[idx]; }

  IntType envelopeArea() const { return envelopeArea_; }
  IntType usedArea() const { return usedArea_; }

  SizeType numOfCornerPoints() const { return cornerPoints_.size(); }
  SizeType numOfxAxisSamples() const { return xAxisDistances_.size(); }
  SizeType unitsPerSample() const { return unitsPerSample_; }

  FloatType usedAreaPortion() const;
  FloatType envelopeAreaPortion() const;
  FloatType envelopeAreaIncrease() const;
  FloatType blockedAreaPortion() const;

  FloatType hasUpperCPYMismatch() const {
    return upperCPYMismatch_ == 0 ? 0.0 : 1.0;
  }
  FloatType hasLowerCPXMismatch() const {
    return lowerCPXMismatch_ == 0 ? 0.0 : 1.0;
  }
  FloatType mismatch() const {
    FloatType mnum = 0.0;
    if (lowerCPXMismatch_ != 0) mnum += 1.0;
    if (upperCPYMismatch_ != 0) mnum += 1.0;
    return mnum;
  }

  IntType upperCPYMismatch() const { return upperCPYMismatch_; }
  IntType lowerCPXMismatch() const { return lowerCPXMismatch_; }

  const std::vector<Point>& cornerPoints() const { return cornerPoints_; }
  const std::vector<IntType>& xAxisDistances() const { return xAxisDistances_; }

  void validate() const;
  bool isValid() const;

  std::string cornerPointsToString() const;
  std::string xAxisDistToString() const;

  void appendStateFeatures(std::vector<FloatType>& fVec) const;
  void appendXDistFeatures(std::vector<FloatType>& fVec) const;
  void appendMinimalFeatures(std::vector<FloatType>& fVec) const;

 private:
  void prepareCornerPoints(const std::vector<Point>& cpVec, Rectangle newRect);
  void prepareXAxisDists();

  const SizeType unitsPerSample_;

  IntType envelopeArea_;
  IntType prevEnvelopeArea_;
  IntType usedArea_;

  IntType upperCPYMismatch_;
  IntType lowerCPXMismatch_;

  Point containerSize_;

  std::vector<Point> cornerPoints_;
  std::vector<IntType> xAxisDistances_;
};

}  // namespace kp2d