#pragma once
#include <memory>

#include "Distribution.h"
#include "Logger.h"
#include "Storage.h"

namespace kp2d {

class PointGenerator {
 public:
  virtual Point generate() = 0;
  virtual ~PointGenerator() {}
  virtual std::unique_ptr<PointGenerator> clone() const = 0;

  static std::unique_ptr<PointGenerator> create(
      std::unique_ptr<IntDistrib>&& xDistr,
      std::unique_ptr<IntDistrib>&& yDistr);

  static std::unique_ptr<PointGenerator> create(
      std::unique_ptr<IntDistrib>&& distr);

  virtual bool operator==(PointGenerator* pg) const = 0;
};

class DDPointGenerator : public PointGenerator {
 public:
  Point generate() override { return Point{xDistr_->draw(), yDistr_->draw()}; }

  DDPointGenerator(std::unique_ptr<IntDistrib>&& xDistr,
                   std::unique_ptr<IntDistrib>&& yDistr)
      : xDistr_{std::move(xDistr)}, yDistr_{std::move(yDistr)} {}

  std::unique_ptr<PointGenerator> clone() const override {
    return std::unique_ptr<PointGenerator>(
        new DDPointGenerator{xDistr_->clone(), yDistr_->clone()});
  }

  bool operator==(PointGenerator* pg) const override;

 private:
  std::unique_ptr<IntDistrib> xDistr_;
  std::unique_ptr<IntDistrib> yDistr_;
};

class SDPointGenerator : public PointGenerator {
 public:
  Point generate() override { return Point{distr_->draw(), distr_->draw()}; };

  SDPointGenerator(std::unique_ptr<IntDistrib>&& distr)
      : distr_{std::move(distr)} {}

  std::unique_ptr<PointGenerator> clone() const override {
    return std::unique_ptr<PointGenerator>(
        new SDPointGenerator{distr_->clone()});
  }

  bool operator==(PointGenerator* pg) const override;

 private:
  std::unique_ptr<IntDistrib> distr_;
};

class StorageGenerator {
 public:
  std::unique_ptr<Storage> generate(const Point& container);

  std::unique_ptr<Storage> generate(const Point& container,
                                    IntType numOfGroups);

  StorageGenerator(std::unique_ptr<PointGenerator>&& pGen,
                   std::unique_ptr<IntDistrib>&& nDistr)
      : pointGen_{std::move(pGen)}, numberDistr_{std::move(nDistr)} {}

  StorageGenerator(StorageGenerator&& sgen);

  StorageGenerator(const StorageGenerator& sgen)
      : pointGen_{sgen.pointGen_->clone()},
        numberDistr_{sgen.numberDistr_->clone()} {}

  ~StorageGenerator() = default;

  bool operator==(const StorageGenerator& sg) const;
  bool isValid() const { return pointGen_ && numberDistr_; }

 private:
  RectangleGroup generateGroup(const Point& container);

  std::unique_ptr<PointGenerator> pointGen_;
  std::unique_ptr<IntDistrib> numberDistr_;
};

}  // namespace kp2d