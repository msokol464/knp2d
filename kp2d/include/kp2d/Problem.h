#pragma once
#include <iostream>
#include <memory>

#include "../RectangleBinPack/Rect.h"
#include "../nlohmann/json.hpp"
#include "Config.h"
#include "Distribution.h"
#include "Logger.h"
#include "State.h"
#include "Storage.h"

using kp2d::FloatType;

namespace kp2d {

class Problem {
 public:
  Problem(Point containerRectSize, std::unique_ptr<Storage>&& storage,
          FeatOption opt = FeatOption::allFeatures,
          SizeType unitsPerSample = param::DefaultUnitsPerSample)
      : containerSize_{containerRectSize},
        storage_{std::move(storage)},
        featureOption_{opt} {
    state_ = State::create(containerRectSize, unitsPerSample);
    proposedStates_.reserve(storage_->numOfGroups() * 2);
  }
  Problem(const Problem& p);
  Problem(Problem&&) = default;

  Problem& operator=(const Problem& p);
  Problem& operator=(Problem&&) = default;

  bool isEquiv(const Problem& p) const;

  ~Problem() = default;

  // Is it necessary ??? Leaving this for now
  static std::unique_ptr<Problem> create(
      Point containerSize, std::unique_ptr<Storage>&& storage,
      FeatOption opt = FeatOption::allFeatures,
      SizeType unitsPerSample = param::DefaultUnitsPerSample);

  std::string solutionToString() const;
  std::string cornerPointsToString() const;
  std::string xAxisDistToString() const;
  std::string storageToString() const;
  SizeType numOfProposedStates() const { return proposedStates_.size(); }
  SizeType numOfGroups() const { return storage_->numOfGroups(); }
  IntType numOfFeatures() const {
    // std::cout << "[Problem::numOfFeatures] Storage::FeaturesPerGroup * "
    //              "storage_->numOfGroups() = "
    //           << Storage::FeaturesPerGroup * storage_->numOfGroups() << '\n'
    //           << "[Problem::numOfFeatures] State::NumOfFeatures = "
    //           << State::NumOfFeatures << '\n'
    //           << "[Problem::numOfFeatures] state_->numOfxAxisSamples() = "
    //           << state_->numOfxAxisSamples() << '\n';

    // enum FeatOption {
    //   allFeatures,
    //   noXDistances,
    //   areaMismatchHeuristic,
    //   minimalFeatures
    // };
    switch (featureOption_) {
      case FeatOption::allFeatures:
        return Storage::FeaturesPerGroup(featureOption_) *
                   storage_->numOfGroups() +
               State::NumOfFeatures(featureOption_) +
               state_->numOfxAxisSamples();
      case FeatOption::noXDistances:
        return Storage::FeaturesPerGroup(featureOption_) *
                   storage_->numOfGroups() +
               State::NumOfFeatures(featureOption_) + 4;
      case FeatOption::areaMismatchHeuristic:
        return 1;
      case FeatOption::minimalFeatures:
        return 5 + State::NumOfFeatures(featureOption_);
      case FeatOption::minXDistances:
        return 4 + state_->numOfxAxisSamples();
      default:
        return 0;
    }
  }

  static IntType predictedNumOfFeatures(IntType numOfGroups, IntType H,
                                        IntType uPerSample, FeatOption option);

  DynMatrix prepareFeatures();

  IntType containerWidth() const { return containerSize_.x(); }
  IntType containerHeight() const { return containerSize_.y(); }

  // DynMatrix getWidthHeightHeuristic();

  DynMatrix getAreaMismatchHeuristic();

  FloatType unusedContainerPortion() const;
  FloatType unusedStorageArea() const;
  FloatType blockedAreaPortion() const {
    if (state_) return state_->blockedAreaPortion();

    assert(false);
    return 0;
  }

  template <class SortingRule>
  void sort(SortingRule rule) {
    storage_->sort<SortingRule>(rule);
  }

  void selectState(SizeType idx);
  const std::vector<Rectangle>& solution() const { return solution_; }
  bool hasSolution() const {
    std::cout << "[Problem::hasSolution] Solution size: " << solution_.size()
              << '\n';
    return solution_.size() > 0;
  }

  void validate() const {
    expect(containerSize_ == state_->container() &&
               containerSize_ == storage_->container(),
           ErrorCode::ContainerMismatch);
    state_->validate();
    storage_->validate();
    for (const auto& state : proposedStates_) state->validate();
  }

  bool isValid() const {
    if (!(containerSize_ == state_->container() &&
          containerSize_ == storage_->container()))
      return false;

    if (containerSize_.x() <= 0 || containerSize_.y() <= 0) return false;
    if (!state_->isValid()) return false;
    if (!storage_->isValid()) return false;

    for (const auto& state : proposedStates_) {
      if (!state->isValid()) return false;
    }
    return true;
  }

  bool validateSolution() const;
  nlohmann::ordered_json toJson() const;
  nlohmann::ordered_json solutionToJson() const;
  void saveToJson(std::ofstream& oFile) const;

  /// @brief Functions accomodating solution from heuristic algorithm
  void insert(const std::vector<rbp::Rect>& rects);
  std::vector<rbp::RectSize> getRects();

 private:
  Point containerSize_;

  std::unique_ptr<Storage> storage_;
  std::vector<Rectangle> solution_;

  std::unique_ptr<State> state_;
  std::vector<std::unique_ptr<State>> proposedStates_;

  std::vector<Rectangle> proposedRects_;

  FeatOption featureOption_;

  bool canBePlaced(const Rectangle& r) const;

 public:
  friend std::ostream& operator<<(std::ostream& os, const Problem& problem) {
    os << std::string(32, '=') << "\n2d KNAPSACK PROBLEM\n"
       << std::string(32, '=') << "\nContainer: " << problem.containerSize_
       << "\nUnits per sample: " << problem.state_->unitsPerSample()
       << "\nUsed area portion: " << problem.state_->usedAreaPortion() * 100.0
       << '%' << "\nEnvelope area potrion: "
       << problem.state_->envelopeAreaPortion() * 100.0 << '%'
       << "\n\nRECTANGLE GROUPS\nArea: " << problem.storage_->sumArea() << '\n'
       << std::string(32, '-') << '\n'
       << problem.storage_->toString() << "\nCORNER POINTS\n"
       << std::string(32, '-') << '\n'
       << problem.state_->cornerPointsToString() << "\nX-AXIS DISTANCES\n"
       << std::string(32, '-') << '\n'
       << problem.state_->xAxisDistToString();

    if (problem.solution_.size() > 0) {
      os << "\nSOLUTION\n"
         << std::string(32, '-') << '\n'
         << problem.solutionToString();
    }
    return os;
  }
};

}  // namespace kp2d