#pragma once

#include <algorithm>
#include <optional>
#include <sstream>
#include <vector>

#include "../nlohmann/json.hpp"
#include "Distribution.h"
#include "RectangleGroup.h"

namespace kp2d {

class Storage {
 public:
  Storage(std::vector<RectangleGroup>&& vect, const Point& container);
  Storage(const Point& container, SizeType maxGroups);

  static std::unique_ptr<Storage> create(std::vector<RectangleGroup>&& vect,
                                         const Point& container);
  static std::unique_ptr<Storage> create(const Point& container,
                                         SizeType maxGroups = param::MaxGroups);
  std::unique_ptr<Storage> clone() {
    return std::unique_ptr<Storage>(new Storage{*this});
  }

  Storage(const Storage&) = default;
  ~Storage() = default;

  static SizeType FeaturesPerGroup(FeatOption opt);

  // int rectsNumber() const { return numOfRectangles_; }
  IntType numOfGroups() const { return rectGroups_.size(); }
  // SizeType numOfNonEmptyGroups() const { return 0; }

  bool hasGroups() const { return rectGroups_.size() > 0; }
  IntType sumArea() const { return sumArea_; }

  std::string toString() const;
  nlohmann::ordered_json toJson() const;
  Point container() const { return containerSize_; }
  RectangleGroup& at(size_t index) { return rectGroups_.at(index); };

  template <class SortingRule>
  bool isSorted(SortingRule rule) const {
    return std::is_sorted(begin(rectGroups_), end(rectGroups_), rule);
  }

  template <class SortingRule>
  void sort(SortingRule rule) {
    std::sort(begin(rectGroups_), end(rectGroups_), rule);
  }

  void setSumArea();
  void addGroup(const RectangleGroup& group);
  void removeEmptyGroups();
  void removeRectangle(const Rectangle& rect);
  void consolidate();
  bool isEquiv(const Storage& other) const;  /// DESTROYS SORTING - not ideal

  /// TODO: delete?? function for testing purposes,
  /// tests might have been deleted
  Rectangle getRandomRectangleAtPoint(const Point& placement);
  // void returnToStorage(Rectangle r);

  void appendAllFeatures(std::vector<FloatType>& fVec,
                         Rectangle currentRect) const;

  void validate() const;
  bool isValid() const;

  struct AccessRange {
    std::vector<RectangleGroup>::iterator begin_;
    std::vector<RectangleGroup>::iterator end_;
    std::vector<RectangleGroup>::iterator begin() { return begin_; }
    std::vector<RectangleGroup>::iterator end() { return end_; }
  };

  AccessRange rectGroups();

  /// TODO: check if used
  std::vector<RectangleGroup>::iterator beginRectGroups();
  std::vector<RectangleGroup>::iterator endRectGroups();

 private:
  std::vector<RectangleGroup> rectGroups_;

  IntType sumArea_;

  Point containerSize_;

  // SizeType numOfNonEmptyGroups_;
  // SizeType numOfRectangles_;
};

}  // namespace kp2d