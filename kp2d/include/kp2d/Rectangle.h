#pragma once
#include <array>
#include <cassert>
#include <cmath>
#include <iomanip>
#include <memory>
#include <optional>

#include "Config.h"
#include "Logger.h"

namespace kp2d {

class Point {
 public:
  Point(IntType xValue, IntType yValue) : x_{xValue}, y_{yValue} {}
  Point(Point&&) = default;
  Point(const Point&) = default;

  Point& operator=(const Point& rhs) = default;

  inline bool operator==(const Point& rhs) const {
    return rhs.x() == x_ && rhs.y() == y_;
  }
  inline bool operator!=(const Point& rhs) const {
    return rhs.x() != x_ || rhs.y() != y_;
  }
  inline bool operator<(const Point& rhs) const {
    return rhs.x() < x_ && rhs.y() < y_;
  }
  inline bool operator>(const Point& rhs) const {
    return rhs.x() > x_ && rhs.y() > y_;
  }

  IntType product() const { return x_ * y_; }
  IntType x() const { return x_; }
  IntType y() const { return y_; }

  void swapAxis() { std::swap(x_, y_); }

  friend std::ostream& operator<<(std::ostream& os, const Point& p) {
    return (os << std::right << '(' << std::right << std::setw(cco::ColumnWidth)
               << p.x_ << ", " << std::right << std::setw(cco::ColumnWidth)
               << p.y_ << ')');
  }

 private:
  IntType x_;
  IntType y_;
};

class Rectangle {
 public:
  static Rectangle rotated(const Rectangle& r) {
    return Rectangle{{r.height(), r.width()}, r.anchorPoint()};
  }
  static std::array<Rectangle, 2> getOrientations(const Rectangle& r) {
    return {r, Rectangle::rotated(r)};
  }

  Rectangle(Point size, Point anchor = Point{0, 0})
      : anchorPoint_{anchor}, size_{size} {
    expect(size_.x() >= 0 && size_.y() >= 0, ErrorCode::IllFormedRectangle,
           __PRETTY_FUNCTION__);
  }

  Rectangle(IntType x, IntType y, IntType w, IntType h)
      : anchorPoint_{x, y}, size_{w, h} {
    expect(size_.x() >= 0 && size_.y() >= 0, ErrorCode::IllFormedRectangle,
           __PRETTY_FUNCTION__);
  }

  Rectangle(const Rectangle&) = default;
  Rectangle(Rectangle&&) = default;
  ~Rectangle() = default;

  Rectangle& operator=(const Rectangle&) = default;
  Rectangle& operator=(Rectangle&&) = default;
  bool operator==(const Rectangle& r) const {
    // return (size_ == r.size_ && anchorPoint_ == r.anchorPoint_);
    return (size_ == r.size_);
  }
  bool operator!=(const Rectangle& r) const { return !(*this == r); }

  IntType rightmostX() const { return anchorPoint_.x() + size_.x(); }
  IntType uppermostY() const { return anchorPoint_.y() + size_.y(); }

  Point size() const { return size_; }
  Point anchorPoint() const { return anchorPoint_; }

  IntType area() const { return size_.product(); }
  IntType width() const { return size_.x(); }
  IntType height() const { return size_.y(); }
  // bool isRotated() const { return isRotated_; }
  bool isEquiv(const Rectangle& r) const {
    if (this->width() == 0 || this->height() == 0 || r.width() == 0 ||
        r.height() == 0)
      return false;
    return (this->width() == r.width() && this->height() == r.height()) ||
           (this->width() == r.height() && this->height() == r.width());
  }
  bool isIntersectedBy(const Rectangle& other) const;
  bool isValid() const { return width() > 0 && height() > 0; }

  /// @brief Swaps hight and width of the rectangle and flips isRotated_
  /// @return Changed rectangle
  Rectangle rotate();

  /// @brief Change coordinates of lower-right anchor point of the rectangle
  /// @param point new anchor point coordinates
  void setAnchorPoint(const Point& point) { anchorPoint_ = point; }

  friend std::ostream& operator<<(std::ostream& os, const Rectangle& r) {
    return (os << '[' << std::setw(cco::ColumnWidth) << r.size_.x() << " x "
               << std::setw(cco::ColumnWidth) << r.size_.y() << "] at"
               << r.anchorPoint_);
  }

 private:
  Point anchorPoint_;
  Point size_;
  // bool isRotated_;  // probably used in returnToStorage
};

}  // namespace kp2d
