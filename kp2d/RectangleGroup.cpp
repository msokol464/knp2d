#include "include/kp2d/RectangleGroup.h"

using namespace kp2d;

bool kp2d::RectangleGroup::isValidIn(const Point& container) const {
  return width() > 0 && width() <= container.x() && height() > 0 &&
         height() <= container.y() && number() > 0 &&
         sumArea() <= container.product();
}
