#include "include/kp2d/CutProblemGenerator.h"

#include "CutProblemGenerator.h"

kp2d::CutProblemGenerator::CutProblemGenerator(
    Point container, IntType binsX, IntType binsY,
    std::unique_ptr<IntDistrib>&& nDistrib, IntType numOfGroups, FeatOption opt,
    SizeType unitsPerSample)
    : container_{container},
      binsX_{binsX},
      binsY_{binsY},
      distrib_{std::move(nDistrib)},
      numOfGroups_{numOfGroups},
      featureOption_{opt},
      unitsPerSample_{unitsPerSample} {}

kp2d::Problem kp2d::CutProblemGenerator::generate(AreaOption aOpt) {
  // Generate storage
  // Storage(std::vector<RectangleGroup>&& vect, const Point& container);
  std::vector<RectangleGroup> rectGroups;

  if (numOfGroups_ < 0) {
    //
    setNumAndGenerateGroups(rectGroups, aOpt);
  } else {
    rectGroups.reserve(numOfGroups_);
    generateGroups(rectGroups, aOpt);
  }

  return Problem{container_, Storage::create(std::move(rectGroups), container_),
                 featureOption_, unitsPerSample_};
}

void kp2d::CutProblemGenerator::generateGroups(
    std::vector<RectangleGroup>& rectGroups, AreaOption aOpt) {
  IntType divX = 0, divY = 0;
  IntType max = binsY_ * (binsX_ - 1);

  std::random_device rd{};
  std::mt19937 mt{rd()};

  std::vector<IntType> flattenedIndicies(binsX_ * binsY_);
  std::iota(flattenedIndicies.begin(), flattenedIndicies.end(), 0);

  RectangleGroup group;
  IntType groupsSumArea = 0;
  bool stopGenerating = false;
  for (IntType i = 0; i < numOfGroups_; i++) {
    // There was area overflow or underflow
    // but there are still less than numOfGroups groups in vector
    if (stopGenerating && numOfGroups_ > 0) {
      rectGroups.resize(numOfGroups_);
      break;
    }

    setDivisors(divX, divY, flattenedIndicies, mt, max);
    --max;
    assert(divX > 0 && divY > 0);

    group = RectangleGroup{container_.x() / divX, container_.y() / divY,
                           distrib_->draw()};

    stopGenerating = addOrDiscardGroup(aOpt, groupsSumArea, group, rectGroups);
  }

  if (aOpt == AreaOption::AreaOverflow) {
    UniformInt U{0, int(rectGroups.size()) - 1};
    int idx = 0;
    while (groupsSumArea <= container_.product()) {
      idx = U.draw();

      rectGroups[idx].addRectangle();
      groupsSumArea += rectGroups[idx].area();
    }
  }
}

void kp2d::CutProblemGenerator::setNumAndGenerateGroups(
    std::vector<RectangleGroup>& rectGroups, AreaOption aOpt) {
  //
  assert(rectGroups.size() == 0);
  IntType divX = 0, divY = 0;
  IntType max = binsY_ * (binsX_ - 1);

  std::random_device rd{};
  std::mt19937 mt{rd()};

  std::vector<IntType> flattenedIndicies(binsX_ * binsY_);
  std::iota(flattenedIndicies.begin(), flattenedIndicies.end(), 0);
  rectGroups.reserve(flattenedIndicies.size());

  RectangleGroup group;
  IntType groupsSumArea = 0;
  bool stopGenerating = false;
  while (!stopGenerating) {
    setDivisors(divX, divY, flattenedIndicies, mt, max--);
    assert(divX > 0 && divY > 0);

    group = RectangleGroup{container_.x() / divX, container_.y() / divY,
                           distrib_->draw()};
    stopGenerating = addOrDiscardGroup(aOpt, groupsSumArea, group, rectGroups);
  }

  assert(rectGroups.size() > 0);
  numOfGroups_ = rectGroups.size();
}

bool kp2d::CutProblemGenerator::addOrDiscardGroup(
    const AreaOption& aOpt, IntType& groupsSumArea, RectangleGroup& newGroup,
    std::vector<RectangleGroup>& currentGroups) {
  bool stopGenerating = false;
  if (groupsSumArea + newGroup.sumArea() > container_.product()) {
    while (groupsSumArea + newGroup.sumArea() > container_.product()) {
      newGroup.removeRectangle();
    }
    if (aOpt == AreaOption::AreaOverflow) {
      newGroup.addRectangle();
    }
    if (newGroup.number() > 0) {
      groupsSumArea += newGroup.sumArea();
      currentGroups.push_back(newGroup);
    } else {
      currentGroups.emplace_back();
    }
    stopGenerating = true;
  } else {
    groupsSumArea += newGroup.sumArea();
    currentGroups.push_back(newGroup);
  }
  return stopGenerating;
}
