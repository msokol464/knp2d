#include "include/kp2d/FixedProblemGenerator.h"

#include "FixedProblemGenerator.h"

kp2d::FixedProblemGenerator::FixedProblemGenerator(
    const Point& c, const std::vector<Point>& sizes,
    std::unique_ptr<IntDistrib>&& numdistr, std::unique_ptr<IntDistrib>&& noise,
    bool areaConstraint, FeatOption opt, SizeType ups)
    : container_{c},
      elemSizes_{sizes},
      numDistr_{std::move(numdistr)},
      noiseDistr_{std::move(noise)},
      areaConstraint_{areaConstraint},
      featureOption_{opt},
      unitsPerSample_{ups} {}

kp2d::Problem kp2d::FixedProblemGenerator::generate() {
  std::vector<RectangleGroup> groups;
  groups.reserve(elemSizes_.size());

  IntType sumArea = 0;
  bool substractionNeeded = false;
  for (const Point& size : elemSizes_) {
    RectangleGroup g{size, numDistr_->draw()};
    sumArea += g.sumArea();
    groups.push_back(g);
    if (sumArea > container_.product()) substractionNeeded = true;
  }

  if (areaConstraint_ && substractionNeeded) {
    std::uniform_int_distribution<size_t> U(0, elemSizes_.size() - 1);
    while (sumArea > container_.product()) {
      size_t idx = U(RndEngine::get());
      if (groups[idx].number() > 1) {
        groups[idx].removeRectangle();
        sumArea -= groups[idx].area();
      }
    }
  }

  return Problem{container_, Storage::create(std::move(groups), container_),
                 featureOption_, unitsPerSample_};
}

kp2d::Problem kp2d::FixedProblemGenerator::generateNoisy() {
  if (!noiseDistr_) {
    // std::cout << "[FixedProblemGenerator::generateNoisy] Missing noise
    // distrbution, generating knapsack problem wihout noise\n";
    return generate();
  }
  std::vector<RectangleGroup> groups;
  groups.reserve(elemSizes_.size());

  IntType sumArea = 0;
  bool substractionNeeded = false;
  for (const Point& size : elemSizes_) {
    IntType width = 0, height = 0, number = 0;

    while (width <= 0 || width > container_.x()) {
      width = size.x() + noiseDistr_->draw();
    }

    while (height <= 0 || height > container_.y()) {
      height = size.y() + noiseDistr_->draw();
    }

    number = numDistr_->draw();

    RectangleGroup g{width, height, number};
    sumArea += g.sumArea();
    groups.push_back(g);
    if (sumArea > container_.product()) substractionNeeded = true;
  }

  if (areaConstraint_ && substractionNeeded) {
    std::uniform_int_distribution<size_t> U(0, elemSizes_.size() - 1);
    while (sumArea > container_.product()) {
      size_t idx = U(RndEngine::get());
      if (groups[idx].number() > 1) {
        groups[idx].removeRectangle();
        sumArea -= groups[idx].area();
      }
    }
  }

  return Problem{container_, Storage::create(std::move(groups), container_),
                 featureOption_, unitsPerSample_};
}

std::vector<kp2d::Problem> kp2d::FixedProblemGenerator::generateProblems(
    size_t numProblems) {
  std::vector<Problem> problems;
  problems.reserve(numProblems);

  if (!noiseDistr_)
    for (size_t i = 0; i < numProblems; i++) {
      problems.push_back(generate());
    }
  else {
    for (size_t i = 0; i < numProblems; i++) {
      problems.push_back(generateNoisy());
    }
  }
  return problems;
}
