#include "include/kp2d/Generators.h"

#include "Generators.h"

// #include <iostream>

using namespace kp2d;

/// @brief Create generator object to make random points
/// @param xDistr Distribution for values of x
/// @param yDistr Distribution for values of y
/// @return Unique pointer to generator object
std::unique_ptr<PointGenerator> PointGenerator::create(
    std::unique_ptr<IntDistrib>&& xDistr,
    std::unique_ptr<IntDistrib>&& yDistr) {
  return std::unique_ptr<PointGenerator>(
      new DDPointGenerator(std::move(xDistr), std::move(yDistr)));
}

/// @brief Create generator object to make random points
/// @param distr Distribution for both values of x and y
/// @return Unique pointer to generator object
std::unique_ptr<PointGenerator> PointGenerator::create(
    std::unique_ptr<IntDistrib>&& distr) {
  return std::unique_ptr<PointGenerator>(
      new SDPointGenerator(std::move(distr)));
}

kp2d::StorageGenerator::StorageGenerator(StorageGenerator&& sgen) {
  //
  pointGen_ = std::unique_ptr<PointGenerator>(nullptr);
  pointGen_.swap(sgen.pointGen_);
  numberDistr_ = std::unique_ptr<IntDistrib>(nullptr);
  numberDistr_.swap(sgen.numberDistr_);
}

bool kp2d::StorageGenerator::operator==(const StorageGenerator& sg) const {
  bool a = (pointGen_->operator==(sg.pointGen_.get()));

  bool b = (numberDistr_->operator==(sg.numberDistr_.get()));

  return a && b;
}

RectangleGroup StorageGenerator::generateGroup(const Point& container) {
  Point rectSize{-1, -1};
  IntType rectNumber, trialsSize = 0, trialsNum = 0;

  do {
    rectSize = pointGen_->generate();
    // ++trialsSize;
  } while ((rectSize.x() <= 0 || rectSize.y() <= 0 ||
            rectSize.x() > container.x() || rectSize.y() > container.y()) &&
           trialsSize++ < param::MaxRandTrials);
  expect(trialsSize < param::MaxRandTrials, ErrorCode::ExceededMaxTrials,
         __PRETTY_FUNCTION__);

  do {
    rectNumber = numberDistr_->draw();
    // ++trialsNum;
  } while (rectNumber <= 0 && trialsNum++ < param::MaxRandTrials);
  expect(trialsNum < param::MaxRandTrials, ErrorCode::ExceededMaxTrials,
         __PRETTY_FUNCTION__);

  return RectangleGroup{rectSize, rectNumber};
}

std::unique_ptr<Storage> StorageGenerator::generate(const Point& container) {
  IntType sumArea = 0, propArea = 0;
  IntType maxArea = container.product();
  IntType iter = 0;

  std::unique_ptr<Storage> storPtr = Storage::create(container);

  do {
    RectangleGroup gr{-1, -1, -1};

    do {
      gr = generateGroup(container);
      propArea = sumArea + gr.sumArea();
      ++iter;

    } while ((!gr.isValidIn(container) || propArea > maxArea) &&
             iter < param::MaxRandTrials);

    if (propArea <= maxArea) {
      storPtr->addGroup(std::move(gr));
      sumArea = propArea;
      iter = 0;
    }

  } while (sumArea <= maxArea && iter < param::MaxRandTrials);

  expect(iter < param::MaxRandTrials, ErrorCode::ExceededMaxTrials,
         __PRETTY_FUNCTION__);

  storPtr->consolidate();
  return storPtr;
}

std::unique_ptr<Storage> StorageGenerator::generate(const Point& container,
                                                    IntType numOfGroups) {
  std::unique_ptr<Storage> storPtr = Storage::create(container);

  while (storPtr->numOfGroups() < numOfGroups) {
    IntType iter = 0;
    RectangleGroup gr{-1, -1, -1};
    // std::cout << "A\n";
    do {
      gr = generateGroup(container);
      ++iter;
      // std::cout << "B\n";
    } while (!gr.isValidIn(container) && iter < param::MaxRandTrials);

    expect<ErrorAction::Throwing>(gr.isValidIn(container),
                                  ErrorCode::ExceededMaxTrials);
    storPtr->addGroup(gr);
  }
  storPtr->consolidate();

  IntType extraArea = storPtr->sumArea() - container.product();

  while (extraArea > 0) {
    // std::cout << "C\n";
    for (auto group = storPtr->beginRectGroups();
         group != storPtr->endRectGroups() && extraArea > 0; group++) {
      // std::cout << "D\n";
      if (group->number() > 0) {
        group->removeRectangle();
        extraArea -= group->area();
      }
    }
  }
  storPtr->setSumArea();
  return storPtr;
}

bool kp2d::DDPointGenerator::operator==(kp2d::PointGenerator* pg) const {
  if (pg == nullptr)
    return false;
  else if (typeid(*this) != typeid(*pg))
    return false;

  return (
      *(xDistr_.get()) == dynamic_cast<DDPointGenerator*>(pg)->xDistr_.get() &&
      *(yDistr_.get()) == dynamic_cast<DDPointGenerator*>(pg)->yDistr_.get());
}

bool kp2d::SDPointGenerator::operator==(kp2d::PointGenerator* pg) const {
  if (pg == nullptr)
    return false;
  else if (typeid(*this) != typeid(*pg))
    return false;

  return (*(distr_.get()) == dynamic_cast<SDPointGenerator*>(pg)->distr_.get());
}
