#include "include/kp2d/Storage.h"

#include <iostream>

#include "Storage.h"

using namespace kp2d;

Storage::Storage(std::vector<RectangleGroup>&& vect, const Point& container)
    : rectGroups_{vect}, sumArea_{0}, containerSize_{container} {
  if (rectGroups_.size() == 0) return;

  setSumArea();
}

Storage::Storage(const Point& container, SizeType maxGroups = param::MaxGroups)
    : sumArea_{0}, containerSize_{container} {
  rectGroups_.reserve(maxGroups);
}

std::unique_ptr<Storage> Storage::create(std::vector<RectangleGroup>&& vect,
                                         const Point& container) {
  return std::unique_ptr<Storage>(new Storage(std::move(vect), container));
}

std::unique_ptr<Storage> Storage::create(const Point& container,
                                         SizeType maxGroups) {
  return std::unique_ptr<Storage>(new Storage(container, maxGroups));
}

SizeType kp2d::Storage::FeaturesPerGroup(FeatOption opt) {
  switch (opt) {
    case FeatOption::allFeatures:
    case FeatOption::noXDistances:
      return 4;
    case FeatOption::areaMismatchHeuristic:
      return 0;
    case FeatOption::minimalFeatures:
      return 3;
    default:
      assert(false);
  }
}

Storage::AccessRange Storage::rectGroups() {
  return AccessRange{beginRectGroups(), endRectGroups()};
}

std::vector<RectangleGroup>::iterator Storage::beginRectGroups() {
  return begin(rectGroups_);
}
std::vector<RectangleGroup>::iterator Storage::endRectGroups() {
  return end(rectGroups_);
}

bool Storage::isEquiv(const Storage& other) const {
  if (numOfGroups() != other.numOfGroups()) return false;

  if (this->container() != other.container()) return false;

  if (!std::is_permutation(
          rectGroups_.begin(), rectGroups_.end(), other.rectGroups_.begin(),
          other.rectGroups_.end(),
          [](const RectangleGroup& rhs, const RectangleGroup& lhs) -> bool {
            return rhs.isEquiv(lhs);
          }))
    return false;

  return true;
}

void Storage::consolidate() {
  if (this->numOfGroups() == 0 || this->sumArea() == 0) return;
  std::sort(begin(rectGroups_), end(rectGroups_),
            [](const RectangleGroup& rhs, const RectangleGroup& lhs) -> bool {
              if (rhs.width() == lhs.width())
                return rhs.height() < lhs.height();
              return rhs.width() < lhs.width();
            });

  auto fIter = rectGroups_.begin();
  auto sIter = fIter;

  for (fIter++; fIter != rectGroups_.end(); fIter++, sIter++) {
    fIter->consolidateIfPossible(*sIter);
  }

  removeEmptyGroups();
}

void Storage::addGroup(const RectangleGroup& group) {
  sumArea_ += group.sumArea();
  rectGroups_.push_back(group);
}

void Storage::removeEmptyGroups() {
  rectGroups_.erase(
      std::remove_if(rectGroups_.begin(), rectGroups_.end(),
                     [](const RectangleGroup& group) { return group.empty(); }),
      rectGroups_.end());
}

void Storage::removeRectangle(const Rectangle& rect) {
  for (auto& gr : rectGroups_) {
    if (rect.isEquiv(gr.seeRectangle())) {
      if (!gr.empty()) {
        gr.removeRectangle();
        sumArea_ -= rect.area();
      }
      // else
      //   numOfNonEmptyGroups_--;
      break;
    }
  }
}

std::string Storage::toString() const {
  std::stringstream oss;
  for (const auto& group : rectGroups_) {
    oss << group << '\n';
  }
  return oss.str();
}

nlohmann::ordered_json kp2d::Storage::toJson() const {
  nlohmann::ordered_json stor, widths, heights, quantities;

  for (const auto& group : rectGroups_) {
    widths.push_back(group.width());
    heights.push_back(group.height());
    quantities.push_back(group.number());
  }
  stor["sumArea"] = sumArea_;
  stor["w"] = widths;
  stor["h"] = heights;
  stor["n"] = quantities;

  return stor;
}

Rectangle Storage::getRandomRectangleAtPoint(const Point& placement) {
  std::unique_ptr<UniformInt> randomGroupIdx =
      UniformInt::create(0, this->numOfGroups() - 1);

  auto group = rectGroups_[randomGroupIdx->draw()];
  while (group.number() <= 0) {
    group = rectGroups_[randomGroupIdx->draw()];
  }
  return group.getRectangle(placement);
}

// void Storage::returnToStorage(Rectangle r) {
//   // if (r.isRotated()) {
//   //   r.rotate();
//   // }

//   for (auto& group : rectGroups_) {
//     if ((group.width() == r.width() && group.height() == r.height()) ||
//         (group.width() == r.height() && group.height() == r.width())) {
//       group.addRectangle();
//       break;
//     }
//   }
// }

void Storage::appendAllFeatures(std::vector<FloatType>& fVec,
                                Rectangle currentRect) const {
  ///-------------------------------------------------------///
  /// NOTE: Important !!!
  /// When changing number of features
  /// it is necessary to also update FeaturesPerGroup value
  /// in Storage.h file
  ///-------------------------------------------------------///
  // std::cout << "[Storage::appendFeatures]" << currentRect << '\n';

  for (const auto& group : rectGroups_) {
    // std::cout << "[Storage::appendFeatures]" << currentRect << '\n';

    // TODO: is this group used
    fVec.push_back(currentRect.isEquiv(group.seeRectangle()) ? 1.0 : 0.0);

    // Group sum area / container area * scalar factor
    fVec.push_back(static_cast<FloatType>(group.sumArea()) /
                   static_cast<FloatType>(containerSize_.product()) * 4);

    // Container side length modulo corresponding rectangle side length
    // normalized
    fVec.push_back(
        static_cast<FloatType>(containerSize_.x() % currentRect.width()) /
        static_cast<FloatType>(currentRect.width()));
    fVec.push_back(
        static_cast<FloatType>(containerSize_.y() % currentRect.height()) /
        static_cast<FloatType>(currentRect.height()));

    // // Anchor point coordinates normalized
    // // std::cout << "[Storage::appendFeatures] anchorPoint: "
    // //           << currentRect.anchorPoint() << " at " << &currentRect <<
    // '\n';

    // fVec.push_back(static_cast<FloatType>(currentRect.anchorPoint().x()) /
    //                static_cast<FloatType>(containerSize_.x()));
    // fVec.push_back(static_cast<FloatType>(currentRect.anchorPoint().y()) /
    //                static_cast<FloatType>(containerSize_.y()));

    // // Distances form rectangle edges to container edges normalized
    // fVec.push_back(
    //     static_cast<FloatType>(containerSize_.x() - currentRect.rightmostX())
    //     / static_cast<FloatType>(containerSize_.x()));
    // fVec.push_back(
    //     static_cast<FloatType>(containerSize_.y() - currentRect.uppermostY())
    //     / static_cast<FloatType>(containerSize_.y()));
  }
}

void Storage::validate() const {
  IntType sumCheck =
      std::accumulate(rectGroups_.begin(), rectGroups_.end(), 0,
                      [](IntType sum, const RectangleGroup& group) {
                        return sum + group.sumArea();
                      });
  expect(sumArea_ == sumCheck, ErrorCode::FaultyStorageSumAreaErr,
         __PRETTY_FUNCTION__);
  // expect(sumArea_ <= containerSize_.product(),
  //        ErrorCode::StorageExceededContainerArea, __PRETTY_FUNCTION__);
  for (const auto& gr : rectGroups_) {
    expect(
        gr.width() <= containerSize_.x() && gr.height() <= containerSize_.y(),
        ErrorCode::GroupExceededContainerArea, __PRETTY_FUNCTION__);
    expect(gr.width() > 0 && gr.height() > 0 && gr.number() >= 0,
           ErrorCode::FaultyGroup, __PRETTY_FUNCTION__);
  }
}

bool Storage::isValid() const {
  IntType sumCheck =
      std::accumulate(rectGroups_.begin(), rectGroups_.end(), 0,
                      [](IntType sum, const RectangleGroup& group) {
                        return sum + group.sumArea();
                      });

  if (sumArea_ != sumCheck) return false;

  // if(sumArea_ <= containerSize_.product()) return false;
  for (const auto& gr : rectGroups_) {
    if (gr.width() < 0) return false;
    if (gr.width() > containerSize_.x()) return false;

    if (gr.height() < 0) return false;
    if (gr.height() > containerSize_.y()) return false;

    if (gr.number() < 0) return false;
  }
  return true;
}

void Storage::setSumArea() {
  sumArea_ = 0;
  for (const auto& gr : rectGroups_) {
    sumArea_ += gr.sumArea();
  }
}