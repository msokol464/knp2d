#include "include/kp2d/Rectangle.h"

#include "Rectangle.h"

using namespace kp2d;

// Ractangle::Orientation[] Ractangle::Orientations =
//     {Ractangle::Orientation::NonRotated, Ractangle::Orientation::Rotated}

bool kp2d::Rectangle::isIntersectedBy(const Rectangle& other) const {
  if (anchorPoint_.x() < other.anchorPoint().x() &&
      rightmostX() <= other.anchorPoint().x())
    return false;
  else if (anchorPoint_.y() < other.anchorPoint().y() &&
           uppermostY() <= other.uppermostY())
    return false;
  else if (anchorPoint_.x() >= other.rightmostX() ||
           anchorPoint_.y() >= other.uppermostY())
    return false;
  return true;
}

Rectangle Rectangle::rotate() {
  // isRotated_ = !isRotated_;
  size_.swapAxis();
  return (*this);
}
