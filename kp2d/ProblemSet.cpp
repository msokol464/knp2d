#include "include/kp2d/ProblemSet.h"

#include "ProblemSet.h"

template <class Solver, kp2d::EvalType Eval, class EvaluatingMethod,
          class SolvingMethod>
inline bool kp2d::ProblemSet<Solver, Eval, EvaluatingMethod,
                             SolvingMethod>::isUnsolved() const {
  std::cout << "[ProblemSet] Problem vec size: " << problems_.size() << '\n';
  std::cout << "[ProblemSet]  Eval vec size: " << evaluations_.size() << '\n';

  // assert(problems_.size() == evaluations_.size() &&
  //        "Number of evaluations and numer of problem don't match");

  for (size_t i = 0; i < problems_.size(); i++) {
    if (problems_[i].hasSolution()) return false;
    // if (evaluations_[i] > 0.0) return false;
  }
  return true;
}

template <class Solver, kp2d::EvalType Eval, class EvaluatingMethod,
          class SolvingMethod>
bool kp2d::ProblemSet<Solver, Eval, EvaluatingMethod, SolvingMethod>::isValid()
    const {
  using kp2d::IntType;
  IntType features = problems_[0].numOfFeatures();
  // size_t idx = 0;
  for (const auto& problem : problems_) {
    // std::cout << "[ProblemSet::isValid] problem check: " << idx << '\n';
    if (!problem.isValid()) return false;
    // std::cout << "[ProblemSet::isValid] feature check: " << idx << '\n';
    if (problem.numOfFeatures() != features) return false;
    // if(evaluations_[idx++] != EvaluatingMethod::eval(problem);) return false;
    // idx++;
  }
  return true;
}

template <class Solver, kp2d::EvalType Eval, class EvaluatingMethod,
          class SolvingMethod>
nlohmann::ordered_json kp2d::ProblemSet<Solver, Eval, EvaluatingMethod,
                                        SolvingMethod>::toJson() const {
  using json = nlohmann::ordered_json;
  json kpJson, mData;

  std::string evalDesc;

  kpJson["ObjectType"] = "ProblemSet";
  kpJson["SolvingMethod"] = SolvingMethod::getName();
  kpJson["EvalType"] = EvaluatingMethod::getName();
  kpJson["AvgEval"] = getAvgEval();
  kpJson["Evaluations"] = json::array();
  for (const auto& eval : evaluations_) {
    kpJson["Evaluations"].push_back(eval);
  }
  kpJson["Data"] = solutionsToJson();

  return kpJson;
}

template <class Solver, kp2d::EvalType Eval, class EvaluatingMethod,
          class SolvingMethod>
nlohmann::ordered_json kp2d::ProblemSet<
    Solver, Eval, EvaluatingMethod, SolvingMethod>::solutionsToJson() const {
  using json = nlohmann::ordered_json;
  json kpJson;
  for (const auto& problem : problems_) {
    kpJson.push_back(problem.solutionToJson());
  }
  return kpJson;
}

template <class Solver, kp2d::EvalType Eval, class EvaluatingMethod,
          class SolvingMethod>
bool kp2d::ProblemSet<Solver, Eval, EvaluatingMethod, SolvingMethod>::isEquiv(
    const ProblemSet& ps) const {
  //
  for (size_t i = 0; i < problems_.size(); i++) {
    if (!problems_[i].isEquiv(ps.problems_[i])) return false;
  }
  return true;
}

template class kp2d::ProblemSet<nnet::Network,
                                kp2d::EvalType::UnusedContainerPortion>;
template class kp2d::ProblemSet<nnet::Network,
                                kp2d::EvalType::UnusedStorageArea>;
template class kp2d::ProblemSet<nnet::Network,
                                kp2d::EvalType::BlockedAndUnusedArea>;

template class kp2d::ProblemSet<kp2d::RandomPlacementSolver,
                                kp2d::EvalType::UnusedContainerPortion>;
template class kp2d::ProblemSet<kp2d::RandomPlacementSolver,
                                kp2d::EvalType::UnusedStorageArea>;

template class kp2d::ProblemSet<kp2d::HeuristicSolver,
                                kp2d::EvalType::UnusedStorageArea>;

template class kp2d::ProblemSet<rbp::MaxRectsBinPack,
                                kp2d::EvalType::UnusedContainerPortion>;
template class kp2d::ProblemSet<rbp::MaxRectsBinPack,
                                kp2d::EvalType::UnusedStorageArea>;