#include "include/kp2d/Problem.h"

#include "Problem.h"

using namespace kp2d;

std::string Problem::solutionToString() const {
  std::stringstream oss;
  for (const auto& rectangle : solution_) {
    oss << rectangle << '\n';
  }
  return oss.str();
}

std::string Problem::cornerPointsToString() const {
  if (state_) {
    return state_->cornerPointsToString();
  }
  return "State is set to nullptr\n";
}

std::string Problem::xAxisDistToString() const {
  if (state_) {
    return state_->xAxisDistToString();
  }
  return "State is set to nullptr\n";
}

std::string Problem::storageToString() const {
  std::stringstream oss;
  for (const auto& group : storage_->rectGroups()) {
    oss << group << '\n';
  }
  return oss.str();
}

kp2d::Problem::Problem(const Problem& p)
    : containerSize_{p.containerSize_},
      solution_{p.solution_},
      proposedRects_{p.proposedRects_},
      featureOption_{p.featureOption_} {
  // storage_ = std::unique_ptr<Storage>(new Storage(*p.storage_));
  storage_ = std::make_unique<Storage>(*p.storage_);

  // state_ = std::unique_ptr<State>(new State(*p.state_));
  state_ = std::make_unique<State>(*p.state_);

  for (const auto& stPtr : p.proposedStates_) {
    proposedStates_.push_back(std::unique_ptr<State>(new State(*stPtr)));
  }
}

kp2d::Problem& kp2d::Problem::operator=(const Problem& p) {
  // TODO: insert return statement here
  containerSize_ = p.containerSize_;
  solution_ = p.solution_;
  proposedRects_ = p.proposedRects_;
  storage_ = std::unique_ptr<Storage>(new Storage(*p.storage_));
  state_ = std::unique_ptr<State>(new State(*p.state_));
  for (const auto& stPtr : p.proposedStates_) {
    proposedStates_.push_back(std::unique_ptr<State>(new State(*stPtr)));
  }
  return *this;
}

bool kp2d::Problem::isEquiv(const Problem& p) const {
  if (!storage_->isEquiv(*p.storage_)) return false;

  if (storage_->container() != p.storage_->container()) return false;

  if (state_->unitsPerSample() != p.state_->unitsPerSample()) return false;

  if (state_->container() != p.state_->container()) return false;

  if (containerSize_ != p.containerSize_) return false;

  return true;
}

std::unique_ptr<Problem> Problem::create(Point containerSize,
                                         std::unique_ptr<Storage>&& storage,
                                         FeatOption opt,
                                         SizeType unitsPerSample) {
  return std::unique_ptr<Problem>(
      new Problem(containerSize, std::move(storage), opt, unitsPerSample));
}

bool kp2d::Problem::validateSolution() const {
  if (solution_.size() <= 1) return true;

  for (unsigned i = 0; i < solution_.size(); i++) {
    if (!solution_.at(i).isValid()) return false;
    for (unsigned j = 0; j < i; j++) {
      if (solution_.at(i).isIntersectedBy(solution_.at(j))) return false;
    }
  }
  return true;
}

/// @brief Function checks wether given rectangle fits inside container when
/// placed at the anchor point
bool Problem::canBePlaced(const Rectangle& r) const {
  return r.uppermostY() <= containerSize_.y() &&
         r.rightmostX() <= containerSize_.x();
}

/// TODO
nlohmann::ordered_json kp2d::Problem::toJson() const {
  using json = nlohmann::ordered_json;
  json kpJson, data, mData;

  kpJson["Libname"] = "kp2d";
  kpJson["ObjectType"] = "Solution";

  mData = json::array();
  mData["UnitsPerSample"] = state_->unitsPerSample();
  kpJson["Metadata"] = mData;

  kpJson["Data"] = json::array();
  kpJson["Data"].push_back(solutionToJson());

  return kpJson;
}

void kp2d::Problem::saveToJson(std::ofstream& oFile) const {
  oFile << toJson().dump(2) << '\n';
}

void kp2d::Problem::insert(const std::vector<rbp::Rect>& rects) {
  ///
  for (size_t i = 0; i < rects.size(); i++) {
    for (RectangleGroup& group : storage_->rectGroups()) {
      if ((group.width() == rects[i].width &&
           group.height() == rects[i].height) ||
          (group.height() == rects[i].width &&
           group.width() == rects[i].height)) {
        if (!group.empty()) {
          group.removeRectangle();
          solution_.push_back(Rectangle{rects[i].x, rects[i].y, rects[i].width,
                                        rects[i].height});
          break;
        }
      }
    }
  }
  storage_->setSumArea();
}

std::vector<rbp::RectSize> kp2d::Problem::getRects() {
  std::vector<rbp::RectSize> rects;
  for (RectangleGroup& group : storage_->rectGroups()) {
    for (int i = 0; i < group.number(); i++) {
      rects.push_back(rbp::RectSize{group.width(), group.height()});
    }
  }
  return rects;
}

nlohmann::ordered_json kp2d::Problem::solutionToJson() const {
  using json = nlohmann::ordered_json;
  json sol, xValues, yValues, widths, heights;

  sol["W"] = containerSize_.x();
  sol["H"] = containerSize_.y();

  if (solution_.size() > 0) {
    for (const auto& rect : solution_) {
      xValues.push_back(rect.anchorPoint().x());
      yValues.push_back(rect.anchorPoint().y());
      widths.push_back(rect.width());
      heights.push_back(rect.height());
    }

    sol["x"] = xValues;
    sol["y"] = yValues;
    sol["w"] = widths;
    sol["h"] = heights;
  }

  sol["storage"] = storage_->toJson();

  return sol;
}

FloatType kp2d::Problem::unusedContainerPortion() const {
  if (state_ && state_->usedAreaPortion() > 0.) {
    FloatType ucp = 1.0 - state_->usedAreaPortion();

    assert(ucp >= 0.0);
    assert(ucp != 1.0);

    return ucp;
  } else if (solution_.size() > 0) {
    FloatType usedPortion = 0;
    for (const Rectangle& r : solution_) {
      usedPortion += r.area();
    }
    usedPortion /= containerSize_.product();
    return 1.0 - usedPortion;
  }
  return 1.0;
}

FloatType kp2d::Problem::unusedStorageArea() const {
  return static_cast<FloatType>(storage_->sumArea()) /
         static_cast<FloatType>(containerSize_.product());
}

IntType kp2d::Problem::predictedNumOfFeatures(IntType numOfGroups, IntType H,
                                              IntType uPerSample,
                                              FeatOption option) {
  int xAxisSamplesNum =
      (H % uPerSample == 0 ? H / uPerSample : H / uPerSample + 1);
  switch (option) {
    case FeatOption::allFeatures:
      return Storage::FeaturesPerGroup(option) * numOfGroups +
             State::NumOfFeatures(option) + xAxisSamplesNum;
    case FeatOption::noXDistances:
      return Storage::FeaturesPerGroup(option) * numOfGroups +
             State::NumOfFeatures(option) + 4;
    case FeatOption::areaMismatchHeuristic:
      return 1;
    case FeatOption::minimalFeatures:
      return 5 + State::NumOfFeatures(option);
    case FeatOption::minXDistances:
      return 4 + xAxisSamplesNum;
    default:
      return 0;
  }
}

DynMatrix kp2d::Problem::prepareFeatures() {
  std::vector<FloatType> features;
  SizeType numOfOptions = 0;

  for (auto& group : storage_->rectGroups()) {
    if (!group.empty()) {
      Rectangle rect = group.getRectangle();

      for (const auto& point : state_->cornerPoints()) {
        rect.setAnchorPoint(point);

        for (const auto r : Rectangle::getOrientations(rect)) {
          if (canBePlaced(r)) {
            // std::cout << "[Problem::prepareFeatures] Rectangle: " << r <<
            // '\n';
            std::unique_ptr<State> propState = State::create(*state_, r);
            numOfOptions += 1;

            if (featureOption_ == FeatOption::allFeatures) {
              storage_->appendAllFeatures(features, r);
              propState->appendStateFeatures(features);
              propState->appendXDistFeatures(features);
            } else if (featureOption_ == FeatOption::noXDistances) {
              storage_->appendAllFeatures(features, r);
              features.push_back(static_cast<FloatType>(r.anchorPoint().x()) /
                                 static_cast<FloatType>(containerSize_.x()));
              features.push_back(static_cast<FloatType>(r.anchorPoint().y()) /
                                 static_cast<FloatType>(containerSize_.y()));
              features.push_back(static_cast<FloatType>(r.rightmostX()) /
                                 static_cast<FloatType>(containerSize_.x()));
              features.push_back(static_cast<FloatType>(r.uppermostY()) /
                                 static_cast<FloatType>(containerSize_.y()));
              propState->appendStateFeatures(features);
            } else if (featureOption_ == FeatOption::minimalFeatures) {
              features.push_back(propState->usedAreaPortion());
              features.push_back(static_cast<FloatType>(r.anchorPoint().x()) /
                                 static_cast<FloatType>(containerSize_.x()));
              features.push_back(static_cast<FloatType>(r.anchorPoint().y()) /
                                 static_cast<FloatType>(containerSize_.y()));
              features.push_back(static_cast<FloatType>(r.rightmostX()) /
                                 static_cast<FloatType>(containerSize_.x()));
              features.push_back(static_cast<FloatType>(r.uppermostY()) /
                                 static_cast<FloatType>(containerSize_.y()));
              features.push_back(
                  static_cast<FloatType>(group.sumArea()) /
                  static_cast<FloatType>(containerSize_.product()));
              // propState->appendMinimalFeatures(features);

              FloatType area = static_cast<FloatType>(group.area()) /
                               static_cast<FloatType>(containerSize_.product());

              FloatType inc = propState->envelopeAreaIncrease() - area;
              FloatType mism = 1.0 - propState->mismatch();

              features.push_back(area);
              features.push_back(inc);
              features.push_back(mism);
            } else if (featureOption_ == FeatOption::minXDistances) {
              FloatType area = static_cast<FloatType>(group.area()) /
                               static_cast<FloatType>(containerSize_.product());
              FloatType inc = propState->envelopeAreaIncrease() - area;
              FloatType mism = 1.0 - propState->mismatch();

              features.push_back(inc);
              features.push_back(area);
              features.push_back(mism);
              features.push_back(propState->envelopeAreaPortion());

              propState->appendXDistFeatures(features);
            }

            proposedStates_.push_back(std::move(propState));
            proposedRects_.push_back(r);
          }
        }
      }
      // Rectangle wasn't used, only proposed states were created
      group.addRectangle();
    }
  }

  // std::cout << "[Problem::prepareFeatures] Feature vec size: "
  //           << features.size() << '\n';

  if (features.size() > 0) {
    DynMatrix featureMatrix;
    featureMatrix =
        Eigen::Map<DynMatrix>(&features[0], numOfFeatures(), numOfOptions);

    // std::cout << "[Problem::prepareFeatures] Feature mat size: "
    //           << featureMatrix.rows() << 'x' << featureMatrix.cols() << '\n';

    return featureMatrix;
  }
  return DynMatrix{0, 0};
}

DynMatrix kp2d::Problem::getAreaMismatchHeuristic() {
  std::vector<FloatType> values;
  SizeType numOfOptions = 0;
  FloatType area, mism, hValue, inc;

  for (auto& group : storage_->rectGroups()) {
    if (!group.empty()) {
      Rectangle rect = group.getRectangle();

      for (const auto& point : state_->cornerPoints()) {
        rect.setAnchorPoint(point);

        for (const auto r : Rectangle::getOrientations(rect)) {
          if (canBePlaced(r)) {
            std::unique_ptr<State> propState = State::create(*state_, r);
            numOfOptions += 1;

            area = static_cast<FloatType>(group.area()) /
                   static_cast<FloatType>(containerSize_.product());

            inc = propState->envelopeAreaIncrease() - area;
            mism = propState->mismatch();
            hValue = area * (1. + 3. * (2 - mism)) - 4 * inc;
            values.push_back(hValue);

            assert(values.size() > 0);
            proposedStates_.push_back(std::move(propState));
            proposedRects_.push_back(r);
          }
        }
      }
      // Rectangle wasn't used, only proposed states were created
      group.addRectangle();
    }
  }

  std::cout << "[Problem::getAreaMismatchHeuristic] Values vec size: "
            << values.size() << '\n';

  if (values.size() > 0) {
    DynMatrix featureMatrix;
    featureMatrix = Eigen::Map<DynMatrix>(&values[0], 1, numOfOptions);

    std::cout << "[Problem::prepareFeatures] Feature mat size: "
              << featureMatrix.rows() << 'x' << featureMatrix.cols() << '\n';

    return featureMatrix;
  }
  return DynMatrix{0, 0};
}

void kp2d::Problem::selectState(SizeType idx) {
  // std::cout << "[Problem::selectState] idx: " << idx << '\n';
  // std::cout << "[Problem::selectState] prop. states number: "
  //           << proposedStates_.size() << '\n';
  expect(idx < proposedStates_.size(), kp2d::ErrorCode::OutOfRange,
         __PRETTY_FUNCTION__);
  expect(proposedStates_.size() == proposedRects_.size(),
         kp2d::ErrorCode::UnmatchedStates, __PRETTY_FUNCTION__);

  state_ = std::move(proposedStates_.at(idx));
  solution_.push_back(proposedRects_.at(idx));
  storage_->removeRectangle(proposedRects_.at(idx));

  proposedStates_.clear();
  proposedStates_.reserve(storage_->numOfGroups() * 2 *
                          state_->numOfCornerPoints());

  proposedRects_.clear();
  proposedRects_.reserve(storage_->numOfGroups() * 2 *
                         state_->numOfCornerPoints());
}