#include "include/kp2d/ProblemGenerator.h"

#include "ProblemGenerator.h"

bool kp2d::ProblemGenerator::operator==(const ProblemGenerator& pg) const {
  return storGen_ == pg.storGen_ && container_ == pg.container_ &&
         unitsPerSample_ == pg.unitsPerSample_ &&
         numOfGroups_ == pg.numOfGroups_;
}

kp2d::IntType kp2d::ProblemGenerator::numOfFeatures() {
  if (numOfGroups_ <= 0) {
    Problem p{container_, storGen_.generate(container_), featureOption_,
              unitsPerSample_};
    numOfGroups_ = p.numOfGroups();

    // std::cout << "[ProblemGenerator::numOfFeatures] Number features "
    //              "when generating sample: "
    //           << p.numOfFeatures() << '\n';

    return p.numOfFeatures();
  }
  // std::cout << "[ProblemGenerator::numOfFeatures] Number of x axsis samples:
  // "
  //           << numOfxAxisSamples() << '\n'
  //           << "[ProblemGenerator::numOfFeatures] Number of features: "
  //           << Storage::FeaturesPerGroup * numOfGroups_ +
  //           State::NumOfFeatures +
  //                  numOfxAxisSamples()
  //           << '\n';

  return Storage::FeaturesPerGroup(featureOption_) * numOfGroups_ +
         State::NumOfFeatures(featureOption_) + numOfxAxisSamples();
}

kp2d::Problem kp2d::ProblemGenerator::generate() {
  if (numOfGroups_ <= 0) {
    Problem p{container_, storGen_.generate(container_), featureOption_,
              unitsPerSample_};
    numOfGroups_ = p.numOfGroups();
    return p;
  }
  Problem p{container_, storGen_.generate(container_, numOfGroups_),
            featureOption_, unitsPerSample_};
  // std::cout << "[ProblemGenerator::generate] number of features: "
  //           << p.numOfFeatures() << '\n';
  return p;
}

std::unique_ptr<kp2d::Problem> kp2d::ProblemGenerator::generatePtr() {
  if (numOfGroups_ <= 0) {
    std::unique_ptr<Problem> p =
        Problem::create(container_, storGen_.generate(container_),
                        featureOption_, unitsPerSample_);
    numOfGroups_ = p->numOfGroups();
    return p;
  }
  return kp2d::Problem::create(container_,
                               storGen_.generate(container_, numOfGroups_),
                               featureOption_, unitsPerSample_);
}

kp2d::IntType kp2d::ProblemGenerator::numOfxAxisSamples() const {
  // std::cout << "[ProblemGenerator::numOfxAxisSamples] container_.y() / "
  //              "unitsPerSample_ = "
  //           << num << '\n';

  kp2d::IntType num = container_.y() / unitsPerSample_;
  return (container_.y() % unitsPerSample_ == 0 ? num : num + 1);
}