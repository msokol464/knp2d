#include "include/Training.h"

void app::printTrainUsage() {
  std::cout << "kp2dapp train [options]\n"
            << "-h <number_of_hidden_net_nodes>\n"
            << "-a <activation_type> (Identity/ReLU/Sigmoid/Tanh/SoftMax)\n"
            << "-g <number_of_groups>\n"
            << "-t <number_of_tasks>\n"
            << "-f <maximal_num_of_function_evaluation>\n"
            << "-o <output_directory>\n";
}

app::TrainingSetup app::parseArgvTrain(int argc, char *argv[]) {
  TrainingSetup setup;
  for (int i = 0; i < argc; i++) {
    if (strcmp(argv[i], "-h") == 0) {
      std::cout << "-h\n";
      //
      i++;
      std::stringstream sstr{argv[i]};
      sstr >> setup.hiddenLayerDim;
      if (!sstr.fail() && sstr.eof()) {
        std::cout << "Number of hidden layers: " << setup.numberOfTasks << '\n';
      } else {
        std::cout << "Incorrect number of hidden layers\n";
        return TrainingSetup{};
      }

    } else if (strcmp(argv[i], "-a") == 0) {
      std::cout << "-a\n";
      //
      i++;
      if (sizeof(argv[i]) > TRAIN_MAX_STR_LEN) {
        std::cout << "String passed as activation type is too lenghty\n";
        return TrainingSetup{};
      }
      setup.activationType = std::string{argv[i]};
      std::cout << "Activation type: " << setup.activationType << '\n';

    } else if (strcmp(argv[i], "-g") == 0) {
      std::cout << "-g\n";
      //
      i++;
      std::stringstream sstr{argv[i]};
      sstr >> setup.numberOfGroups;
      if (!sstr.fail() && sstr.eof()) {
        std::cout << "Number of groups: " << setup.numberOfGroups << '\n';
      } else {
        std::cout << "Failed to read number of groups\n";
        return TrainingSetup{};
      }
    } else if (strcmp(argv[i], "-t") == 0) {
      std::cout << "-t\n";
      //
      i++;
      std::stringstream sstr{argv[i]};
      sstr >> setup.numberOfTasks;
      if (!sstr.fail() && sstr.eof()) {
        std::cout << "Number of tasks: " << setup.numberOfTasks << '\n';
      } else {
        std::cout << "Failed to read number of tasks\n";
        return TrainingSetup{};
      }
    } else if (strcmp(argv[i], "-f") == 0) {
      std::cout << "-f\n";
      //
      i++;
      std::stringstream sstr{argv[i]};
      sstr >> setup.maxOptimzationEvals;
      if (!sstr.fail() && sstr.eof()) {
        std::cout << "Maximum number of function evaluations: "
                  << setup.maxOptimzationEvals << '\n';
      } else {
        std::cout << "Failed to read number of function evaluations\n";
        return TrainingSetup{};
      }
    } else if (strcmp(argv[i], "-o") == 0) {
      std::cout << "-o\n";
      //
      i++;
      if (sizeof(argv[i]) > SOLVE_MAX_STR_LEN) {
        std::cout << "Path is too lengthy\n";
        return TrainingSetup{};
      }
      setup.outDirname = std::string{argv[i]};
      std::cout << "Output path: " << setup.outDirname << '\n';
    }
  }
  return setup;
}

//   int hiddenLayerDim;
//   std::string activationType;
//   int numberOfGroups;
//   int numberOfTasks;
//   int maxOptimzationEvals;
//   std::string outDirname;
bool app::isTrainingSetupValid(const TrainingSetup &setup) {
  if (setup.hiddenLayerDim <= 0 || setup.hiddenLayerDim > TRAIN_MAXHIDDEN) {
    std::cout << "Number of hidden nodes exceeds range [1, " << TRAIN_MAXHIDDEN
              << "]\n";
    return false;
  }
  if (setup.activationType.length() <= 0 ||
      setup.activationType.length() > SOLVE_MAX_STR_LEN ||
      nnet::Activation::typeMap.find(setup.activationType) ==
          nnet::Activation::typeMap.end()) {
    std::cout << "Activation name is incorrect\nEnter one of the following: "
                 "Identity/ReLU/Sigmoid/Tanh/SoftMax\n";
    return false;
  }
  if (setup.numberOfGroups <= 0 || setup.numberOfGroups > TRAIN_MAXGROUPS) {
    std::cout << "Number of groups exceeds range [1, " << TRAIN_MAXGROUPS
              << "]\n";
    return false;
  }
  if (setup.numberOfTasks <= 0 || setup.numberOfTasks > TRAIN_MAXTASKS) {
    std::cout << "Number of tasks exceeds range [1, " << TRAIN_MAXTASKS
              << "]\n";
    return false;
  }
  if (setup.maxOptimzationEvals <= 0 ||
      setup.maxOptimzationEvals > TRAIN_MAXEVALS) {
    std::cout
        << "Maximal number of optimized function evaluation exceeds range [1, "
        << TRAIN_MAXEVALS << "]\n";
    return false;
  }
  if (!std::filesystem::is_directory(setup.outDirname)) {
    std::cout << "Output path given is not a valid directory name\n";
    return false;
  }
  return true;
}

int app::train(const TrainingSetup &setup) {
  const kp2d::EvalType eval = kp2d::EvalType::UnusedStorageArea;
  kp2d::ProblemGenerator gen{
      kp2d::Point{solvingConst::W, solvingConst::H},
      kp2d::NormalInt::create(solvingConst::meanN, solvingConst::varN),
      kp2d::UniformInt::create(0.2 * solvingConst::W, 0.8 * solvingConst::H),
      kp2d::FeatOption::allFeatures};
  gen.setNumOfGroups(setup.numberOfGroups);

  using SSet = kp2d::SolvableSet<nnet::Network, eval>;
  using PSet = kp2d::ProblemSet<nnet::Network, eval>;
  ///
  nnet::Activation::Type activType =
      nnet::Activation::typeMap.at(setup.getActivationType());

  // using Layer =
  //     std::tuple<nnet::SizeType, nnet::SizeType, nnet::Activation::Type>;

  // auto l1 = Layer{static_cast<long int>(gen.numOfFeatures()), 8,
  //                 nnet::Activation::Type::RELU};
  // auto l2 = Layer{8, 1, Activation::Type::RELU};
  // nnet::Network net{
  //     {{static_cast<long int>(gen.numOfFeatures()), setup.hiddenLayerDim,
  //       nnet::Activation::create(activType)},
  //      {setup.hiddenLayerDim, static_cast<long int>(1),
  //       nnet::Activation::create(activType)}}};

  nnet::Network net{};
  net.addLayer(
      nnet::DenseLayer::create(static_cast<long int>(gen.numOfFeatures()),
                               setup.hiddenLayerDim, activType));
  net.addLayer(nnet::DenseLayer::create(setup.hiddenLayerDim, 1, activType));

  // Training sets
  std::vector<PSet> problemSets;
  for (size_t i = 0; i < trainingConst::setSize; i++) {
    PSet set = gen.generateSet<nnet::Network, eval>(setup.numberOfTasks);
    problemSets.push_back(set);
  }

  SSet initSet{gen.generateSet<nnet::Network, eval>(setup.numberOfTasks), net};
  SSet validSet{
      gen.generateSet<nnet::Network, eval>(trainingConst::validationSetSize),
      net};

  using Objective = nnet::ObjFunction<SSet>;
  using CMAOptimizer = nnet::Optimizer<Objective, cma::pwqBoundStrategy>;
  using Validation =
      nnet::ValFunction<SSet, PSet, Objective, cma::pwqBoundStrategy>;

  Objective objectiveFun{initSet, 8};
  Validation validation{objectiveFun,
                        validSet,
                        trainingConst::validationIter,
                        problemSets,
                        trainingConst::replacementIter,
                        "validation.txt",
                        setup.outDirname,
                        "net.json"};

  CMAOptimizer opt(std::move(objectiveFun), -1, validation, -1,
                   setup.maxOptimzationEvals);

  opt.run();
  opt.print();

  return 0;
}
