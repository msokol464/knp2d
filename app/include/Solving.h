#pragma once

#include <filesystem>
#include <fstream>
#include <string>

#include "../../kp2d/include/kp2d/Problem.h"
#include "../../kp2d/include/kp2d/ProblemGenerator.h"
#include "../../kp2d/include/kp2d/SolvableSet.h"
#include "../../nnet/include/nnet/Network.h"

namespace app {
constexpr int SOLVE_MAX_STR_LEN = 1024;
constexpr int SOLVE_ARGNUM = 10;
constexpr int SOLVE_MAXTASKS = 2000;
constexpr int SOLVE_MAXGROUPS = 12;
const std::string NET_EXT = ".json";

namespace solvingConst {
constexpr int W = 1200;
constexpr int H = 800;
constexpr int meanN = 10;
constexpr int varN = 3;
constexpr int unitsPerSample = 8;

}  // namespace solvingConst

struct SolvingSetup {
  std::string netPathname;
  std::string outDirname;
  int numberOfTasks;
  int numberOfGroups;
};

void printSolveUsage();

SolvingSetup parseArgvSolve(int argc, char* argv[]);

bool isSolvingSetupValid(const SolvingSetup& setup);

int solve(const SolvingSetup& setup);

}  // namespace app