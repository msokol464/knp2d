#pragma once

#include "../../nnet/include/nnet/ObjFunction.h"
#include "../../nnet/include/nnet/Optimizer.h"
#include "../../nnet/include/nnet/ValFunction.h"
#include "Solving.h"

namespace app {
constexpr int TRAIN_MAX_STR_LEN = 1024;
constexpr int TRAIN_ARGNUM = 14;
constexpr int TRAIN_MAXHIDDEN = 100;
constexpr int TRAIN_MAXTASKS = 2000;
constexpr int TRAIN_MAXGROUPS = 12;
constexpr int TRAIN_MAXEVALS = 500000;

namespace trainingConst {
constexpr int validationIter = 10;
constexpr int validationSetSize = 64;
constexpr int setSize = 5000;
constexpr int replacementIter = 1;
}  // namespace trainingConst

struct TrainingSetup {
  long int hiddenLayerDim;
  std::string activationType;
  int numberOfGroups;
  int numberOfTasks;
  int maxOptimzationEvals;
  std::string outDirname;

  const std::string& getActivationType() const { return activationType; }
};

void printTrainUsage();

TrainingSetup parseArgvTrain(int argc, char* argv[]);

bool isTrainingSetupValid(const TrainingSetup& setup);

int train(const TrainingSetup& setup);

}  // namespace app