#include <cstring>
#include <iostream>
#include <sstream>
#include <string>

#include "include/Solving.h"
#include "include/Training.h"

int main(int argc, char* argv[]) {
  if (argc < 2) {
    app::printSolveUsage();
    app::printTrainUsage();
    return -1;
  } else if (strcmp(argv[1], "solve") == 0) {
    if (argc != app::SOLVE_ARGNUM) {
      app::printSolveUsage();
      return -1;
    }
    app::SolvingSetup setup = app::parseArgvSolve(argc - 2, argv + 2);
    if (!app::isSolvingSetupValid(setup)) return -1;

    try {
      return app::solve(setup);
    } catch (std::exception& e) {
      std::cout << "Error occured: " << e.what() << '\n';
      return -1;
    }

  } else if (strcmp(argv[1], "train") == 0) {
    if (argc != app::TRAIN_ARGNUM) {
      app::printTrainUsage();
      return -1;
    }
    // std::cout << "Work in proggress\n";

    app::TrainingSetup setup = app::parseArgvTrain(argc - 2, argv + 2);
    if (!app::isTrainingSetupValid(setup)) return -1;

    try {
      return app::train(setup);
    } catch (std::exception& e) {
      std::cout << "Error occured: " << e.what() << '\n';
      return -1;
    }

  } else {
    app::printSolveUsage();
    app::printTrainUsage();
    return -1;
  }

  // kp2dapp train USAGE:
  // get training problem instances and evaluation problem set
  //-------------------------------

  // construct solver instance
  //-------------------------------

  // training process
  //-------------------------------

  // evaluation
  //-------------------------------

  // DONE

  return 0;
}
