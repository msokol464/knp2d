#include "include/Solving.h"

void app::printSolveUsage() {
  std::cout << "kp2dapp solve [options]\n"
            << "-n <network_json_pathname>\n"
            << "-o <output_directory>\n"
            << "-t <number_of_tasks>\n"
            << "-g <number_of_groups>\n";
}

app::SolvingSetup app::parseArgvSolve(int argc, char *argv[]) {
  SolvingSetup setup;
  for (int i = 0; i < argc; i++) {
    if (strcmp(argv[i], "-n") == 0) {
      std::cout << "-n\n";
      //
      i++;
      if (sizeof(argv[i]) > SOLVE_MAX_STR_LEN) {
        std::cout << "Net path is too lengthy\n";
        return SolvingSetup{};
      }
      setup.netPathname = std::string{argv[i]};
      std::cout << "Network file: " << setup.netPathname << '\n';

    } else if (strcmp(argv[i], "-o") == 0) {
      std::cout << "-o\n";
      //
      i++;
      if (sizeof(argv[i]) > SOLVE_MAX_STR_LEN) {
        std::cout << "Path is too lengthy\n";
        return SolvingSetup{};
      }
      setup.outDirname = std::string{argv[i]};
      std::cout << "Output path: " << setup.outDirname << '\n';

    } else if (strcmp(argv[i], "-t") == 0) {
      std::cout << "-t\n";
      //
      i++;
      std::stringstream sstr{argv[i]};
      sstr >> setup.numberOfTasks;
      if (!sstr.fail() && sstr.eof()) {
        std::cout << "Number of tasks: " << setup.numberOfTasks << '\n';
      } else {
        std::cout << "Incorrect number of tasks\n";
        return SolvingSetup{};
      }
    } else if (strcmp(argv[i], "-g") == 0) {
      std::cout << "-g\n";
      //
      i++;
      std::stringstream sstr{argv[i]};
      sstr >> setup.numberOfGroups;
      if (!sstr.fail() && sstr.eof()) {
        std::cout << "Number of groups: " << setup.numberOfGroups << '\n';
      } else {
        std::cout << "Incorrect number of groups\n";
        return SolvingSetup{};
      }
    }
  }
  return setup;
}

bool app::isSolvingSetupValid(const SolvingSetup &setup) {
  if (setup.numberOfTasks <= 0 || setup.numberOfTasks > SOLVE_MAXTASKS) {
    std::cout << "Number of tasks exceeds range [1, " << SOLVE_MAXTASKS
              << "]\n";
    return false;
  }
  if (setup.numberOfGroups <= 0 || setup.numberOfGroups > SOLVE_MAXGROUPS) {
    std::cout << "Number of groups exceeds range [1, " << SOLVE_MAXGROUPS
              << "]\n";
    return false;
  }
  if (setup.netPathname.length() <= NET_EXT.length() ||
      setup.netPathname.length() > SOLVE_MAX_STR_LEN) {
    std::cout << "Network pathname is to short or exceeds " << SOLVE_MAX_STR_LEN
              << " characters\n";
    return false;
  }
  if (setup.outDirname.length() <= 0 ||
      setup.outDirname.length() > SOLVE_MAX_STR_LEN) {
    std::cout << "Output directory name is to short or exceeds "
              << SOLVE_MAX_STR_LEN << " characters\n";
    return false;
  }
  if (setup.netPathname.compare(setup.netPathname.length() - NET_EXT.length(),
                                NET_EXT.length(), NET_EXT)) {
    std::cout << "Network pathname given is not a " << NET_EXT << " file\n";
    return false;
  }
  if (!std::filesystem::is_directory(setup.outDirname)) {
    std::cout << "Output path given is not a valid directory name\n";
    return false;
  }
  if (!std::filesystem::exists(setup.netPathname) ||
      !std::filesystem::is_regular_file(setup.netPathname)) {
    std::cout << "Network file pathname given is not a valid filename or does "
                 "not exist\n";
    return false;
  }

  try {
    std::cout << setup.netPathname << '\n';
    nnet::Network net = nnet::Network::loadFromJson(setup.netPathname);
    int featureVecSize = kp2d::Problem::predictedNumOfFeatures(
        setup.numberOfGroups, solvingConst::H, solvingConst::unitsPerSample,
        kp2d::FeatOption::allFeatures);
    if (net.inputDim() != featureVecSize) {
      std::cout << "Network input size (" << net.inputDim()
                << ") does not match predicted feature vector size ("
                << featureVecSize << ")\n";
      return false;
    }

  } catch (std::exception &e) {
    std::cout << e.what() << '\n';
    std::cout << "Network couldn't be read from file\n";
    return false;
  }

  return true;
}

int app::solve(const SolvingSetup &setup) {
  const kp2d::EvalType eval = kp2d::EvalType::UnusedStorageArea;

  kp2d::ProblemGenerator gen{
      kp2d::Point{solvingConst::W, solvingConst::H},
      kp2d::NormalInt::create(solvingConst::meanN, solvingConst::varN),
      kp2d::UniformInt::create(0.2 * solvingConst::W, 0.8 * solvingConst::H),
      kp2d::FeatOption::allFeatures};
  gen.setNumOfGroups(setup.numberOfGroups);

  using SSet = kp2d::SolvableSet<nnet::Network, eval>;
  ///
  nnet::Network net = nnet::Network::loadFromJson(setup.netPathname);
  if (net.inputDim() != gen.numOfFeatures()) {
    std::cout << "Network input size (" << net.inputDim()
              << ") does not match generator feature vector size ("
              << gen.numOfFeatures() << ")\n";
    return -1;
  }

  SSet pset{gen.template generateSet<nnet::Network, eval>(setup.numberOfTasks),
            net};
  pset.solve();

  std::string solFilename = setup.outDirname + "/solution.json";
  int i = 1;
  while (std::filesystem::exists(solFilename)) {
    solFilename = setup.outDirname + "/solution_" + std::to_string(i) + ".json";
    i++;
  }

  std::ofstream out{solFilename, std::ios_base::out | std::ios_base::trunc};
  pset.saveToJson(out);
  out.close();
  return 0;
}
